({
    doInit : function(component, event, helper) {
        
        var recID = component.get("v.recordId");
        var action = component.get("c.fecthExistingValues");
        action.setParams({
            recordId :  recID
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS' && component.isValid()){
                var state = response.getState();            
                //component.set('v.AccountId', response.getReturnValue().accountId); 
                component.set("v.FirstName",response.getReturnValue().firstname);
				component.set("v.LastName",response.getReturnValue().lastname);
                component.set("v.Email",response.getReturnValue().email);

            }else{
                alert('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    
    handleSubmit : function(component, event, helper) {
        helper.handleSubmitHelper(component,event,helper);
    },
    closemodal : function(component,event,helper){
        helper.closerecordcreation(component,event,helper);
    }    
})