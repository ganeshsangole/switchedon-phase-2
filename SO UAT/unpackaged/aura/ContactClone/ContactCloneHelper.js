({
    closerecordcreation:function(component,event,helper)
    {
        $A.get("e.force:closeQuickAction").fire();
       
    },
    handleSubmitHelper:function(component,event,helper)
    {
        var recID = component.get("v.recordId");
        var accountId= component.get("v.AccountId");
        if(accountId == '')
        {
            alert('Please enter Account Name');
        }
        else{
        var firstname=component.get("v.FirstName");
        var lastname=component.get("v.LastName");
        var email=component.get("v.Email");
        
        console.log('accountId in save',accountId);
        console.log('firstname in save',firstname);
        console.log('lastname in save',lastname);
        console.log('email in save',email);

        var action = component.get("c.cloneContact");
        action.setParams({
            recordId :  recID,
            firstname : firstname,
            lastname : lastname,
            email: email,
            accountId : accountId
        });
        
        action.setCallback(this, function(response){
           
            var state = response.getState();
            if(state === 'SUCCESS'){
                var state = response.getState();            
               var navEvt = $A.get("e.force:navigateToSObject");
    			navEvt.setParams({
      			"recordId": response.getReturnValue(),
      			"slideDevName": "detail"
    			});
    			navEvt.fire();
                $A.get('e.force:refreshView').fire();
            }else{
                alert('ERROR');
            }
        });
        $A.enqueueAction(action);
        }
    }
})