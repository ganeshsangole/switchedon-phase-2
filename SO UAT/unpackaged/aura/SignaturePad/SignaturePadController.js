({
    Init : function(component, event, helper) {
        var action = component.get("c.getQualificationDetails");
        console.log('Inside init*********');
        var recordId= component.get("v.recordId");
        console.log('REcordId*******112',recordId);
        action.setParams({
            recordId : recordId
        });
        console.log('After calling function*******');
        action.setCallback(this,function(response){
              var state = response.getState();
            var res = response.getReturnValue(); 

            console.log('res value'+JSON.stringify(res));
                                    console.log('After success ********8888+ 12',res[0]);

            if(state==="SUCCESS"){
                console.log('result*********88'+ res.length);
                console.log('noTradeQualificationPresent',component.get("v.noTradeQualificationPresent"));
                if(res.length === 0){
                  component.set("v.noTradeQualificationPresent",true);
              console.log('noTradeQualificationPresent',component.get("v.noTradeQualificationPresent"));
                }
                else{
                 component.set("v.TradeQualificationPresent",true);  
                console.log('noTradeQualificationPresent',component.get("v.noTradeQualificationPresent"));
                for(var i=0;i<res.length;i++){
                    
                    if(res[i]==='TradeQualification1')
                    {
                        console.log('firstTradeQualification',component.get("v.firstTradeQualification"));
                    component.set("v.firstTradeQualification",true);
                    console.log('component.get()',component.get("v.firstTradeQualification"));
                    }
                    if(res[i]==='TradeQualification2')
                    {
                        console.log('secondTradeQualification',component.get("v.secondTradeQualification"));
					component.set("v.secondTradeQualification",true);
                    console.log('component.get()',component.get("v.secondTradeQualification"));
                    }
                    if(res[i]==='TradeQualification3'){
                        console.log('thirdTradeQualification',component.get("v.thirdTradeQualification"));

                  component.set("v.thirdTradeQualification",true);
                    console.log('component.get()',component.get("v.thirdTradeQualification"));
                    }
                }
                }
                 }
        });       
        $A.enqueueAction(action);
        helper.doInit(component, event, helper);
    },
    erase:function(component, event, helper){
        helper.eraseHelper(component, event, helper);
    },
    save:function(component, event, helper){
        var recordId=component.get("v.recordId");
        helper.saveHelper(component, event, helper,recordId);
    },
    savePDF:function(component, event, helper) {
        console.log('Onclick of download button');
        var recordId=component.get("v.recordId");
        console.log(recordId);
        
        helper.savePDFHelper(component, event, helper,recordId);
    },
    cancel : function(component,event,helper){
        console.log('Inside cancel onclick');
        $A.get("e.force:closeQuickAction").fire();
    }
})