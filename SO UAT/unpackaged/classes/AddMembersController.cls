/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class For adding Campaign Members from Add Members quick action on campaign 
History
29-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/

public with sharing class AddMembersController {
    private static String folderName = System.Label.Add_Campaign_Member_Report_Folder;
    
    AddMembersController() { } 

    @AuraEnabled(cacheable=true) 
    public static Map<String, List<ReportWrapper>> reportsAndFolder(){
        Map<String, List<ReportWrapper>> results = new Map<String, List<ReportWrapper>>();
        List<String> reportNames = new list<String>();
        reportNames = System.label.Add_Members_Folder_Names.split(';');
        system.debug('*****reportNames'+reportNames);
        for(Report reportObj : [SELECT Id, Name, FolderName  FROM Report WHERE FolderName != null and FolderName IN: reportNames]) {
            if(results.containsKey(reportObj.FolderName)) {
                results.get(reportObj.FolderName).add(new ReportWrapper(reportObj.Name, reportObj.Id));
            }else {
                results.put(reportObj.FolderName, new List<ReportWrapper>{new ReportWrapper(reportObj.Name, reportObj.Id)});
            }
        }

        return results;
    } 

    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> availableReports() {
        List<Report> reports = new List<Report>([SELECT Id, Name FROM Report WHERE FolderName =: folderName]);
        List<ReportWrapper> reportWrapperList = new List<ReportWrapper>();
        for(Report rept : reports) {
            ReportWrapper rw = new ReportWrapper(rept.Name, rept.Id);
            reportWrapperList.add(rw);
        }
        return reportWrapperList;
    }

    @AuraEnabled
    public static String createMembers(String reportId, String campaignId) {
        try {
            
            List<String> recordIds = GetReportData.getSobjectIds(reportId);
            Integer successRecords = 0;
            Integer failedRecords = 0;
            List<CampaignMember> members = new List<CampaignMember>();
            for(String recordId : recordIds) {
                CampaignMember cm = new CampaignMember();
                cm.campaignId = campaignId;
                cm.contactId = recordId;
                members.add(cm);
            }

            if(!members.isEmpty()) {
                //insert members;
                Database.SaveResult[] srList = Database.insert(members, false);
                for(Database.SaveResult sr : srList) {
                    if(sr.isSuccess()) {
                        successRecords++;
                    }else {
                        failedRecords++;
                    }
                }
            }

          
            Campaign cmp = [SELECT Id, Name FROM Campaign WHERE Id =: campaignId LIMIT 1];
            
            String saveMessage = successRecords +' new members were added to '+cmp.Name;
            
            return saveMessage;
            
        }catch(Exception exp) {
            throw exp;
        }
    }

    public class ReportWrapper {
        @AuraEnabled
        public String elementLabel {get; set;}
        @AuraEnabled
        public String elementValue {get; set;}
        public ReportWrapper(String label, String val) {
            elementLabel = label;
            elementValue = val;
        }
    }
}