/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For AddMembersController 
History
29-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/

@isTest
public class AddMembersControllerTest {    
    
    @isTest(SeeAllData='true')
    public static void reportsAndFolder_Test(){ 
        Map<String, List<AddMembersController.ReportWrapper>> results = new Map<String, List<AddMembersController.ReportWrapper>>();
        Test.startTest();   
        results = AddMembersController.reportsAndFolder();
        Test.stopTest(); 
        system.assert(true);
    }
    
    @isTest(SeeAllData='true')
    public static void availableReports_Test(){           
        Test.startTest();   
        AddMembersController.availableReports();
        Test.stopTest(); 
        system.assert(true);
    }
    
    @isTest(SeeAllData='true')
    public static void createMembers_Test(){  
        List<Report> reportsList = new List<Report>([SELECT Id, Name FROM Report where DeveloperName='Hubspot_Migration_Add_Members_Test_crE' limit 1]);
        List<Campaign> campaignList =new List<Campaign>([select id from Campaign limit 1]); 
        
        if(reportsList != null && !reportsList.isEmpty() && campaignList != null && !campaignList.isEmpty()){
            Test.startTest();   
            AddMembersController.createMembers(reportsList[0].Id,campaignList[0].Id);
            Test.stopTest(); 
            system.assert(true); 
        }
        
    }
    
}