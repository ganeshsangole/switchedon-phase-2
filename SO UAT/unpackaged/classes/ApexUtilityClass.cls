/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class For Common Function Calls 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class ApexUtilityClass {     
    
    /*Create Header as per the values passed*/ 
    public static HttpResponse getAPIResponse(map<string,string> MapOfHeaderDetails){
        HttpRequest request = new HttpRequest();
        request.setTimeOut(120000);     
        
        for(String HeaderParameter:MapOfHeaderDetails.keyset()){
            if(HeaderParameter=='endPoint'){
                request.setEndPoint(MapOfHeaderDetails.get('endPoint'));
                continue;
            }
            else if(HeaderParameter=='methodType'){
                request.setMethod(MapOfHeaderDetails.get('methodType'));
                continue;
            }
            else{             
                request.setHeader(HeaderParameter, MapOfHeaderDetails.get(HeaderParameter)); 
                continue;
            }             
        } 
        Http http = new Http();
        HttpResponse response = http.send(request);
        
        return response;
    }
    
    
    
    
    
    
    /*Request Access Token for API Call.*/
    public static String requestAccessToken() {
        try{
            Outlook_Setting__mdt azureAuth = [SELECT Id, DeveloperName, Azure_Tenent_ID__c, Azure_Client_Secret__c, 
                                              Azure_Client_ID__c, Azure_Grant_Type__c, 
                                              Azure_Scope__c 
                                              FROM Outlook_Setting__mdt 
                                              LIMIT 1];
            
            String strbody = 'client_id =' + azureAuth.Azure_Client_ID__c + '&client_secret=' + azureAuth.Azure_Client_Secret__c + '&scope=' + azureAuth.Azure_Scope__c + '&grant_type=' + azureAuth.Azure_Grant_Type__c;                                        
            
            System.debug('strBody***** : '+strBody);
            HttpRequest req = new HttpRequest();
            req.setTimeOut(120000);
            String endPoint = 'callout:Microsoft_Online' + '/' + azureAuth.Azure_Tenent_ID__c + '/oauth2/v2.0/token';
            System.debug('endPoint****** : '+endPoint);
            //String endPoint = 'https://login.microsoftonline.com/{e45aac13-f489-4d77-9004-00666bf2f525}/oauth2/v2.0/token HTTP/1.1';
            req.setEndPoint(endPoint);
            req.setMethod('POST');
            req.setBody(strbody);
            Http http = new Http();
            HttpResponse response = http.send(req);
            
            if(response.getStatusCode() == 200) {
                Map<String, Object> responseGetBody = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                String accessToken = (String) responseGetBody.get('access_token');
                System.debug('Accesstoken after 200*** : '+accessToken);
                return accessToken;
            }else {
                System.debug('Error Occured******** : '+response.getStatus());
                System.debug('Error code********* : '+response.getStatusCode());
                System.debug('Error message******* : '+response.getBody());
            }
            
            return '';
        }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','ApexUtilityClass','requestAccessToken');
            result.getReturnValue(); // Log Exception
        }
        return '';
    }
    
    
    
    
    
    /*Create Outlook Contact And Salesforce Contact Field Map */
    public static map<String,String> createFieldMappingMap() {
        map<string,string> MapFieldMapping=new map<string,string>();
        list<Outlook_Salesforce_Field_Mapping__mdt>ListFieldMapping=new list<Outlook_Salesforce_Field_Mapping__mdt>();
        
        ListFieldMapping=[select id,Outlook_Contact_Field_Name__c,Salesforce_Contact_Field_API_Name__c,Data_Type__c from Outlook_Salesforce_Field_Mapping__mdt];
        
        if(ListFieldMapping !=null && !ListFieldMapping.isempty()){
            for(Outlook_Salesforce_Field_Mapping__mdt fieldMappingRecord:ListFieldMapping){
                if(fieldMappingRecord.Outlook_Contact_Field_Name__c !=null && fieldMappingRecord.Salesforce_Contact_Field_API_Name__c !=null){
                    MapFieldMapping.put(fieldMappingRecord.Outlook_Contact_Field_Name__c,fieldMappingRecord.Salesforce_Contact_Field_API_Name__c);             
                }
            }
        } 
        return MapFieldMapping;        
    }
    
    /*Create Map Of Contact Field And Its DataType*/
    public static map<String,String> createMapOfContactFieldAndItsDataType(){
        map<string,string> mapOfContactFieldAndItsDataType=new map<string,string>();
        list<Outlook_Salesforce_Field_Mapping__mdt>listFieldMapping=new list<Outlook_Salesforce_Field_Mapping__mdt>();
        listFieldMapping=[select id,Outlook_Contact_Field_Name__c,Salesforce_Contact_Field_API_Name__c,Data_Type__c from Outlook_Salesforce_Field_Mapping__mdt];
        
        if(listFieldMapping !=null && !listFieldMapping.isempty()){
            for(Outlook_Salesforce_Field_Mapping__mdt fieldMappingRecord:listFieldMapping){
                if(fieldMappingRecord.Outlook_Contact_Field_Name__c !=null && fieldMappingRecord.Salesforce_Contact_Field_API_Name__c !=null){
                    mapOfContactFieldAndItsDataType.put(fieldMappingRecord.Salesforce_Contact_Field_API_Name__c,fieldMappingRecord.Data_Type__c);             
                }
            }
        }
        return mapOfContactFieldAndItsDataType;
    }
    
    
    /*Handle Success And Failed Records Result */
    public static void handleDMLResults(list<Database.UpsertResult> upsertResult,List<SObject>sObjectList,String objectName,Boolean sendEmail,String emailSubject){
        
        string successBody='';
        string errorBody=''; 
        Integer noOfSuccess = 0;
        Integer noOfErrors = 0;        
        
        List<Object_And_Its_Id_Field_API_Name__mdt> ObjectAndItsIdFieldAPIName=new list<Object_And_Its_Id_Field_API_Name__mdt>();
        ObjectAndItsIdFieldAPIName=[select Id,ID_Field_API_Name__c,Object_API_Name__c from Object_And_Its_Id_Field_API_Name__mdt where Object_API_Name__c=:objectName limit 1];
        
        try{             
            if( upsertResult != null && !upsertResult.isEmpty()){
                
                for(Integer index = 0 ; index < upsertResult.size() ; index ++){
                    if(!upsertResult[index].isSuccess() ){
                        noOfErrors++;
                        system.debug('***failbody');
                        if(objectName == 'Contact'){
                            errorBody += string.valueOf(((Contact)sObjectList.get( index )).get(ObjectAndItsIdFieldAPIName[0].ID_Field_API_Name__c)) + ','; 
                        }
                        else if(objectName == 'Account'){
                            errorBody += string.valueOf(((Account)sObjectList.get( index )).get(ObjectAndItsIdFieldAPIName[0].ID_Field_API_Name__c)) + ','; 
                        }
                        
                        
                        string errorMsg = '';
                        for(Database.Error error : upsertResult[index].getErrors()){
                            errorMsg += error.getMessage() + ',';
                        }
                        errorMsg = errorMsg.removeEnd(',');
                        errorBody += '"'+errorMsg + '"'; 
                        errorBody += '\n';
                    }else{
                        noOfSuccess++;
                        system.debug('***successBody');
                        if(objectName == 'Contact'){
                            successBody += String.valueOf(((Contact)sObjectList.get( index )).get(ObjectAndItsIdFieldAPIName[0].ID_Field_API_Name__c)) + ',';   
                        }
                        else if(objectName == 'Account'){
                            successBody += String.valueOf(((Account)sObjectList.get( index )).get(ObjectAndItsIdFieldAPIName[0].ID_Field_API_Name__c)) + ',';   
                        }                        
                        
                        successBody += '"'+'Successfully Updated' + '"';  
                        successBody += '\n';
                    }
                }             
                
                String errorCsv = objectName+' Id, Error Message \n' ;
                
                if(errorBody != null && errorBody != ''){
                    errorCsv = errorCsv + errorBody; 
                }
                
                String successCsv = objectName+' Id, Status \n' ;
                if(successBody != null && successBody != ''){                   
                    successCsv = successCsv + successBody;
                }
                
                if(Test.isRunningTest()){
                    ApexUtilityClass.sendEmailForLogs(errorCsv,successCsv,objectName,emailSubject); 
                }
                else{                    
                    if(sendEmail == true && noOfErrors > 0){
                        ApexUtilityClass.sendEmailForLogs(errorCsv,successCsv,objectName,emailSubject);
                    } 
                }
                
                
                Application_Log__c applicationLog = new Application_Log__c();               
                applicationLog.Log_Message__c = 'No. Of Success Records : '+ noOfSuccess +'\n'+ 'No. Of Failed Records : '+ noOfErrors+'\n'+'Please find attached files for more details.';
                applicationLog.Log_Type__c='Process Log';
                
                if(applicationLog != null){                   
                    insert applicationLog;
                }
                
                //Attach Success Records and Failed Records Details Files to Applicatiion Log Record.
                List<ContentVersion> contentVersions = new List<ContentVersion>();
                Blob successRecordsContentBlob =Blob.valueOf(successCsv);
                
                ContentVersion successRecordsFile = new ContentVersion();
                successRecordsFile.title = objectName+' Success File '+system.now();
                successRecordsFile.VersionData = successRecordsContentBlob;
                successRecordsFile.PathOnClient = objectName+' Success File.txt';
                contentVersions.add(successRecordsFile);                
                
                Blob failedRecordsContentBlob = Blob.valueOf(errorCsv);
                
                ContentVersion errorRecordsFile = new ContentVersion();
                errorRecordsFile.title =  objectName+' Error File '+system.now();  
                errorRecordsFile.VersionData =failedRecordsContentBlob;  
                errorRecordsFile.PathOnClient = objectName+' Error File.txt';
                contentVersions.add(errorRecordsFile);
                
                insert contentVersions;
                //Query content versions to get the content document ids
                List<ContentVersion> contentList = [SELECT id, ContentDocumentId FROM ContentVersion where Id IN:contentVersions];
                List<ContentDocumentLink> contentLinks = new List<ContentDocumentLink>();
                
                if(contentList!=Null && contentList.size()>0){
                    for(Integer j =0;j<contentList.size();j++){
                        ContentDocumentLink contentlink=new ContentDocumentLink();
                        contentlink.LinkedEntityId=applicationLog.id;
                        contentlink.ShareType= 'v';
                        contentlink.ContentDocumentId=contentList[j].ContentDocumentId;
                        contentlink.Visibility = 'AllUsers';
                        contentLinks.add(contentlink);
                    }
                    //Insert content document links. Associate each document with inserted Application Log
                    insert contentLinks;  
                }
            } 
        }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','ApexUtilityClass','handleDMLResults');
            result.getReturnValue(); // Log Exception
        }
    }
    
    
    
    
    //Send Email For Logs 
    public static void sendEmailForLogs(String errorCsv,String successCsv,String objectName,String emailSubject){
        String emailRecipients = system.label.Database_SaveResult_Email_Recipients; 
        String errorString = '';        
        
        system.debug('***in errorCsv');
        String csvname= objectName+' Error File.csv';
        Messaging.EmailFileAttachment errorCsvAttc = new Messaging.EmailFileAttachment();
        errorCsvAttc.setFileName(csvname);
        errorCsvAttc.setBody(Blob.valueOf(errorCsv));
        
        Messaging.EmailFileAttachment successCsvAttc = new Messaging.EmailFileAttachment();
        successCsvAttc.setFileName(objectName+' Success File.csv');
        successCsvAttc.setBody(Blob.valueOf(successCsv));
        list<string> toAddresses = new list<string>();          
        
        if(emailRecipients != null){                       
            toAddresses.addAll(emailRecipients.split(','));
            system.debug('***toAddresses'+toAddresses);
        }
        
        
        String subject = emailSubject;    
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setToAddresses(toAddresses );
        email.setPlainTextBody(errorString );
        email.setPlainTextBody('PFA success and error file attached.');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{errorCsvAttc,successCsvAttc});
        
        try{
            system.debug('***in try block send email');
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        } catch(Exception error){
            system.debug('***in try block send email');
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','ApexUtilityClass','sendEmailForLogs');
            result.getReturnValue(); // Log Exception
        }            
        
    }    
}