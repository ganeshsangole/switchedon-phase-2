/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: BatchCompanyOnbaording used for lead conversion
History
10-07-2021 Tranzevo Initial Release for Trade Worker Induction Process 
----------------------------------------------------------------------------------------------*/
global class BatchCompanyOnbaording implements Database.Batchable<sObject>,Database.Stateful{
    Set<Id> setLeads = new set<Id>(); 
    Set<Id> convertedLeads = new Set<ID>();

    public BatchCompanyOnbaording(Set<Id> setLeadIds) {
        system.debug('setLeadIds line 6 in batch'+setLeadIds);
        setLeads = setLeadIds;
    }

    global Database.queryLocator start(Database.BatchableContext bc){
        String query= 'Select Id,First_Name__c, SuppliedEmail, Last_Name__c,Region__c,Any_Special_Requirements__c ,Legal_Entity__c , SuppliedCompany,Trade_Type__c, CreatedDate, ClosedDate, Lead__c, Lead__r.FirstName, Lead__r.LastName, Lead__r.Email, Lead__r.Phone, Lead__r.MobilePhone , Bank_Account_Name__c, Bank_Account_Number__c, NZBN_Number__c, Physical_Address__c, Physical_Address_City__c, Physical_Address_Postal__c, Physical_Address_Suburb__c, Postal_Address__c, Postal_Address_Postal_Code__c, Postal_Address_City__c, Postal_Address_Suburb__c, GST_Number__c, Public_Liability_Expiry__c, Public_Liability_Insurer__c, Public_Liability_Policy_No__c, Public_Liability_Value_Insured__c, Vehicle_Insurance_Expiry__c,Vehicle_Insurance_Insurer__c, Vehicle_Insurance_Policy_No__c,ERIC_Contact_Name__c, Safetyhub_profile__c, ERIC_Web_Portal_User__c, H_S_folder__c, VAULT_login_details__c from CASE where Lead__c IN:setLeads';
        System.debug('Query'+query);
        return Database.getQueryLocator(query);    
    }
    
    global void execute (Database.BatchableContext BC, List<Case> listCases){
        System.debug('*****listCases'+listCases);
         set<Id> caseIds = new set<ID>();
         list<Account> listAccounts = new list<Account>();
         //Map<Id,List<Case_Contact__c>> mapCaseIdKeyContacts = new Map<Id,List<Case_Contact__c>>();
         //Map<Id,List<Contact>> mapCaseListContacts = new Map<Id,List<Contact>>();
         List<Contact> listKeyContacts = new List<Contact>();
         Map<String,Id> mapEmailLeadId = new Map<String,Id>();
         Map<Id,Id> mapLeadContactId = new Map<Id,Id>();
         Map<Id,Account> mapLeadIdAccount = new Map<Id,Account>();
         Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Trade').getRecordTypeId();
         Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Trade').getRecordTypeId();
         list<lead> LeadList = new List<lead>();
         
        try{
         for(Case thisCase :listCases){
             caseIds.add(thisCase.Id);
             if(thisCase.Lead__c != null && thisCase.Lead__r.Email != null){
                 mapEmailLeadId.put(thisCase.Lead__r.Email,thisCase.Lead__c);
             }
         }
         
         for(Case thisCase :listCases){
             Account thisAccount = new Account();
             
                 thisAccount.Name = thisCase.SuppliedCompany;
                 thisAccount.Bank_Account_Name__c = thisCase.Bank_Account_Name__c;
                 thisAccount.Bank_Account_Number__c = thisCase.Bank_Account_Number__c;
                 thisAccount.NZBN_Number__c = thisCase.NZBN_Number__c;
                 thisAccount.GST_Number__c = thisCase.GST_Number__c;
                 if(thisCase.Postal_Address_Suburb__c != null){
                     thisAccount.BillingStreet = thisCase.Postal_Address__c + ' ' + thisCase.Postal_Address_Suburb__c;
                 }else{
                     thisAccount.BillingStreet = thisCase.Postal_Address__c;
                 }
                 thisAccount.BillingCountry = 'New Zealand';
                 thisAccount.BillingCity = thisCase.Postal_Address_City__c;
                 thisAccount.BillingPostalCode = thisCase.Postal_Address_Postal_Code__c;
                 if(thisCase.Physical_Address_Suburb__c != null){
                     thisAccount.ShippingStreet = thisCase.Physical_Address__c + ' ' + thisCase.Physical_Address_Suburb__c;
                 }else{
                     thisAccount.ShippingStreet = thisCase.Physical_Address__c;                 
                 }
                 thisAccount.ShippingCity = thisCase.Physical_Address_City__c;
                 thisAccount.ShippingCountry = 'New Zealand';
                 thisAccount.ShippingPostalCode = thisCase.Physical_Address_Postal__c;
                 thisAccount.Trade_Type__c = thisCase.Trade_Type__c;
                 thisAccount.Public_Liability_Expiry__c = thisCase.Public_Liability_Expiry__c;
                 thisAccount.Public_Liability_Insurer__c = thisCase.Public_Liability_Insurer__c;
                 thisAccount.Public_Liability_Policy_No__c = thisCase.Public_Liability_Policy_No__c;
                 thisAccount.Public_Liability_Value_Insured__c = thisCase.Public_Liability_Value_Insured__c;
                 thisAccount.Vehicle_Insurance_Expiry__c = thisCase.Vehicle_Insurance_Expiry__c;
                 thisAccount.Vehicle_Insurance_Insurer__c = thisCase.Vehicle_Insurance_Insurer__c;
                 thisAccount.Vehicle_Insurance_Policy_No__c = thisCase.Vehicle_Insurance_Policy_No__c;
                 thisAccount.RecordTypeId = accountRecordTypeId;
                 thisAccount.Active__c = true;
                 thisAccount.Date_commenced_with_SOH__c = Date.valueof(thisCase.ClosedDate);
                 thisAccount.Region__c = thisCase.Region__c;
                 thisAccount.ERIC_Contact_Name__c = thisCase.ERIC_Contact_Name__c;
                 thisAccount.Safetyhub_profile__c = thisCase.Safetyhub_profile__c;
                 thisAccount.ERIC_Web_Portal_User__c = thisCase.ERIC_Web_Portal_User__c;
                 thisAccount.H_S_folder__c = thisCase.H_S_folder__c;
                 thisAccount.VAULT_login_details__c = thisCase.VAULT_login_details__c;
                 thisAccount.Any_Special_Requirements__c = thisCase.Any_Special_Requirements__c;
                 thisAccount.Legal_Entity__c = thisCase.Legal_Entity__c;
                 mapLeadIdAccount.put(thisCase.Lead__c,thisAccount);
         }
         
         system.debug('******mapLeadIdAccount'+mapLeadIdAccount);
         
         if(mapLeadIdAccount != null){
             Database.SaveResult[] accountsSR = Database.insert(mapLeadIdAccount.values(),false);
             system.debug('******mapLeadIdAccountValue****'+mapLeadIdAccount.values());
         }
         
         system.debug('******mapLeadIdAccount'+mapLeadIdAccount);
         system.debug('*****caseIds'+caseIds);
         
         if(!caseIds.isEmpty()){
             for(Case_Contact__c keyContact: [Select Id, Case__c,Case__r.Lead__c, Contact_Type__c, Salutation__c, First_Name__c, Last_Name__c, Email__c, Phone__c from Case_Contact__c  where Case__c IN:caseIds]){
                 Id accountRecId = mapLeadIdAccount.get(keyContact.Case__r.Lead__c).Id;
                 system.debug('******accountRecId'+accountRecId);
                 Contact tempContact = new Contact();
                      tempContact.firstName = keyContact.First_Name__c;
                      tempContact.lastName = keyContact.Last_Name__c;
                      tempContact.Email = keyContact.Email__c;
                      tempContact.MobilePhone = keyContact.Phone__c;
                      tempContact.Phone = keyContact.Phone__c;
                      tempContact.Contact_Type__c = keyContact.Contact_Type__c;
                      tempContact.RecordTypeId = contactRecordTypeId;
                      tempContact.accountId = accountRecId;
                      tempContact.Is_Active__c = true;
                 listKeyContacts.add(tempContact);
             }//END OF FOR
         }//END OF IF

         system.debug('********listKeyContacts'+listKeyContacts);
         if(!listKeyContacts.isEmpty()){
             Database.DMLOptions dml = new Database.DMLOptions();
             dml.DuplicateRuleHeader.AllowSave = true; 
             
             Database.SaveResult[] contactSR = Database.insert(listKeyContacts,dml);
             system.debug('*****listKeyContacts'+listKeyContacts);
             system.debug('*****contactSR'+contactSR);
             
             for(Contact thisContact: listKeyContacts){
                 if(mapEmailLeadId != null && mapEmailLeadId.containsKey(thisContact.Email)){
                     mapLeadContactId.put(mapEmailLeadId.get(thisContact.Email),thisContact.Id);
                 }         
             }
             
             system.debug('******mapLeadContactId'+mapLeadContactId);
             
         }
         
         if(mapLeadContactId != null && mapLeadIdAccount != null){
             LeadList = [ Select Id,firstname,LastName,email,Status, IsConverted, Converted_Lead__c from Lead where ID IN :mapLeadIdAccount.keyset()];
             
         
             List<Database.LeadConvert> massLeadconvert = new List<Database.LeadConvert>();
             LeadStatus CLeadStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE  IsConverted=true Limit 1];
             for(Lead thisLead: LeadList){
                 if(thisLead.IsConverted  != true && thisLead.Converted_Lead__c == true){
                     Database.LeadConvert Leadconvert = new Database.LeadConvert();
                     Leadconvert.setLeadId(thisLead.Id);  
                     Leadconvert.setAccountID(mapLeadIdAccount.get(thisLead.Id).Id);
                     Leadconvert.setContactID(mapLeadContactId.get(thisLead.Id));              
                     Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
                     Leadconvert.setDoNotCreateOpportunity(TRUE); 
                     
                     system.debug('******Leadconvert'+Leadconvert);
                     MassLeadconvert.add(Leadconvert);
                 }
             }
             
              if (!MassLeadconvert.isEmpty()) {
                List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
                system.debug('***lcr'+lcr);
                for(Database.LeadConvertResult thisResult: lcr){
                    system.debug('*****thisResult.getAccountId****'+thisResult.getAccountId());
                    system.debug('*****thisResult.getContactId****'+thisResult.getContactId());
                    system.debug('*****thisResult.getLeadId****'+thisResult.getLeadId());
                    system.debug('*****isSuccess****'+thisResult.isSuccess());
                    
                    if(thisResult.isSuccess()){
                        convertedLeads.add(thisResult.getLeadId());                    
                    }
                    
                    for(Database.error thisError: thisResult.getErrors()){
                        system.debug('*****thisError****'+thisError);                
                    }
                 }
                 
                 system.debug('******convertedLeads'+convertedLeads);
              } 
         
         }
    
     }catch(Exception e){
          SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','BatchCompanyOnbaording','execute');
            result.getReturnValue();
      } 

         
    }

    global void finish(Database.BatchableContext BC){
         ID batchprocessid = Database.executebatch(new BatchUpdateCases(convertedLeads),1);
         system.debug('***batchprocessid'+batchprocessid);
    }
}