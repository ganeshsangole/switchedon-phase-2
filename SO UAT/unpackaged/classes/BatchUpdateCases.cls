global class BatchUpdateCases implements Database.Batchable<sObject>,Database.Stateful{
    Set<Id> setLeads = new set<Id>(); 
    
    public BatchUpdateCases(Set<Id> setLeadIds) {
        setLeads = setLeadIds;
    }

    global Database.queryLocator start(Database.BatchableContext bc){
        String query= 'Select Id, Lead__c, lead__r.ConvertedAccountId, lead__r.ConvertedContactId from Case where Lead__r.IsConverted = true and Lead__c IN:setLeads';
        System.debug('Query'+query);
        return Database.getQueryLocator(query);    
    }
    
    global void execute (Database.BatchableContext BC, List<Case> listCases){
        list<Case> listCaseUpdates = new list<Case>();
        for(Case thisCase: listCases){
            thisCase.AccountId = thisCase.lead__r.ConvertedAccountId;
            thisCase.ContactId = thisCase.lead__r.ConvertedContactId;
            listCaseUpdates.add(thisCase);            
        }
        
        if(!listCaseUpdates.isEmpty()){
            update listCaseUpdates;
        }
         
    }

    global void finish(Database.BatchableContext BC){
            
    }
}