/*----------------------------------------------------------------------------------------------
* Class : BatchUpdateContactTask
* Description: Batch to update bounce email contact details on Task
* @author: Tranzevo
* Date: 17/05/2020
----------------------------------------------------------------------------------------------*/

global class BatchUpdateContactTask implements Database.Batchable<sObject>,Database.Stateful{
    global Database.queryLocator start(Database.BatchableContext bc){
        String UpdateContactTaskclause= System.Label.Update_Contact_Task_where_clause;
        String query=' Select Id,IsEmailBounced,EmailBouncedReason,EmailBouncedDate,' +'(Select Id,Bounced_Reason__c,Emil_Bounced_Date__c,Is_Email_Bounced__c,Subject,CreatedDate from Tasks ' + UpdateContactTaskclause  + ' ) ' +' from Contact ' + ' where EmailBouncedDate = TODAY ';
        System.debug('Query'+query);
        return Database.getQueryLocator(query);    
    }
    global void execute (Database.BatchableContext BC, List<Contact> conList){
        List<Task> tk = new List<Task>();
        for(Contact con:conList)
        {
            for(Task taskfield:con.Tasks)
            {
                taskfield.Bounced_Reason__c = con.EmailBouncedReason;
                taskfield.Emil_Bounced_Date__c = con.EmailBouncedDate;
                taskfield.Is_Email_Bounced__c = con.IsEmailBounced;
                tk.add(taskfield);
                system.debug('Task value'+ taskfield);
            }
        }	  
        if(tk.size() > 0){
            update tk;
        }
        system.debug('Task id**********'+ tk.size());
        system.debug('Test debug***************'); 
    }
    public void finish(Database.BatchableContext BC){
        
        // Get the ID of the AsyncApexJob representing this batch job from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =
                          :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        BatchClassEmailNotification__c bcen = BatchClassEmailNotification__c.getInstance();
        Boolean sendnotificaitonField = bcen.SendNotification__c;
        system.debug('sendnotificaitonField'+sendnotificaitonField);
        
        if(sendnotificaitonField== TRUE){
            system.debug('Inside loop for sending mail');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            mail.setPlainTextBody
                ('The batch Apex job processed ' + a.TotalJobItems +
                 ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}