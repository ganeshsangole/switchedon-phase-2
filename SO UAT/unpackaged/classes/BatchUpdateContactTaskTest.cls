/*----------------------------------------------------------------------------------------------
* Class : BatchUpdateContactTaskTest
* Description: Test class for BatchUpdateContactTask
* @author: Tranzevo
* Date: 17/05/2020
----------------------------------------------------------------------------------------------*/
@isTest
private class BatchUpdateContactTaskTest {
    @testSetup
    static void setup() {
        
        BatchClassEmailNotification__c cs = new BatchClassEmailNotification__c();
        cs.SendNotification__c=TRUE;
        insert cs;
        
        TestDataFactory.AccountParams aParms = new TestDataFactory.AccountParams();
        aParms.Name = 'testAccount';
        List<Account> acclist = TestDataFactory.createTestAccounts(1,aParms);       
        insert acclist;
        
        TestDataFactory.ContactParams cParms = new TestDataFactory.ContactParams();
        cParms.accountId = acclist[0].Id;
        cParms.firstName = 'Test first name';
        cParms.email = 'k51@tranzevo.com';
        cParms.lastName = 'TestLast Name';
        cParms.emailbounceddate = datetime.now();
        cParms.emailBouncedReason = 'This email bounced';
        
        List<Contact> conlist = TestDataFactory.createTestContacts(1,cParms);       
        insert conlist;
        system.debug('cont insert'+conlist);
        
        TestDataFactory.CampaignParams campParms = new TestDataFactory.CampaignParams();
        campParms.campaignName = 'Health and Safety campaign';
        
        List<Campaign> camplist = TestDataFactory.createTestCampaigns(1,campParms);       
        insert camplist;
        
        TestDataFactory.CampaignMembersParams campmemParms = new TestDataFactory.CampaignMembersParams();
        campmemParms.campaignId = camplist[0].Id;
        campmemParms.contactId=conlist[0].Id;
        
        List<CampaignMember> campmemlist = TestDataFactory.createTestCampaignMembers(1,campmemParms);       
        insert campmemlist;
		
        TestDataFactory.TaskParams taskParms = new TestDataFactory.TaskParams();
        taskParms.subject = 'Mass Email: Donni';
        taskParms.callType='Outbound';
        taskParms.activityDate = date.today();
        taskParms.status = 'New';
        taskParms.priority = 'Normal';
        taskParms.whoId = conlist[0].Id;
        
        List<Task> tasklist = TestDataFactory.createTestTask(1,taskParms);       
        insert tasklist;
        
    }
    @isTest static void test() {
        Test.startTest();
        BatchUpdateContactTask bt = new BatchUpdateContactTask();
        Id batchId = Database.executeBatch(bt,200);
        Test.stopTest();
    }
}