/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Case Trigger Handler used to create trade contacts,validate onboarding task completion, 
             validate KO approval form and create content document link
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class CaseTriggerHandler{
    public static void handleMethod(List<Case> newList, Map<Id,Case> oldMap){
        if(Trigger.isBefore){
            if(Trigger.isUpdate){
                //Validate if KO Approval File is uploaded on the Case
                CaseTriggerHelper.validateKOApprovalForm(newList,oldMap);   
                //Validate if tasks are completed before closing the Case
                CaseTriggerHelper.validateOnboardingTaskCompletion(newList,oldMap);      
            }        
        }
        
        if(Trigger.isAfter){
           if(Trigger.isUpdate){
              //Create Trade Contacts once MOJ Request is Approved 
              CaseTriggerHelper.createTradeContacts(newList,oldMap);
              //Associate the Photograph with Trade Contact
              CaseTriggerHelper.createContentDocumentLink(newList,oldMap);
           }
        }
    }//END OF METHOD 
}//END OF CLASS