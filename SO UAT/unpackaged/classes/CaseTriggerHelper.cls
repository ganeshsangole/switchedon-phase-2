/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Case Trigger Helper used to create trade contacts,validate onboarding task completion, 
             validate KO approval form and create content document link
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class CaseTriggerHelper{

    //This method will be used to create Trade Contacts
    public static void createTradeContacts(List<Case> newList, Map<Id,Case> oldMap){
    List<Case> listApprovedCases = new List<Case>();
    Map<String,ID> mapContactEmailCaseId = new Map<String,ID>();
    //Map<ID,Contact> mapCaseIDContact = new Map<ID,Contact>();
    list<Contact> listTradeWorkerContacts = new list<Contact>();
    list<Contact> listSuccessfulContacts = new List<Contact>();
    list<Case> listCasesToUpdate = new List<Case>();
    String tradeWorkerOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Worker_Onboarding').getRecordTypeId();
        try{
        for(Case thisCase: newList){
           if(thisCase.MOJ_Approved__c == true && oldMap.get(thisCase.Id).MOJ_Approved__c != true && thisCase.RecordTypeId == tradeWorkerOnboardingRecordType ){
              listApprovedCases.add(thisCase);     
           }       
       }
       system.debug('******listApprovedCases'+listApprovedCases);
       if(!listApprovedCases.isEmpty()){
       String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
           for(Case thisCase: listApprovedCases){
              Contact tempContact = new Contact();
              tempContact.RecordTypeId = tradeWorkerRecordTypeId;
              tempContact.is_Active__c = false;
              tempContact.Contact_Type__c = 'Worker';
              tempContact.firstName = thisCase.First_Name__c;
              tempContact.lastname = thisCase.Last_Name__c;
              tempContact.Email = thisCase.SuppliedEmail;
              tempContact.Phone = thisCase.SuppliedPhone;
              tempContact.AccountId = thisCase.AccountId;
              tempContact.Gender__c = thisCase.Gender__c;
              tempContact.Ethnicity__c = thisCase.Ethnicity__c;
              tempContact.Birthdate = thisCase.Date_of_Birth__c;
              tempContact.Job_Title__c = thisCase.Job_Title__c;
              tempContact.Work_Immigration_Status__c = thisCase.Work_Immigration_Status__c;
              tempContact.Base_Location__c = thisCase.Base_Locations__c;
              tempContact.Trade_Qualifications_Registrations__c = thisCase.Trade_Qualifications_Registrations__c;
              tempContact.Trade_Qualifications_Registration_No__c = thisCase.Trade_Qualifications_Registration_No__c;
              tempContact.Working_under_supervision_of__c = thisCase.Working_under_supervision_of__c;
              tempContact.Working_Under_Supvson_Of_Registration_No__c = thisCase.Working_Under_Supvson_Of_Registration_No__c;
              tempContact.Work_Management_Reference__c = thisCase.Work_Management_Reference__c;
              tempContact.Site_Safe_Qualifications_or_Equivalent__c = thisCase.Site_Safe_Qualifications_or_Equivalent__c;
              tempContact.Site_Safe_Qualifications_Registration_No__c = thisCase.Site_Safe_Qualifications_Registration_No__c;
              tempContact.Apprenticeship_Certification_Types__c = thisCase.Apprenticeship_Certification_Types__c;
              tempContact.Year_Level__c = thisCase.Year_Level__c;
              tempContact.Expiry_Date__c = thisCase.Expiry_Date__c;
              tempContact.Visa_Expiry_Date__c = thisCase.Visa_Expiry_Date__c;
              tempContact.Is_a_Direct_Employee__c = thisCase.Is_a_Direct_Employee__c;
              tempContact.Do_you_have_a_Criminal_History_Record__c = thisCase.Do_you_have_a_Criminal_History_Record__c;
              tempContact.Obligation_under_HSAW_Act_2015__c = thisCase.Obligation_under_HSAW_Act_2015__c;
              tempContact.Inducted_through_internal_systems_fami__c = thisCase.Inducted_through_internal_systems_fami__c;
              tempContact.Is_KO_tenant_family_members_in_KO_prop__c = thisCase.Is_KO_tenant_family_members_in_KO_prop__c;
              tempContact.Is_Construction_ready__c = thisCase.Is_Construction_ready__c;
              tempContact.Trade_Type__c = thisCase.Trade_Type__c;
              tempContact.Gold_Key__c = thisCase.Gold_Key__c;
              //tempContact.Date_of_Induction_Completed__c = thisCase.Date_of_Induction_Completed__c;
              //tempContact.Induction_Completed_By__c = thisCase.Induction_Completed_By__c;
              tempContact.Relationship_to_Person_1__c = thisCase.Relationship_to_Person_1__c;
              tempContact.Relationship_to_Person_3__c = thisCase.Relationship_to_Person_3__c;
              tempContact.Relationship_to_Person_2__c = thisCase.Relationship_to_Person_2__c;
              tempContact.Address_1__c = thisCase.Address_1__c;
              tempContact.Address_2__c = thisCase.Address_2__c;
              tempContact.Address_3__c = thisCase.Address_3__c;
              tempContact.Photograph_Content_Version_Id__c = thisCase.Photo_Version_Id__c;
              if(thisCase.Postal_Address_Suburb__c != null){
                     tempContact.MailingStreet = thisCase.Postal_Address__c + ' ' + thisCase.Postal_Address_Suburb__c;
                 }else{
                     tempContact.MailingStreet = thisCase.Postal_Address__c;
                 }
                 tempContact.MailingCountry = 'New Zealand';
                 tempContact.MailingCity = thisCase.Postal_Address_City__c;
                 tempContact.MailingPostalCode = thisCase.Postal_Address_Postal_Code__c;
              
              
              listTradeWorkerContacts.add(tempContact);
              mapContactEmailCaseId.put(thisCase.SuppliedEmail,thisCase.ID);
           }
           system.debug('******listTradeWorkerContacts'+listTradeWorkerContacts); 
           if(!listTradeWorkerContacts.isEmpty()){
                 Database.DMLOptions dml = new Database.DMLOptions();
                 dml.DuplicateRuleHeader.AllowSave = true;                  
                 Database.SaveResult[] contactSR = Database.insert(listTradeWorkerContacts,dml);
                 system.debug('*****listTradeWorkerContacts'+listTradeWorkerContacts);
                 system.debug('*****contactSR'+contactSR);
                 
                 for(Integer i = 0; i < contactSR.size(); i++){
                     if(contactSR[i].isSuccess()){
                         listSuccessfulContacts.add(listTradeWorkerContacts[i]);
                     }
                 }
                 
                system.debug('******listSuccessfulContacts'+listSuccessfulContacts); 
                
                if(!listSuccessfulContacts.isEmpty()){
                    for(Contact thisContact :listSuccessfulContacts){
                        if(mapContactEmailCaseId.containsKey(thisContact.email)){
                            Case thisCase = new Case();
                            thisCase.Id = mapContactEmailCaseId.get(thisContact.email);
                            thisCase.Trade_Worker__c = thisContact.Id;
                            thisCase.Status = 'Induction - To be scheduled';
                            listCasesToUpdate.add(thisCase);
                        }
                    }  
                   if(!listCasesToUpdate.isEmpty()){
                       update listCasesToUpdate;
                   }//END OF listCasesToUpdate                  
                }               
           }           
       }//END OF IF
       }
       catch(Exception e){
       SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','CaseTriggerHelper','createTradeContacts');
            result.getReturnValue();
      } 
       
    }//END OF createTradeContacts METHOD
    
    //Method to validate if the tasks are completed before closing the case.
    public static void validateOnboardingTaskCompletion(list<Case> newList, Map<Id,Case> oldMap){
       list<Case> listCases = new list<Case>(); 
       Set<Id> setCaseIds = new Set<Id>();
       Set<ID> setCasesWithOpenTasks = new Set<ID>();
       String tradeCompanyOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Company_Onboarding').getRecordTypeId();
       String tradeWorkerOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Worker_Onboarding').getRecordTypeId();
       
        try{
       for(Case thisCase: newList){
            if(thisCase.Status == 'Closed-Approved' && oldMap.get(thisCase.Id).Status != 'Closed-Approved' && (thisCase.RecordTypeId == tradeCompanyOnboardingRecordType || thisCase.RecordTypeID == tradeWorkerOnboardingRecordType)){
                listCases.add(thisCase);
                setCaseIds.add(thisCase.Id);
            }//END OF IF        
        }//END OF FOR
        
        if(!setCaseIds.isEmpty()){
            for(Task thisTask: [Select Id,Status,WhatId from task where status != 'Completed' and whatId IN :setCaseIds]){
                if(thisTask.WhatId != null){
                  setCasesWithOpenTasks.add(thisTask.WhatId);  
                }//END OF IF
            }//END OF FOR
            
            if(!listCases.isEmpty()){
                for(Case thisCase: listCases){
                    if(setCasesWithOpenTasks.contains(thisCase.Id)){
                        thisCase.addError(system.label.Open_Tasks_Error_Message);
                    }
                }
            }
        
        }//END OF IF
        }
        catch(Exception e){
        SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','CaseTriggerHelper','validateOnboardingTaskCompletion');
            result.getReturnValue();
      } 
    }//END OF METHOD
    
    
    //Method to create a contentdocumentlink to associate the trade worker photograph with trade contact
    public static void createContentDocumentLink(list<Case> newList, Map<Id,Case> oldMap){
    Map<ID,ID> mapContentVersionIdContactId = new Map<ID,ID>();
    List<ContentDocumentLink> listContentDocLink = new List<ContentDocumentLink>();
        //Filter all the relavant records.
        try{
        for(Case thisCase: newList){
            if(thisCase.Trade_Worker__c != null &&  oldMap.get(thisCase.Id).Trade_Worker__c == null && thisCase.Photo_Version_Id__c != null){
               list<String> listParts = thisCase.Photo_Version_Id__c.split('/');
               if(!listParts.isEmpty()){
                   String contentVersionId = listParts[listParts.size()-1];
                   mapContentVersionIdContactId.put(contentVersionId,thisCase.Trade_Worker__c);
               }//END OF IF
          }//END OF IF        
        }//END OF FOR
        
        if(mapContentVersionIdContactId != null && !mapContentVersionIdContactId.keyset().isEmpty()){
            for(ContentVersion thisVersion: [Select Id,ContentdocumentId from ContentVersion where ID IN :mapContentVersionIdContactId.keyset()]){
                //Create Content Document links for Trade contact records
                ContentDocumentLink tempRec = new ContentDocumentLink();
                tempRec.LinkedEntityId = mapContentVersionIdContactId.get(thisVersion.Id); // Account ID
                tempRec.ContentDocumentId = thisVersion.ContentdocumentId; //Content Dcoument ID
                tempRec.ShareType = 'V';
                tempRec.Visibility = 'InternalUsers';
                listContentDocLink.add(tempRec);
            }//END OF FOR
            
            //Insert Content Document Links
            if(!listContentDocLink.isEmpty()){
                insert listContentDocLink;
            }//END OF IF
        }//END OF IF
        }
        catch(Exception e){
      SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','CaseTriggerHelper','createContentDocumentLink');
            result.getReturnValue();
      } 
        
    }//END OF METHOD
    
    //This method will be used to validate KO approval form
    public static void validateKOApprovalForm(list<Case> newList, Map<Id, Case> oldMap){
        list<Case> listCases = new list<Case>();
        Set<ID> setCaseIds = new Set<ID>();
        Set<ID> setCasesWithKOForm = new Set<ID>();
        
        Set<String> fileTitle = new Set<String>();
        String strfileTitle = system.label.KOApprovalFileTitle;
        for(String title: strfileTitle.split(';')){
        fileTitle.add(title);
        }
        
        String tradeCompanyOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Company_Onboarding').getRecordTypeId();
    
        try{
        for(Case thisCase: newList){
           if(thisCase.Status == 'Pending KO Approval' && oldMap.get(thisCase.Id).Status != 'Pending KO Approval' && thisCase.RecordTypeID == tradeCompanyOnboardingRecordType){
              listCases.add(thisCase);
              setCaseIds.add(thisCase.Id);
           }//END OF IF   
        }//END OF FOR
        
        if(!setCaseIds.isEmpty()){
            for(ContentDocumentLink thisLink: [Select Id,ContentDocument.Title,ContentDocumentID,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN:setCaseIds and ContentDocument.Title like : fileTitle]){
                setCasesWithKOForm.add(thisLink.LinkedEntityId);
            }//END OF FOR     
            
            for(Case thisCase: listCases){
                if(!setCasesWithKOForm.contains(thisCase.ID)){
                    thisCase.addError(system.label.KO_Approval_File_Upload_Error_Message);
                }//END OF IF
            }//END OF FOR
        }//END OF IF
        }
        catch(Exception e){
      SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','CaseTriggerHelper','validateKOApprovalForm');
            result.getReturnValue();
      } 
    }//END OF METHOD
}//END OF CLASS