/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Contact Clone Controller used to clone an inactive contact
History
10-07-2021 Tranzevo Initial Release 
----------------------------------------------------------------------------------------------*/
public class ContactCloneController {
    
    public static String firstname;
    public static String lastname; 
    public static String email;
    public static String accountId;
    
    //Method used to fetch existing contact details
    @AuraEnabled
    public static tableWrapper fecthExistingValues (string recordId ){
        
        List<Contact> lstContactDetails =  new List<Contact>();
        //get the contact details and send to component
       
        lstContactDetails = [Select Id,AccountId,
                             FirstName,
                             LastName,Email
                             From Contact
                             Where Id =: recordId];
        for(Contact data:lstContactDetails)
        {
            firstname=data.FirstName;
            lastname=data.LastName;
            email=data.Email.removeEnd('.inactive');
        }
        
        
        tableWrapper tWrapper = new tableWrapper();
        
        tWrapper.firstname=firstname;
        tWrapper.lastname=lastname;
        tWrapper.email=email;
        tWrapper.accountId=accountId;
        
        return tWrapper;
    }
    
    //Method used to Clone Contact
    @AuraEnabled
    public static String cloneContact (string recordId,String firstname, String lastname, String email,String accountId){
        system.debug('Inside clone contact function');
        List<Contact> lstContactDetails =  new List<Contact>();
        List<Contact> contactDataList=new List<Contact>();
        List<Id> listOfIds = new List<Id>();
        String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
        
        try{
        lstContactDetails = [Select Id,Contact_Type__c,MobilePhone,Phone,Gender__c,Job_Title__c,Ethnicity__c,Base_Location__c,
                             Gold_Key__c,Notes__c,Trade_Type__c,Trade_Skills__c,Trade_Qualifications_Registrations__c,Trade_Qualifications_Registration_No__c,
                             Working_under_supervision_of__c,Working_Under_Supvson_Of_Registration_No__c,Site_Safe_Qualifications_or_Equivalent__c,Site_Safe_Qualifications_Registration_No__c,
                             Apprenticeship_Certification_Types__c,Expiry_Date__c,Work_Immigration_Status__c,Visa_Expiry_Date__c,
                             Is_a_Direct_Employee__c,Do_you_have_a_Criminal_History_Record__c,Obligation_under_HSAW_Act_2015__c,Is_Construction_ready__c,Inducted_through_internal_systems_fami__c,
                             Is_KO_tenant_family_members_in_KO_prop__c,Relationship_to_Person_1__c,Relationship_to_Person_2__c,Relationship_to_Person_3__c,Address_1__c,
                             Address_2__c,Address_3__c,Induction_Completed_By__c,Photograph_Content_Version_Id__c	
                             From Contact
                             Where Id =: recordId limit 1];
        for(Contact data:lstContactDetails)
        {
            Contact con =new Contact();
            con.FirstName= firstname;
            con.LastName =lastname;
            con.Email = email;
            con.AccountId=accountId;
            con.Is_Active__c =true;
            con.Contact_Type__c=data.Contact_Type__c;
            con.MobilePhone=data.MobilePhone;
            con.Phone=data.Phone;
            con.Gender__c=data.Gender__c;
            con.Job_Title__c=data.Job_Title__c;
            con.Ethnicity__c=data.Ethnicity__c;
            con.Base_Location__c=data.Base_Location__c;
            con.Gold_Key__c=data.Gold_Key__c;
            con.Notes__c=data.Notes__c;
            con.Trade_Type__c=data.Trade_Type__c;
            con.Trade_Skills__c=data.Trade_Skills__c;
            con.Trade_Qualifications_Registration_No__c=data.Trade_Qualifications_Registration_No__c;
            con.Working_under_supervision_of__c=data.Working_under_supervision_of__c;
            con.Working_Under_Supvson_Of_Registration_No__c=data.Working_Under_Supvson_Of_Registration_No__c;
            con.Site_Safe_Qualifications_or_Equivalent__c=data.Site_Safe_Qualifications_or_Equivalent__c;
            con.Site_Safe_Qualifications_Registration_No__c=data.Site_Safe_Qualifications_Registration_No__c;
            con.Apprenticeship_Certification_Types__c=data.Apprenticeship_Certification_Types__c;
            con.Expiry_Date__c=data.Expiry_Date__c;
            con.Visa_Expiry_Date__c=data.Visa_Expiry_Date__c;
            con.Is_a_Direct_Employee__c=data.Is_a_Direct_Employee__c;
            con.Do_you_have_a_Criminal_History_Record__c=data.Do_you_have_a_Criminal_History_Record__c;
            con.Obligation_under_HSAW_Act_2015__c=data.Obligation_under_HSAW_Act_2015__c;
            con.Is_Construction_ready__c=data.Is_Construction_ready__c;
            con.Inducted_through_internal_systems_fami__c=data.Inducted_through_internal_systems_fami__c;
            con.Is_KO_tenant_family_members_in_KO_prop__c=data.Is_KO_tenant_family_members_in_KO_prop__c;
            con.Relationship_to_Person_1__c=data.Relationship_to_Person_1__c;
            con.Relationship_to_Person_2__c=data.Relationship_to_Person_2__c;
            con.Relationship_to_Person_3__c=data.Relationship_to_Person_3__c;
            con.Address_1__c=data.Address_1__c;
            con.Address_2__c=data.Address_2__c;
            con.Address_3__c=data.Address_3__c;
            con.RecordTypeId=tradeWorkerRecordTypeId;
            con.Trade_Qualifications_Registrations__c = data.Trade_Qualifications_Registrations__c;
            con.Work_Immigration_Status__c = data.Work_Immigration_Status__c;
            con.Date_of_Induction_Completed__c=System.today();
            con.Issue_Date__c=System.today();
            con.Induction_Completed_By__c=data.Induction_Completed_By__c;
            con.Photograph_Content_Version_Id__c=data.Photograph_Content_Version_Id__c;
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.SaveResult sr = Database.insert(con, dml);
            
            if (sr.isSuccess()) {
                listOfIds.add(sr.getId());
                System.debug('Duplicate contact has been inserted in Salesforce!'+listOfIds);
            }
        }
        
        
        List<ContentDocumentLink> conDocLinkList= [Select Id,LinkedEntityId,ContentDocumentId,Visibility,ShareType from ContentDocumentLink where LinkedEntityId=: recordId];
        system.debug('conDocLinkList'+conDocLinkList);
        List<ContentDocumentLink> conDocCloneList=new List<ContentDocumentLink>();
        
        if(listOfIds.size()>0)
        {
            for(ContentDocumentLink condoclinkdata: conDocLinkList){
                system.debug('Inside clone doc loop condoclinkdata'+condoclinkdata);
                ContentDocumentLink conDocLink=new ContentDocumentLink();
                conDocLink.LinkedEntityId=listOfIds[0];
                conDocLink.Visibility= condoclinkdata.Visibility;
                conDocLink.ContentDocumentId=condoclinkdata.ContentDocumentId;
                conDocLink.ShareType=condoclinkdata.ShareType;
                conDocCloneList.add(conDocLink);        
            }
            system.debug('conDocCloneList'+conDocCloneList);
            
            if(conDocCloneList.size() > 0)
            {
                insert conDocCloneList;
            }
        }
        }catch(Exception e){
            
            SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','ContactCloneController','cloneContact');
            result.getReturnValue();
        }
        return listOfIds[0];
    }
    
    public class tableWrapper {
        
        @AuraEnabled
        public String firstname;
        @AuraEnabled
        public String lastname;
        @AuraEnabled
        public String email;
        @AuraEnabled
        public String accountId;
        
    }
}