/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Contact Trigger Handler used to set trade worker Id,set Contact's on Trade account and
             set employee account
History
10-07-2021 Tranzevo Initial Release 
----------------------------------------------------------------------------------------------*/
public class ContactTriggerHandler{
    public static void handleMethod(List<Contact> newContactList, Map<Id,Contact> oldContactMap){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                ContactTriggerHelper.setTradeWorkerId(newContactList);
                ContactTriggerHelper.setEmployeeAccount(newContactList);
            }//END OF IF
        }//END OF IF
        
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                ContactTriggerHelper.setContactsOnTradeAccount(newContactList);
            }        
        }
    }//END OF METHOD 
}//END OF CLASS