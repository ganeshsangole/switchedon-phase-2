/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Contact Trigger Helper used to set trade worker Id,set Contact's on Trade account and
             set employee account
History
10-07-2021 Tranzevo Initial Release 
----------------------------------------------------------------------------------------------*/
public class ContactTriggerHelper{
    
    //This method will be used to the trade Id card auto number for workers
    public static void setTradeWorkerId(list<Contact> listContacts){
        Boolean checkFirstTradeWorker = false;

        List<Contact> listTradeWorkers = new list<Contact>();
        String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
        try{
        for(Contact thisContact :listContacts){
            if(thisContact.RecordTypeId == tradeWorkerRecordTypeId && thisContact.Trade_Auto_No__c == null && thisContact.Contact_Type__c=='Worker'){
                listTradeWorkers.add(thisContact);
            }       
        }//END OF FOR    
        
        if(!listTradeWorkers.isEmpty()){
            list<Contact> listTradeAutoNos = new list<Contact>();
            listTradeAutoNos = [Select Id,Name,Email,Trade_ID_Number__c,Trade_Auto_No__c from Contact where RecordTypeId = :tradeWorkerRecordTypeId and Trade_Auto_No__c != null order by Trade_ID_Number__c desc limit 1];
            system.debug('*****listTradeAutoNos'+listTradeAutoNos );
            Integer lastTradeWorkerSequence;
                if(!listTradeAutoNos.isEmpty()){
                    lastTradeWorkerSequence = Integer.valueof(listTradeAutoNos[0].Trade_ID_Number__c);              
                }else{
                    lastTradeWorkerSequence = Integer.valueof(system.label.Trade_Worker_ID_Sequence);
                }
                system.debug('*****lastTradeWorkerSequence'+lastTradeWorkerSequence);
                
                for(Contact thisContact: listTradeWorkers){
                    String seqNumber;
                    if(lastTradeWorkerSequence == 1 && listTradeAutoNos.isEmpty() && checkFirstTradeWorker == false){
                        lastTradeWorkerSequence = lastTradeWorkerSequence;
                        checkFirstTradeWorker=true;
                    }else{
                        lastTradeWorkerSequence = lastTradeWorkerSequence +  1;
                    }
                    
                    if(String.valueof(lastTradeWorkerSequence).length() == 1 ){
                        seqNumber = '00'+String.valueof(lastTradeWorkerSequence);
                    }else if(String.valueof(lastTradeWorkerSequence).length() == 2){
                        seqNumber = '0'+String.valueof(lastTradeWorkerSequence);  
                    }else{
                        seqNumber = String.valueof(lastTradeWorkerSequence); 
                    }     
                    system.debug('*****seqNumber'+seqNumber);
                    thisContact.Trade_Auto_No__c = seqNumber;                  
                }//END OF FOR
            
        }//END OF IF
        }
        catch(Exception e){
        SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','ContactTriggerHelper','setTradeWorkerId');
            result.getReturnValue();
      } 
        
    }//END OF METHOD
    
    //This method will be used to set the admin,hse,main,primary,quality contact on trade account
    public static void setContactsOnTradeAccount(list<Contact> listContacts){
        
        String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
        list<Account> listAccountUpdate = new List<Account>();
        Map<ID,Map<String,Contact>> mapAccountContacts = new Map<ID,Map<String,Contact>>();
        
        try{
        for(Contact thisContact: listContacts){
            if(thisContact.Contact_Type__c != null && thisContact.RecordTypeId == tradeWorkerRecordTypeId && thisContact.AccountId != null){
                
                if(mapAccountContacts.containsKey(thisContact.AccountId)){
                    Map<String,Contact> tempMap = mapAccountContacts.get(thisContact.AccountId);
                    tempMap.put(thisContact.Contact_Type__c,thisContact);
                    mapAccountContacts.put(thisContact.AccountId,tempMap);
                }else{
                    mapAccountContacts.put(thisContact.AccountId, new Map<String,Contact>{thisContact.Contact_Type__c => thisContact});                  
                }                  
            }               
        }//END OF FOR
        
        if(!mapAccountContacts.keySet().isEmpty()){
            for(Account thisAccount : [Select Id, Main_Contact__c, Main_Contact__r.Contact_Type__c, Administration_Contact__c, Administration_Contact__r.Contact_Type__c, H_S_Contact__c, H_S_Contact__r.Contact_Type__c, Quality_Contact__c, Quality_Contact__r.Contact_Type__c from Account where ID IN: mapAccountContacts.keySet()]){
                if((thisAccount.Administration_Contact__c == null || thisAccount.Administration_Contact__r.Contact_Type__c != 'Admin') && mapAccountContacts.get(thisAccount.Id).containskey('Admin')){
                    thisAccount.Administration_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Admin').Id;
                    system.debug('****AdminContactSet');
                }
                
                if((thisAccount.H_S_Contact__c == null || thisAccount.H_S_Contact__r.Contact_Type__c != 'HSE') && mapAccountContacts.get(thisAccount.Id).containskey('HSE')){
                    thisAccount.H_S_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('HSE').Id;
                    system.debug('****HSContactSet');
                }
                
                if((thisAccount.Quality_Contact__c == null || thisAccount.Quality_Contact__r.Contact_Type__c != 'Quality') && mapAccountContacts.get(thisAccount.Id).containskey('Quality')){
                    thisAccount.Quality_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Quality').Id;
                    system.debug('****QualityContactSet');
                }
                
                if(thisAccount.Main_Contact__c == null && mapAccountContacts.get(thisAccount.Id).containskey('Primary')){
                    thisAccount.Main_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Primary').Id;
                    //Set Primary Contact if Admin Contact is not available
                    if(thisAccount.Administration_Contact__c == null){
                        thisAccount.Administration_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Primary').Id;
                        system.debug('****PrimaryAdminContact');
                    }
                    //Set Primary Contact if H&S Contact is not available
                    if(thisAccount.H_S_Contact__c == null){
                        thisAccount.H_S_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Primary').Id;
                        system.debug('****PrimaryH&SContact');
                    }
                    //Set Primary Contact if Quality Contact is not available
                    if(thisAccount.Quality_Contact__c == null){
                        thisAccount.Quality_Contact__c =  mapAccountContacts.get(thisAccount.Id).get('Primary').Id;
                        system.debug('****PrimaryQualityContact');
                    }
                    
                }//END OF PRIMARY IF
                listAccountUpdate.add(thisAccount);   
                system.debug('*******listAccountUpdate'+listAccountUpdate);           
            }//END OF PRIMARY FOR  
            
            //Update Accounts with Contacts Information
            if(!listAccountUpdate.isEmpty()){
                update listAccountUpdate;
                system.debug('*******UpdatedlistAccountUpdate'+listAccountUpdate);           
            }//END OF UPDATE           
            
        }//END OF IF
        }
        catch(Exception e){
       SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','ContactTriggerHelper','setContactsOnTradeAccount');
            result.getReturnValue();
      } 
    }//END OF METHOD
    
    // This method will be called from ContactTriggerHandler and will update the M365 Employee Account Id.  
    public static void setEmployeeAccount(List<Contact> newContactList){
        
        String defaultAccountName = System.label.Default_Account_On_New_Contacts;
        String employeeRecordTypeName = System.label.Contact_Record_Type_Name;
        String companyAPIName = system.label.Account_Name_Field_API_Name;
        Set<String> setOfCompanyNames = new Set<String>();
        Map<String,Id> mapOfAccountNameAndId = new Map<String,Id>();
        List<Contact> filteredContactList = new List<Contact>();
        String employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(employeeRecordTypeName).getRecordTypeId();
        
        try{
        for(Contact contact: newContactList){
            if(contact.RecordTypeId == employeeRecordTypeId && contact.AccountId == null){
                filteredContactList.add(contact);
                if(contact.AD_Company_Name__c != null){
                    setOfCompanyNames.add(contact.AD_Company_Name__c);               
                }
            }
        }
        
        //Update AccountId On Contact  
        if(filteredContactList != null && !filteredContactList.isEmpty()){            
            // Add default Switched On Group account to be assigned on contacts where AD_Company_Name is not found or blank. 
            setOfCompanyNames.add(defaultAccountName);
            //Fetch Account names and respective Ids.
            mapOfAccountNameAndId = UserTriggerHelper.getMapOfAccountNameAndId(setOfCompanyNames); 
            
            // Assign AccountId
            for(Contact contact:filteredContactList){
                Object companyName = contact.get(companyAPIName);           
                if(companyName != null && mapOfAccountNameAndId.containskey((String)companyName)){
                    contact.AccountId = mapOfAccountNameAndId.get((String)companyName);  
                }
                else{
                    contact.AccountId = mapOfAccountNameAndId.get(defaultAccountName);      
                }            
            }
        }
        }
        catch(Exception e){
       SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','ContactTriggerHelper','setEmployeeAccount');
            result.getReturnValue();
      } 
    }//END OF METHOD
}//END OF CLASS