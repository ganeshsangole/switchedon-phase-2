/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "ContactTriggerHandler" and "ContactTriggerHelper" Apex Class
History
08-07-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class ContactTriggerTest {
    @istest
    public static void HandleTriggerLogic_Test(){
        TestDataFactory.createCustomSettingData();
        TestDataFactory.createContactData();
    }  
}