/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: ContentDocumentLinkHandler used to update the file ids on the corresponding 
			 MOJ result record
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class ContentDocumentLinkHandler {
   public static void handleMethod(List<ContentDocumentLink> newList){
       if(Trigger.isAfter){
           if(Trigger.isInsert){
             ContentDocumentLinkHelper.getVersionId(newList);
           }//END OF IS INSERT IF
        }//END OF IS AFTER IF
    }//END OF METHOD
}//END OF CLASS