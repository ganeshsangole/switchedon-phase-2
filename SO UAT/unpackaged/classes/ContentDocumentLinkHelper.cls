/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: ContentDocumentLinkHelper used to update the file ids on the corresponding 
			 MOJ result record
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class ContentDocumentLinkHelper{
   static final String MOJ_RESULT_OBJECT_APINAME = 'MOJ_Result__c'; 

//This method will be used to update the file ids on the corresponding MOJ result record
public static void getVersionId(List<ContentDocumentLink> newList){
    List<ID> setMojIDs = new List<ID>();
    List<MOJ_Result__c> listMOJResultsToUpdate = new List<MOJ_Result__c>();
    Map<ID,ID> mapContentDocumentIDMOJID = new Map<ID,ID>();
    
    try{
    for(ContentDocumentLink thisRec: newList){
        if(String.valueof(thisRec.LinkedEntityId.getsobjecttype()) == MOJ_RESULT_OBJECT_APINAME){
            setMojIDs.add(thisRec.LinkedEntityId);
            mapContentDocumentIDMOJID.put(thisRec.ContentDocumentId, thisRec.LinkedEntityId);            
        }//END OF IF
    }//END OF FOR
    
    System.debug('*****setMojIDs'+setMojIDs);
    System.debug('*****mapContentDocumentIDMOJID'+mapContentDocumentIDMOJID);
    if(!setMojIDs.isEmpty()){
        for(ContentVersion thisContentVersion : [Select Id,ContentDocumentID from ContentVersion where ContentDocumentID IN :mapContentDocumentIDMOJID.keyset()]){
            if(mapContentDocumentIDMOJID.containsKey(thisContentVersion.ContentDocumentID)){
                MOJ_Result__c thisResult = new MOJ_Result__c();
                thisResult.Id = mapContentDocumentIDMOJID.get(thisContentVersion.ContentDocumentID);
                thisResult.File_Id__c = '/sfc/servlet.shepherd/version/download/' + thisContentVersion.Id;
                listMOJResultsToUpdate.add(thisResult);
            }//END OF IF
        }//END OF FOR
        System.debug('*****listMOJResultsToUpdate'+listMOJResultsToUpdate);
        //Update the file ids on the corresponding MOJ result record
        if(!listMOJResultsToUpdate.isEmpty()){
           update listMOJResultsToUpdate;    
        }//END OF IF
    }//END OF IF
    }
     catch(Exception e){
       SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','ContentDocumentLinkHelper','getVersionId');
            result.getReturnValue();
      } 
  }//END OF METHOD
}//END OF CLASS