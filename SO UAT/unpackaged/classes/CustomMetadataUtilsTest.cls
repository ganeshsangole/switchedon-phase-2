/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "CustomMetadataUtils" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class CustomMetadataUtilsTest {
    public static string quoteFieldAPIName=system.label.Quote_Field_API_Name;
    public static string authorFieldAPIName=system.label.Author_Field_API_Name;
    public static string IntranetQuoteOfTheDayAPIName=system.label.Intranet_Quote_Of_The_Day_API_Name;
    public static string quoteLabel=system.label.Quote_Label;
    
    @testSetup
    static void testSetup(){
        Map<String, Object> metadataFieldValueMap = new Map<String, Object>();        
        metadataFieldValueMap.put(quoteFieldAPIName,'Test Quote1');
        metadataFieldValueMap.put(authorFieldAPIName,'Test Author1');
        CustomMetadataUtils.createCustomMetadata(IntranetQuoteOfTheDayAPIName,quoteLabel,metadataFieldValueMap);
    }
   
    
    @isTest
    public static void updateCustomMetadata_Test(){ 
        list<Intranet_Quote_Of_The_Day__mdt> ListquoteOfDayRecords=new list<Intranet_Quote_Of_The_Day__mdt>();
        Map<String, Object> metadataFieldValueMap = new Map<String, Object>();      
        
        ListquoteOfDayRecords=[select id,Label,Quote__c,Author__c from Intranet_Quote_Of_The_Day__mdt where Label=:quoteLabel limit 1];        
        
        metadataFieldValueMap.put(quoteFieldAPIName,'Test Quote');
        metadataFieldValueMap.put(authorFieldAPIName,'Test Author');
        
        if(ListquoteOfDayRecords!=null && !ListquoteOfDayRecords.isempty()){ 
            Test.startTest(); 
            if(ListquoteOfDayRecords[0].Label != null){
             CustomMetadataUtils.updateCustomMetadata(IntranetQuoteOfTheDayAPIName,(ListquoteOfDayRecords[0].Label).replaceAll(' ', '_'), ListquoteOfDayRecords[0].Label,metadataFieldValueMap);    
            }           
            Test.stopTest();       
        }
        system.assert(true);
    }
    
}