/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch CMS Data 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class FetchCMSdata {
    //Wrapper class to store content data 
    public class contentCollectionWrapper {  
        @AuraEnabled
        public map<String,Integer> contentIdsAndViewCount = new map<String,Integer>();
        @AuraEnabled
        public List<String> listOfContentIds = new list<String>(); 
        @AuraEnabled
        public List<ConnectApi.ManagedContentVersion> items = new list<ConnectApi.ManagedContentVersion>();
        @AuraEnabled
        public String nextPageUrl='';
        @AuraEnabled
        public Integer total=0; 
        public contentCollectionWrapper() {
            items = new List<ConnectApi.ManagedContentVersion>();
            nextPageUrl = '';
            total = 0;
        }
    }
    
    //Fetch content data using ConnectApi
    @AuraEnabled(cacheable=true)
    public static contentCollectionWrapper relatedContents(String contentType) {                                      
        List<ConnectApi.ManagedContentVersionCollection> contentCollectionList = new List<ConnectApi.ManagedContentVersionCollection>();        
        Set<String> setOfContentIds = new Set<String>();
        list<Audit_Log__c> auditLogsList = new list<Audit_Log__c>(); 
        contentCollectionWrapper result = new contentCollectionWrapper();
        Integer contentListSize = 250;
        ConnectApi.ManagedContentVersionCollection contentCollection ;
        
        try {
            String communityId= Network.getNetworkId();
            system.debug('communityId@@'+communityId);
            String language = 'en_US';  
            
            if(!Test.isRunningTest()) {
                for(Integer i=0; true; i++) {                    
                    contentCollection = ConnectApi.ManagedContent.getAllManagedContent(communityId, i, contentListSize, language, contentType);
                    contentCollectionList.add(contentCollection);                 
                    
                    if(contentCollection.nextPageUrl == null || contentCollection.nextPageUrl == '' ) {
                        break; 
                    }
                }
            }            
            
            for(ConnectApi.ManagedContentVersionCollection contentCollectionRecord:contentCollectionList){
                result.items.addAll(contentCollectionRecord.items);
                result.total += contentCollectionRecord.total;
                result.nextPageUrl = contentCollectionRecord.nextPageUrl;
            }
            
            id userId = UserInfo.getUserId();
            map<string,integer> mapOfContentIdAndItsViewCount = new map<string,integer>();
            auditLogsList = [select id,User__c,CMS_Content_Id__c,Content_Type__c from Audit_Log__c where User__c=:userId and Content_Type__c =:contentType]; 
            
            /*Create map Of Content Id and its view count also create set of content Ids viewed by user */            
            if(auditLogsList !=null && !auditLogsList.isempty()){
                for(Audit_Log__c auditLog:auditLogsList){
                    if(auditLog.CMS_Content_Id__c != null){                   
                        setOfContentIds.add(auditLog.CMS_Content_Id__c);   
                        
                        if(mapOfContentIdAndItsViewCount.keyset().contains(auditLog.CMS_Content_Id__c)){                   
                            mapOfContentIdAndItsViewCount.put(auditLog.CMS_Content_Id__c,mapOfContentIdAndItsViewCount.get(auditLog.CMS_Content_Id__c)+1);  
                        }
                        else{
                            mapOfContentIdAndItsViewCount.put(auditLog.CMS_Content_Id__c,1);   
                        }
                    }
                } 
                result.contentIdsAndViewCount = mapOfContentIdAndItsViewCount;     
                result.listOfContentIds.addAll(setOfContentIds);
            }            
            return result;            
        }
        catch(Exception error){
            system.debug('exception'+error.getMessage());
            SaveResult saveresult = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','FetchCMSdata','relatedContents');
            saveresult.getReturnValue(); // Log Exception
        }
        return null;        
    }
    
    //Create entry in Audit Log
    @AuraEnabled
    public static void addEntryToAuditLog(string CMSContentId,string ContentName,string ContentType ){
        id userId = UserInfo.getUserId();
        list<Audit_Log__c> auditLogsList = new list<Audit_Log__c>();
        auditLogsList.add(new Audit_Log__c(User__c = userId,CMS_Content_Id__c = CMSContentId,Content_Name__c = ContentName,Content_Type__c = ContentType));
        
        if(auditLogsList != null && !auditLogsList.isempty()){
            insert auditLogsList;
        }     
    }
}