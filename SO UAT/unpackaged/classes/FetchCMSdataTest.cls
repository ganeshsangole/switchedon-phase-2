/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "FetchCMSdata" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class FetchCMSdataTest {    
     @isTest
    public static void addEntryToAuditLog_Test(){          
        Test.startTest();   
        FetchCMSdata.addEntryToAuditLog('12345','Finance Content','News');
        Test.stopTest(); 
        system.assert(true);
    }
    
    @isTest
    public static void relatedContents_Test(){  
        TestDataFactory.createAuditLogData();
        Test.startTest();   
        FetchCMSdata.relatedContents('News');
        Test.stopTest(); 
        system.assert(true);
    }
    
    
}