/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch Contact Image From Microsoft 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class FetchOutlookContact {
    /*Get Contact Image*/
    @AuraEnabled
    public static String getImageBlocks(String userId){
        try {
            String accessToken = ApexUtilityClass.requestAccessToken();
            String endPoint = 'callout:Graph_Explorer/'+userId+'/photo/$value';  
            map<string,string> MapOfHeaderDetails=new map<string,string>();
            
            MapOfHeaderDetails.put('endPoint',endPoint);
            MapOfHeaderDetails.put('methodType','GET');        
            MapOfHeaderDetails.put('Authorization', 'Bearer ' + accessToken);
            MapOfHeaderDetails.put('applicationCd','SALESFORCE');
            MapOfHeaderDetails.put('Content-Type','application/json');
            MapOfHeaderDetails.put('accept','application/json');
            
            HttpResponse response = ApexUtilityClass.getAPIResponse(MapOfHeaderDetails);                    
            
            if(response !=null){
                if(response.getStatusCode() == 200) {
                    // System.debug('response.getBody()****** : '+response.getBody());
                    string s = EncodingUtil.base64Encode(response.getBodyAsBlob());
                    
                    System.debug('String representation *** '+s);
                    return s;
                }else {
                    System.debug('Exeption Occured while fetching photo ****** '+response.getBody());
                }
            }
        } 
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','FetchOutlookContact','getImageBlocks');
            result.getReturnValue(); // Log Exception
        }        
        return null;
    }
    
}