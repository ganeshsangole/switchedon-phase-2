/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Send Mock Response
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@istest
global class FetchOutlookContactCalloutMock implements HttpCalloutMock{
        // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"success":{"total":1}}');
        response.setStatusCode(200);
        return response; 
    }

}