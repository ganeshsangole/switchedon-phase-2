/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "FetchOutlookContact" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class FetchOutlookContactTest {
    @isTest
    public static void getImageBlocks_Test(){       
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new FetchOutlookContactCalloutMock()); 
        String result= FetchOutlookContact.getImageBlocks('12345');
        Test.stopTest();  
        system.assert(true);
    }
}