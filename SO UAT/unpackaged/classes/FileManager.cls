public without sharing class FileManager {
        
  @AuraEnabled(cacheable=true)
  public static string userId(){
      return UserInfo.getUserId();
  }

        //fetch picklist values from custom object in lwc
        @AuraEnabled(cacheable=true)
        public static List < customValueWrapper > pickListValueDynamically(sObject customObjInfo, string selectPicklistApi) {
           Schema.DescribeSObjectResult objDescribe = customObjInfo.getSObjectType().getDescribe();     
           map < String, Schema.SObjectField > customFieldMap = objDescribe.fields.getMap();      
           list < Schema.PicklistEntry > custPickValues = customFieldMap.get(selectPicklistApi).getDescribe().getPickListValues();
           list < customValueWrapper > customObjWrapper = new list < customValueWrapper > ();
           for (Schema.PicklistEntry myCustPick: custPickValues) {
             customValueWrapper selectOptionValueWrapper = new customValueWrapper();
                selectOptionValueWrapper.custFldlabel = myCustPick.getLabel();
                selectOptionValueWrapper.custFldvalue = myCustPick.getValue();
               customObjWrapper.add(selectOptionValueWrapper);
           }
           
           return customObjWrapper;
     
         }
         

    
    // end of logic multiselect picklist
         // wrapper class 
           public with sharing class customValueWrapper {
             @auraEnabled public string custFldlabel {get;set;}
             @auraEnabled public string custFldvalue {get;set;}
           }
   
    @AuraEnabled
    public static Map<String,String> getLeadDetails(String leadId){
        String leadIdValue=leadId;
        List<String> leadDetails= new List<String>();
        Map<String,String> stringOFMap=new Map<String,String>(); 
      try{
         
          system.debug('Inside get details funciton LeadId'+leadId);
          for(Lead ld: [Select Id,Company,Street,City,State,PostalCode,Country,Salutation,FirstName,LastName,Phone,Email from Lead where Id =: leadIdValue]){
            
                  if(ld.Salutation != null){
                  String salutation= ld.Salutation.remove('.');
                  stringOFMap.put('Salutation',salutation);
                  }
                stringOFMap.put('Company',ld.Company);
                stringOFMap.put('FirstName',ld.FirstName);
                stringOFMap.put('LastName',ld.LastName);
                stringOFMap.put('Phone',ld.Phone);
                stringOFMap.put('Email',ld.Email);
                stringOFMap.put('PostalAddress',ld.Street);
                stringOFMap.put('City',ld.City);
                stringOFMap.put('PostalCode',ld.PostalCode);        
        }
      }
      catch(Exception e){
        throw e;
      }
      return stringOFMap;
    }
    
    @AuraEnabled
    public static void createRecord(String leadId, Case caseContent,String caseContactData,String fileDetails){
      try{
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Trade Company Onboarding').getRecordTypeId();
          String leadIdValue= leadId;
          List<Object> fieldList = (List<Object>)JSON.deserializeUntyped(fileDetails);
       
          system.debug('LeadId'+ leadId);
         system.debug('Record Type Id'+ recordTypeId);
          system.debug('leadIdValue'+ leadIdValue);
        
         caseContent.Subject = 'Trade Company Onboarding' + '-' + caseContent.SuppliedCompany;
         caseContent.RecordTypeId=recordTypeId; 
    
         
         for(Lead ld: [Select Id,Email,FirstName,LastName,Name,Phone from Lead where Id =: leadIdValue]){
          caseContent.SuppliedEmail = ld.Email;
          system.debug('SuppliedEmail'+ caseContent.SuppliedEmail);
          caseContent.SuppliedName = ld.Name;
          caseContent.First_Name__c = ld.FirstName;
          caseContent.Last_Name__c = ld.LastName;
          system.debug('First name of lead'+ caseContent.First_Name__c);
          system.debug('last name of lead'+ caseContent.Last_Name__c);
          caseContent.Lead__c= ld.Id;
          caseContent.SuppliedPhone= ld.Phone;
          system.debug('SuppliedName'+ caseContent.SuppliedName);
          }
          Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true; 
            dmo.EmailHeader.triggerUserEmail = true;
            caseContent.setOptions(dmo);
            insert caseContent;
   
    
        for(Object fld : fieldList){    
            Map<String,Object> data = (Map<String,Object>)fld;
         
            system.debug('8735673248jhbdcjeg'+data.values());
            linkFile(caseContent.Id, data.values());

        }
       
        List<Case_Contact__c> caseContactRecordList=new List<Case_Contact__c>();
        List<Case_Contact__c> caseContactDetails= (List<Case_Contact__c>) JSON.deserialize(caseContactData,List<Case_Contact__c>.class);
        for(Case_Contact__c caseConObj:caseContactDetails)
        {
            caseConObj.Case__c= caseContent.Id;
            caseConObj.Name = caseConObj.First_Name__c + ' ' + caseConObj.Last_Name__c;
            caseContactRecordList.add(caseConObj);
        }
        insert caseContactRecordList;
    }
      catch(Exception e){
       throw e;
      }

    }      

      private static void linkFile(String recordId, List<Object> fileDetails){
        try {

            system.debug('Inside linkFile'+fileDetails);
            List<string> lsstr= new List<string> ();

            for(Object a: fileDetails){
                lsstr.add(String.valueOf(a));
            }
            system.debug('List of string'+lsstr);
            system.debug('List oth value'+lsstr.get(0));
            system.debug('List of 1st value'+lsstr.get(1));
           
             if(String.isBlank(lsstr.get(1)) || String.isBlank(recordId)) {
                return;
            }
            system.debug('documentID_____'+lsstr.get(1));
            system.debug('recordId'+recordId);
            List<ContentDocument> cdList= new List<ContentDocument>();
            cdList= [Select Id,Title from ContentDocument where Id=: lsstr.get(1)];
            system.debug('cdList++++++++ before update'+cdList);
           cdList[0].Title = lsstr.get(0);           
            update cdList;
            system.debug('cdList++++++++'+cdList);
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.contentDocumentId = lsstr.get(1);
            cdl.linkedEntityId = recordId;
            cdl.shareType = 'V';
          
            insert cdl; 
        }catch(Exception e) {
            
            throw e;
        }
    }

}