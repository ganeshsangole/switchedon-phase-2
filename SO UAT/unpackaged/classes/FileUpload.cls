/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Update ContentVersion Id On Knowledge
History
01-06-2021 Tranzevo Initial Release 
----------------------------------------------------------------------------------------------*/
public class FileUpload {
    @AuraEnabled
    public static void updateContentVersionIdOnKnowledge(List<Id> listContentDocumentIds,Id recordId){
        
        if(listContentDocumentIds !=null && !listContentDocumentIds.isEmpty()){
            list<ContentVersion>listContentVersions = new list<ContentVersion>();
            Id knowledgeRecordId = recordId;
            List<Knowledge__kav> knowledgeList = new List<Knowledge__kav>();
            
            listContentVersions = [select Id,IsLatest,ContentDocumentId  from ContentVersion where IsLatest = true and ContentDocumentId in:listContentDocumentIds limit 1]; 
            
            knowledgeList = [select Id,Banner_Image_Id__c from Knowledge__kav where Id =:knowledgeRecordId]; 
            
            if(knowledgeList != null && !knowledgeList.isEmpty()){
                if(listContentVersions != null && !listContentVersions.isEmpty()){
                    knowledgeList[0].Banner_Image_Id__c = listContentVersions[0].Id;                                      
                }
                update knowledgeList;                  
            }            
        }
    }
    
}