/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "FileUpload" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class FileUploadTest {    
    @isTest
    public static void updateContentVersionIdOnKnowledge_Test(){ 
        List<ContentVersion> contentVersionsList = new List<ContentVersion>();
        List<Id> listContentDocumentIds= new List<Id>();
        list<Knowledge__kav> knowledgeList = new list<Knowledge__kav >();
        
        contentVersionsList = TestDataFactory.createContentVersionData();
        
        knowledgeList = TestDataFactory.createKnowledgeArticle();
        
        listContentDocumentIds.add(contentVersionsList[0].ContentDocumentId);        
        
        Test.startTest();   
        FileUpload.updateContentVersionIdOnKnowledge(listContentDocumentIds,knowledgeList[0].Id);
        Test.stopTest(); 
        system.assert(true);
    }
}