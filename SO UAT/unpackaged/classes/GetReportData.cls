/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class For adding Campaign Members from Add Members quick action on campaign 
History
29-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public with sharing class GetReportData {
    public static Integer nameIndex = 0;
    private static Set<String> supportedFields = new Set<String>{'FIRST_NAME', 'LAST_NAME', 'CONTACT_ID'}; 
    public class CustomException extends Exception{}

    
    private static List<List<fieldData>> getReportData(Reports.ReportResults results, List<fieldDef> reportFields) {
        List<List<fieldData>> reportData  = new List<List<fieldData>>();
        Reports.ReportFactWithDetails factDetails = (Reports.ReportFactWithDetails)results.getFactMap().get('T!T');
        List<Reports.ReportDetailRow> reportDetailRowList = factDetails.getRows();

        for(Reports.ReportDetailRow reportDetailRow: reportDetailRowList) {
            Integer cellIndex = 0;
            List<fieldData> fieldDataRow = new List<fieldData>();
            for(Reports.ReportDataCell reportDataCell: reportDetailRow.getDataCells()) {
                System.debug('reportFields[cellIndex].fieldName**** : '+reportFields[cellIndex].fieldName);
                System.debug('supportedFields**** : '+supportedFields);
                if(supportedFields.contains(reportFields[cellIndex].fieldName)) {
                    fieldData fd = new fieldData();
                    System.debug('report data cell**** : '+JSON.serialize(reportDataCell));
                    fd.fieldValue = (String)reportDataCell.getValue();
                    fd.fieldLabel = (String)reportDataCell.getLabel();
                    fd.columnName = reportFields[cellIndex].fieldName;
                    fieldDataRow.add(fd);
                }

                cellIndex++;
            }

            reportData.add(fieldDataRow);
        }
        
        return reportData;
    }

    private static List<fieldDef> getColumnNames(Reports.ReportMetadata mdata, Reports.ReportResults results) {
        List<fieldDef> columnNames = new List<fieldDef>();
        List<String> columns = mdata.getDetailColumns();
        Reports.ReportExtendedMetadata reportExtendedMetadata = results.getReportExtendedMetadata();
        Map<String, Reports.DetailColumn> detailColumnMap = reportExtendedMetadata.getDetailColumnInfo();

        for(String columnName : columns) {
            Reports.DetailColumn detailColumn = detailColumnMap.get(columnName);
            fieldDef fd = new fieldDef();
            fd.fieldName = detailColumn.getName(); 
            fd.fieldLabel = detailColumn.getLabel();
            fd.dataType = detailColumn.getDataType().name();
            columnNames.add(fd);
        }

        return columnNames;
    }

    private static tabularReportResponse getReportResponse(String reportId) {
        tabularReportResponse trr = new tabularReportResponse();
        Reports.ReportResults results = Reports.ReportManager.runReport(reportId, true);
        Reports.ReportMetadata reportMetadata = results.getReportMetadata();
       
        List<fieldDef> reportFields = getColumnNames(reportMetadata, results);
System.debug('Report results**** : '+results);
        System.debug('Report Fields**** : '+JSON.serialize(reportFields));


        List<List<fieldData>> fieldDataList = getReportData(results, reportFields);


        System.debug('Report Data**** : '+JSON.serialize(fieldDataList));

        trr.reportFields = reportFields;
        trr.fieldDataList = fieldDataList;
        return trr;
    }

    public static tabularReportResponse getResults(String reportId) {
        tabularReportResponse reportResponse = getReportResponse(reportId);
        return reportResponse;
    }

    public static List<String> getSobjectIds(String reportId) {
        List<String> sobjectIds = new List<String>();
        try {
            tabularReportResponse reportResponse = getReportResponse(reportId);
            Boolean contactInfoFound = false;
            for(List<fieldData> rows : reportResponse.fieldDataList) {
                for(fieldData row : rows) {
                    if(supportedFields.contains(row.columnName) && 
                    String.isNotBlank(row.fieldValue) && 
                    row.fieldValue instanceOf Id &&
                    String.valueOf(Id.valueOf(row.fieldValue).getsobjecttype()).equalsIgnoreCase('Contact')) {
                        sobjectIds.add(row.fieldValue);
                        contactInfoFound = true;
                        break;
                    }
                }
            }

            if(!contactInfoFound) {
                throw new CustomException(System.Label.Add_Campaign_members_missing_contact_id);
            }

            return sobjectIds;
        }catch(Exception exp) {
            throw exp;
        }
    }

    public class tabularReportResponse {
        public List<fieldDef> reportFields {get; set;}
        public List<List<fieldData>> fieldDataList {get; set;}
    }

    public class fieldDef {
        public String fieldName {get; set;}
        public String fieldLabel {get; set;}
        public String datatype {get; set;}
    }

    public class fieldData {
        public String fieldValue {get; set;}
        public String fieldLabel {get; set;}
        public String columnName {get; set;}
    }
}