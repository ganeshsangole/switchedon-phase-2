/***********************************************************************************************************************************************************
*History: 
*-Version         Developer Name     Date       Detail Features
* 1.0             Tranzevo         14-7-2021    Initial Development
**********************************************************************************************************************************************************/
@isTest
public class InductionFormsTriggerBatchTest {
    @testSetup static void setupTestData(){
        
		List<Account> accLst = new List<Account>();
        TestDataFactory.AccountParams aParms = new TestDataFactory.AccountParams();
        aParms.Name = 'Test Name';
        
        List<Account> acclist1 = TestDataFactory.createTestAccounts(1,aParms);
        
        accLst.addAll(acclist1);     
        insert accLst;
        
        List<Lead> leadLst = new List<Lead>();
        TestDataFactory.LeadParams lParms = new TestDataFactory.LeadParams();
        lParms.FirstName='Test';
        lParms.LastName='Last';
        lParms.Company='Test company';
        lParms.Status='New';
        
        List<Lead> ledlist1 = TestDataFactory.createTestLeads(1,lParms);
        ledlist1[0].Email='test@example.com';
        ledlist1[0].Company = 'TestCompany';
        ledlist1[0].Salutation='Mr.';
        ledlist1[0].Converted_Lead__c=false;
        
        leadLst.addAll(ledlist1);
        
        insert leadLst;
        
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        cslist1[0].Lead__c= leadLst[0].Id;
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        
        csLst.addAll(cslist1);
        insert csLst;
        
        ContentVersion conver=new ContentVersion();
        conver.Title='test';
        conver.VersionData=Blob.valueOf('Test Image');
        conver.PathOnClient='test.jgp';
        insert conver;
        
         List<Contact> conLst = new List<Contact>();
        TestDataFactory.ContactParams conParms = new TestDataFactory.ContactParams();
        conParms.firstName = 'Test Trade Worker';
        conParms.email ='test@example.com.invalid';
        conParms.lastName='Last Name';
        conParms.recordType='Trade';
        
        List<Contact> conlist1 = TestDataFactory.createTestContacts(1,conParms);
        conlist1[0].AccountId=accLst[0].Id;
        conlist1[0].Contact_Type__c='Worker';
        conLst.addAll(conlist1);
        
        insert conLst;
        
        List<Case_Contact__c> keycontactRecords = new List<Case_Contact__c>();
        
        Case_Contact__c keyconObj = new Case_Contact__c();
        keyconObj.Case__c=csLst[0].Id;
        keyconObj.Contact_Type__c = 'Primary';
        keyconObj.First_Name__c = 'Test First Name'; 
        keyconObj.Last_Name__c = 'Test Last Name'; 
        keyconObj.Phone__c = '21342134'; 
        keyconObj.Salutation__c = 'Mr'; 
        String orgId = UserInfo.getOrganizationId();
        String dateString = 
        String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        keyconObj.Email__c = uniqueName + '@test' + orgId + '.org';
        keycontactRecords.add(keyconObj);
        
        insert keycontactRecords;
    }
    
    //Method to test signature component
    @isTest static void test_SignatureComponent(){
        List<Lead> ldList=[Select Id from Lead];
        List<Case> caseList=[Select Id from Case];
        
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        cslist1[0].Lead__c= ldList[0].Id;
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        
        csLst.addAll(cslist1);
        
       String fileDateBase64 = EncodingUtil.base64Encode(Blob.valueOf('Test Image'));
 
        Test.startTest();
         PageReference pageRef = Page.AttachPDF;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(caseList[0]);
        ApexPages.currentPage().getParameters().put('Id',caseList[0].id);
        
        SignatureController.saveSignature(fileDateBase64,caseList[0].Id);
        SignatureController.savePDFCase(caseList[0].Id);
        TextVFPDFController pdfcontro= new TextVFPDFController();
        Test.stopTest();
    } 
    
    //Method to test contact trigger SetTradeWorkerId method
    @isTest static void test_contactTriggerSetTradeWorkerId(){
        String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
        List<Account> accList=[Select Id,Administration_Contact__c,H_S_Contact__c,Quality_Contact__c,Main_Contact__c from Account];
        
        List<Contact> conLst = new List<Contact>();
        TestDataFactory.ContactParams cParms = new TestDataFactory.ContactParams();
        cParms.firstName = 'First Name';
        cParms.email ='test@example.com.invalid';
        cParms.lastName='Last Name';
        cParms.recordType='Trade';
        
        List<Contact> conlist1 = TestDataFactory.createTestContacts(3,cParms);
        conlist1[0].AccountId=accList[0].Id;
        conlist1[0].RecordTypeId=tradeWorkerRecordTypeId;
        conlist1[0].Contact_Type__c='Worker';
        conlist1[1].AccountId=accList[0].Id;
        conlist1[1].RecordTypeId=tradeWorkerRecordTypeId;
        conlist1[1].Contact_Type__c='Worker';
        conlist1[2].AccountId=accList[0].Id;
        conlist1[2].RecordTypeId=tradeWorkerRecordTypeId;
        conlist1[2].Contact_Type__c='Worker';
        conLst.addAll(conlist1);
        
        Test.startTest();
        ContactTriggerHelper.setTradeWorkerId(conLst);
        Test.stopTest();
        
    }
  
    //Method to test contact trigger setContactsOnTradeAccount method
    @isTest static void test_contactTriggerSetContactsOnTradeAccount(){
        String tradeWorkerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Trade').getRecordTypeId();
        List<Account> accList=[Select Id,Administration_Contact__c,H_S_Contact__c,Quality_Contact__c,Main_Contact__c from Account];
        
        List<Contact> conLst = new List<Contact>();
        TestDataFactory.ContactParams cParms = new TestDataFactory.ContactParams();
        cParms.firstName = 'First Name';
        cParms.email ='test@example.com.invalid';
        cParms.lastName='Last Name';
        cParms.recordType='Trade';
        
        List<Contact> conlist1 = TestDataFactory.createTestContacts(2,cParms);
        conlist1[0].AccountId=accList[0].Id;
        conlist1[0].RecordTypeId=tradeWorkerRecordTypeId;
        conlist1[0].Contact_Type__c='Admin';
        conlist1[1].AccountId=accList[0].Id;
        conlist1[1].RecordTypeId=tradeWorkerRecordTypeId;
        conlist1[1].Contact_Type__c='Primary';
        conLst.addAll(conlist1);
        
        
        Test.startTest();
		insert conLst;
        Test.stopTest();
        
    }
    
     //Method to test contact trigger SetEmployeeAccount method
    @isTest static void test_contactTriggerSetEmployeeAccount(){
        String EmployeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        List<Account> accList=[Select Id,Administration_Contact__c,H_S_Contact__c,Quality_Contact__c,Main_Contact__c from Account];
        
        List<Contact> conLst = new List<Contact>();
        TestDataFactory.ContactParams cParms = new TestDataFactory.ContactParams();
        cParms.firstName = 'First Name';
        cParms.email ='test@example.com.invalid';
        cParms.lastName='Last Name';
        cParms.recordType='Trade';
        
        List<Contact> conlist1 = TestDataFactory.createTestContacts(2,cParms);
        conlist1[0].RecordTypeId=EmployeeRecordTypeId;
        conlist1[0].Employee_Team__c='Contractor';
        conlist1[0].Role__c='Accountant';
        conlist1[0].Contact_Type__c='Admin';
        conlist1[1].RecordTypeId=EmployeeRecordTypeId;
        conlist1[1].Contact_Type__c='Primary';
        conlist1[1].Employee_Team__c='Contractor';
        conlist1[1].Role__c='Accountant';
        conLst.addAll(conlist1);
        
        
        Test.startTest();
		insert conLst;
        Test.stopTest();
        
    }
    
    //Method to test case trigger validateOnboardingTaskCompletion method
    @isTest static void test_caseTriggerValidateOnboardingTaskCompletion(){
        String tradeCompanyOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Company_Onboarding').getRecordTypeId();
        try{
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(2,cParms);
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].RecordTypeId=tradeCompanyOnboardingRecordType;
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        
        
        csLst.addAll(cslist1);
        insert csLst;
        
        Task t=new Task();
        t.Status='Open';
        t.WhatId=csLst[0].Id;
        insert t;
        
        Map<ID,Case> caseMap= new Map<Id,Case>();
        caseMap.put(csLst[0].Id,csLst[0]);
        
        csLst[0].Status='Closed-Approved';
        update csLst;
    	}
         catch(Exception e) {
            System.assert(e.getMessage().contains('Please complete the pending tasks before closing the case!'));
        }
        
    }
     //Method to test case trigger validateKOApprovalForm method
    @isTest static void test_caseTriggerValidateKOApprovalForm(){
        String tradeCompanyOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Company_Onboarding').getRecordTypeId();
        try{
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(2,cParms);
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].RecordTypeId=tradeCompanyOnboardingRecordType;
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        
        
        csLst.addAll(cslist1);
        insert csLst;
        
        Task t=new Task();
        t.Status='Open';
        t.WhatId=csLst[0].Id;
        insert t;
        
        Map<ID,Case> caseMap= new Map<Id,Case>();
        caseMap.put(csLst[0].Id,csLst[0]);
        
        
        csLst[0].Status='Pending KO Approval';
        update csLst;
        }
        catch(Exception e) {
            System.assert(e.getMessage().contains('Please upload Kāinga Ora Approval Document in the Files before updating status to Pending Kāinga Ora Approval!'));
        }
        
    }
    
       //Method to test case trigger CreateTradeContacts method
    @isTest static void test_caseTriggerCreateTradeContacts(){
        List<Account> accList=[Select Id,Administration_Contact__c,H_S_Contact__c,Quality_Contact__c,Main_Contact__c from Account];

        String tradeWorkerOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Worker_Onboarding').getRecordTypeId();
       List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        cslist1[0].AccountId=accList[0].Id;
        cslist1[0].Gender__c='Male';
        cslist1[0].Work_Immigration_Status__c='Work Visa';
        cslist1[0].Ethnicity__c='American';
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].First_Name__c='Test first name';
        cslist1[0].Last_Name__c='Test last name';
        cslist1[0].SuppliedEmail='test@example.com.invalid';
        cslist1[0].SuppliedPhone='12341344';
        cslist1[0].RecordTypeId=tradeWorkerOnboardingRecordType;
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].MOJ_Approved__c=false;
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        
        
        csLst.addAll(cslist1);
        insert csLst;
        
        
        Map<ID,Case> caseMap= new Map<Id,Case>();
        caseMap.put(csLst[0].Id,csLst[0]);
        
        
        csLst[0].MOJ_Approved__c=true;
        update csLst;
   
    }
    
         //Method to test case trigger CreateContentDocumentLink method
    @isTest static void test_caseTriggerCreateContentDocumentLink(){
            List<Account> accList=[Select Id,Administration_Contact__c,H_S_Contact__c,Quality_Contact__c,Main_Contact__c from Account];

       List<Contact> conList=[Select Id from Contact];
        
        Id conId = [Select Id from ContentVersion].Id;
        
        String photourl=System.Label.VersionIdURL;
        
        String tradeWorkerOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Worker_Onboarding').getRecordTypeId();
       List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        cslist1[0].AccountId=accList[0].Id;
        cslist1[0].Gender__c='Male';
        cslist1[0].Work_Immigration_Status__c='Work Visa';
        cslist1[0].Ethnicity__c='American';
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].First_Name__c='Test first name';
        cslist1[0].Last_Name__c='Test last name';
        cslist1[0].SuppliedEmail='test@example.com.invalid';
        cslist1[0].SuppliedPhone='12341344';
        cslist1[0].RecordTypeId=tradeWorkerOnboardingRecordType;
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].MOJ_Approved__c=false;
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        cslist1[0].Photo_Version_Id__c = photourl + conId;

        
        
        csLst.addAll(cslist1);
        insert csLst;
        
        
        Map<ID,Case> caseMap= new Map<Id,Case>();
        caseMap.put(csLst[0].Id,csLst[0]);
        
        
        csLst[0].Trade_Worker__c=conList[0].Id;
        update csLst;
      
    }
    
    //Method to test lead Trigger
    @isTest static void test_handleLeadConversionMethod(){
        String tradeWorkerOnboardingRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Trade_Worker_Onboarding').getRecordTypeId();

		List<Lead> leadLst = new List<Lead>();
        TestDataFactory.LeadParams lParms = new TestDataFactory.LeadParams();
        lParms.FirstName='Test';
        lParms.LastName='Last';
        lParms.Company='Test company';
        lParms.Status='New';
        
        List<Lead> ledlist1 = TestDataFactory.createTestLeads(1,lParms);
        ledlist1[0].Email='test1234@example.com';
        ledlist1[0].Company = 'TestCompany';
        ledlist1[0].Salutation='Mr.';
        ledlist1[0].Converted_Lead__c=false;
        
        leadLst.addAll(ledlist1);
        
        insert leadLst;
        
        system.debug('leadList line 384'+leadLst);
        
        Map<ID,Lead> leadMap= new Map<Id,Lead>();
        leadMap.put(leadLst[0].Id,leadLst[0]);
        
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(1,cParms);
        cslist1[0].Lead__c=leadLst[0].Id;
        cslist1[0].Gender__c='Male';
        cslist1[0].Work_Immigration_Status__c='Work Visa';
        cslist1[0].Ethnicity__c='American';
        cslist1[0].SuppliedEmail='test@example.com';
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].First_Name__c='Test first name';
        cslist1[0].Last_Name__c='Test last name';
        cslist1[0].SuppliedEmail='test@example.com.invalid';
        cslist1[0].SuppliedPhone='12341344';
        cslist1[0].RecordTypeId=tradeWorkerOnboardingRecordType;
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].MOJ_Approved__c=false;
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';

        csLst.addAll(cslist1);
        insert csLst;
        
        List<Case_Contact__c> keycontactRecords = new List<Case_Contact__c>();
        
        Case_Contact__c keyconObj = new Case_Contact__c();
        keyconObj.Case__c=csLst[0].Id;
        keyconObj.Contact_Type__c = 'Primary';
        keyconObj.First_Name__c = 'Test First Name'; 
        keyconObj.Last_Name__c = 'Test Last Name'; 
        keyconObj.Phone__c = '21342134'; 
        keyconObj.Salutation__c = 'Mr'; 
        String orgId = UserInfo.getOrganizationId();
        String dateString = 
        String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        keyconObj.Email__c = uniqueName + '@test' + orgId + '.org';
        keycontactRecords.add(keyconObj);
        
        insert keycontactRecords;
     
        
        Test.startTest();
        leadLst[0].Converted_Lead__c=true;
        update leadLst;
        LeadTriggerHelper.handleLeadConversion(null,null);
        Test.stopTest();
       
    }
    
     @isTest
    private static void test_userId() {
        String uId = CompanyOnboardingRecordCreation.userId();
        String uId1 = TradeWorkerRecordCreation.userId();
        System.assert(uId.length() == 18);
        System.assert(uId1.length() == 18);
    }
    
    @isTest
    private static void test_companypickListValueDynamically() {
        Case_Contact__c casecon=new Case_Contact__c();
        List<CompanyOnboardingRecordCreation.customValueWrapper> pickListEntries = CompanyOnboardingRecordCreation.pickListValueDynamically(casecon,'Contact_Type__c');
        System.assert(pickListEntries.size() > 0);
    }
    
    @isTest
    private static void test_tradeworkerpickListValueDynamically() {
        Case caseObj=new Case();
        List<TradeWorkerRecordCreation.customValueWrapper> pickListEntries = TradeWorkerRecordCreation.pickListValueDynamically(caseObj,'Ethnicity__c');
        System.assert(pickListEntries.size() > 0);
    }
    
    @isTest
    private static void test_getPickListEntries() {
        List<TradeWorkerRecordCreation.PickListEntrty> pickListEntries = TradeWorkerRecordCreation.getPickListEntries('Base_Locations__c', 'Case');
        System.assert(pickListEntries.size() > 0);
    }
    
    //Method to test contact clone controller apex classes
    @isTest static void test_contactCloneControllerMethod(){
    
        List<Account> accountDetails=[Select Id from Account];
        
        List<Account> accLst = new List<Account>();
        TestDataFactory.AccountParams aParms = new TestDataFactory.AccountParams();
        aParms.Name = 'Test Apple Company';
        
        List<Account> acclist1 = TestDataFactory.createTestAccounts(1,aParms);
        
        accLst.addAll(acclist1);     
        insert accLst;
        
        system.debug('Line 505 accLst'+accLst);
        
        List<Contact> conLst = new List<Contact>();
        TestDataFactory.ContactParams conParms = new TestDataFactory.ContactParams();
        conParms.firstName = 'Test Trade Worker';
        conParms.email ='testdata45678@example.com.invalid';
        conParms.lastName='Last Name';
        conParms.recordType='Trade';
        
        List<Contact> conlist1 = TestDataFactory.createTestContacts(2,conParms);
        conlist1[0].AccountId=accountDetails[0].Id;
        conLst.addAll(conlist1);
        
        insert conLst;
        
        List<Contact> contactLst = new List<Contact>();
        TestDataFactory.ContactParams conParms1 = new TestDataFactory.ContactParams();
        conParms1.firstName = 'Test Trade Worker';
        conParms1.email ='test456356@example.com.invalid';
        conParms1.lastName='Last Name';
        conParms1.recordType='Trade';
        
        List<Contact> contactLst1 = TestDataFactory.createTestContacts(1,conParms1);
        contactLst1[0].AccountId=accLst[0].Id;
        contactLst.addAll(contactLst1);
        
        
        String ContactId=conLst[0].Id;
        
        Test.startTest();
        ContactCloneController.fecthExistingValues(conLst[0].Id);
        ContactCloneController.cloneContact(ContactId,contactLst[0].FirstName,contactLst[0].LastName,contactLst[0].Email,contactLst[0].AccountId);
        Test.stopTest();
        
    }
    //Method to test company onboarding and trade worker onboarding apex classes
    @isTest static void test_createRecordMethod(){
        List<Lead> ldList=[Select Id,Salutation from Lead];
		List<Account> accList=[Select Id from Account];
        
        List<Case> csLst = new List<Case>();
        TestDataFactory.CaseParams cParms = new TestDataFactory.CaseParams();
        cParms.Origin = 'Web';
        cParms.Status = 'New';
        
        List<Case> cslist1 = TestDataFactory.createTestCases(2,cParms);
        cslist1[0].Lead__c= ldList[0].Id;
        cslist1[0].SuppliedCompany='Test Company name';
        cslist1[0].Bank_Account_Name__c='New Zealand Bank';
        cslist1[0].Bank_Account_Number__c='124234';
        cslist1[0].Base_Locations__c='Christchurch';
        cslist1[0].Public_Liability_Insurer__c='Health Company';
        cslist1[0].Public_Liability_Policy_No__c='1234123';
        cslist1[0].Public_Liability_Expiry__c=System.Today();
        cslist1[0].Public_Liability_Value_Insured__c='1234';
        cslist1[0].Vehicle_Insurance_Insurer__c='Insurance';
        cslist1[1].Lead__c= ldList[0].Id;
        cslist1[1].SuppliedCompany='Test Company name';
        cslist1[1].Bank_Account_Name__c='New Zealand Bank';
        cslist1[1].Bank_Account_Number__c='124234';
        cslist1[1].Base_Locations__c='Christchurch';
        cslist1[1].Public_Liability_Insurer__c='Health Company';
        cslist1[1].Public_Liability_Policy_No__c='1234123';
        cslist1[1].Public_Liability_Expiry__c=System.Today();
        cslist1[1].Public_Liability_Value_Insured__c='1234';
        cslist1[1].Vehicle_Insurance_Insurer__c='Insurance';
        cslist1[1].AccountId=accList[0].Id;
        
        csLst.addAll(cslist1);
        
        
        List<Case_Contact__c> keycontactRecords = new List<Case_Contact__c>();
        
        Case_Contact__c keyconObj = new Case_Contact__c();
        keyconObj.Contact_Type__c = 'Primary';
        keyconObj.First_Name__c = 'Test First Name'; 
        keyconObj.Last_Name__c = 'Test Last Name'; 
        keyconObj.Phone__c = '21342134'; 
        keyconObj.Salutation__c = 'Mr'; 
        String orgId = UserInfo.getOrganizationId();
        String dateString = 
        String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        keyconObj.Email__c = uniqueName + '@test' + orgId + '.org';
        keycontactRecords.add(keyconObj);
        
         ContentVersion conVer = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Image')
        );
        
        insert conVer;
        
        ContentVersion conVer1 = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Image')
        );
        
        insert conVer1;
        
        ContentVersion conVer2 = new ContentVersion(
            Title = 'test',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Image')
        );
        
        insert conVer2;
        
        List<ContentVersion> converList= [Select Id,Title,ContentDocumentId from ContentVersion];
        List<String> documentList=new List<String>();
        ContentDocumentObj obj= new ContentDocumentObj();
        obj.filenames=converList[0].Title;
        obj.fileids=converList[0].ContentDocumentId;
        
        ContentDocumentObj obj1= new ContentDocumentObj();
        obj1.filenames=converList[1].Title;
        obj1.fileids=converList[1].ContentDocumentId;
        
        ContentDocumentObj obj2= new ContentDocumentObj();
        obj2.filenames=converList[2].Title;
        obj2.fileids=converList[2].ContentDocumentId;
       
        
        String objType = JSON.serialize(keycontactRecords);
        String filearray='[{"FileNames":"Switched On Contractor Agreement(IMS 1010)","Fileids":"' + converList[0].ContentDocumentId + '"}]';//JSON.serializePretty(arrayOfProducts);
       String filearray1= '[{"FileNames":"Switched On Contractor Agreement(IMS 1010)","Fileids":"' + converList[0].ContentDocumentId + '"},' + 
           '{"FileNames":"MOJ Result","Fileids":"' + converList[1].ContentDocumentId + '"},' + '{"FileNames":"Photograph","Fileids":"' + converList[2].ContentDocumentId + '"}]';

        
        Test.startTest();
        CompanyOnboardingRecordCreation.createRecord(ldList[0].Id,csLst[0],objType,filearray);
        CompanyOnboardingRecordCreation.getLeadDetails(ldList[0].Id);
        TradeWorkerRecordCreation.getAccountDetails(accList[0].Id);
		TradeWorkerRecordCreation.getAccountDetails(null);
        TradeWorkerRecordCreation.createCaseForTradeWorker(accList[0].Id,csLst[1],filearray1);
        Test.stopTest();
    }
    public class ContentDocumentObj {
        public String filenames {get; set;}
        public String fileids {get; set;}        
    }
   
}