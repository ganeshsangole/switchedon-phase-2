/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Lead Trigger Handler for lead conversion
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class LeadTriggerHandler {

   public static void handleMethod(List<Lead> newLeadList, Map<Id,Lead> oldLeadMap){
        if(Trigger.isAfter){
           if(Trigger.isUpdate){
              LeadTriggerHelper.handleLeadConversion(newLeadList,oldLeadMap);
           }
        }
    } 
}