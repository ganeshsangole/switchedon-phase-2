/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Lead Trigger Helper for lead conversion
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
public class LeadTriggerHelper {
    
    //This method will be used to convert Lead
    public static void handleLeadConversion(list<Lead> newLeadList, map<Id,Lead> oldLeadMap){
        try{
        Set<Id> setLeadIds = new Set<Id>();
        
        for (Lead thisLead : newLeadList){
            if(thisLead.Converted_Lead__c  == true && oldLeadMap.get(thisLead.Id).Converted_Lead__c != true){
             setLeadIds.add(thisLead.id);
            }
       
         }
         
         if(!setLeadIds.isEmpty()){
            Database.executeBatch(new BatchCompanyOnbaording(setLeadIds),1);
         }
        }
        catch(Exception e){
       SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','LeadTriggerHelper','handleLeadConversion');
            result.getReturnValue();
      }
    }
}