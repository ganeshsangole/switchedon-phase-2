/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch Links From Custom Metadata
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class Links {
    @AuraEnabled
    public static string getLinks(String linkType){     
        list<Intranet_Link__mdt> linksMetadata = new list<Intranet_Link__mdt>();
        
        linksMetadata = [select id,MasterLabel,Link_URL__c,Link_Type__c,Sort_Order__c from Intranet_Link__mdt where Link_Type__c=:linkType order by Sort_Order__c asc];
        
        return JSON.serialize(linksMetadata); 
        
    }    
    
}