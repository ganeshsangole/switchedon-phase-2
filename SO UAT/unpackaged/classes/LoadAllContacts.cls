/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch All Contact Details From Microsoft
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class LoadAllContacts{
    public static String selectClause=system.label.Delta_Query_Select_Clause;
    public static String contactRecordTypeName=system.label.Contact_Record_Type_Name;
    public static map<string,string>mapFieldMapping=new map<string,string>();
    public static list<Contact> contactList=new list<Contact>();    
    public static Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactRecordTypeName).getRecordTypeId();
    public static String accessToken;
    
    /*FIRST TIME GET ALL CONTACT DETAILS IN SALESFORCE*/ 
    public static void getContacts(string nextPageLink) {  
        try{ String endPoint;           
            if(nextPageLink.length() ==0){
                 accessToken = ApexUtilityClass.requestAccessToken();
                mapFieldMapping=ApexUtilityClass.createFieldMappingMap();
               // endPoint = 'callout:Graph_Explorer'+'?$top=5&$'+selectClause+nextPageLink;
                endPoint = 'callout:Graph_Explorer'+'?$'+selectClause;
            }
            else{
               // endPoint = 'callout:Graph_Explorer'+'?$top=5'+nextPageLink;
                endPoint = 'callout:Graph_Explorer'+nextPageLink;
            }
            
            string nextLink;          
            map<string,string> mapOfHeaderDetails=new map<string,string>();
            
            mapOfHeaderDetails.put('endPoint',endPoint);
            mapOfHeaderDetails.put('methodType','GET');
            mapOfHeaderDetails.put('Prefer','return=minimal');
            mapOfHeaderDetails.put('Authorization', 'Bearer ' + accessToken);
            mapOfHeaderDetails.put('applicationCd','SALESFORCE');
            mapOfHeaderDetails.put('Content-Type','application/json');
            mapOfHeaderDetails.put('accept','application/json');
            
            HttpResponse response = ApexUtilityClass.getAPIResponse(mapOfHeaderDetails);                    
            
            if(response !=null){
            System.debug('response*** : '+response);
            System.debug('response.getBody()****** : '+response.getBody());
            
            if(response.getStatusCode()==200){
                Map<String, Object> results= (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                
                list<Object> objectList=(list<Object>)results.get('value');
                
                if(objectList!=null && !objectList.isEmpty()){
                    if(mapFieldMapping.keySet() !=null && !mapFieldMapping.keySet().isempty()){
                        
                        for(Object objrecord:objectList){
                            Contact contactObject=new Contact(); 
                            Map<String, Object> recordDetails=(Map<String, Object>)objrecord;                            
                            
                            for(string OutlookContactField:mapFieldMapping.keySet()){
                                if(recordDetails.keyset().contains(OutlookContactField)){
                                    contactObject.put(mapFieldMapping.get(OutlookContactField),string.valueOf(recordDetails.get(OutlookContactField)));
                                }
                            }
                            
                            contactObject.Is_Active__c=true;
                            contactObject.RecordTypeId =recordTypeId; 
                            contactList.add(contactObject);              
                        }
                    }
                }
                system.debug('contactList@@@@'+contactList);
                 system.debug('contactListSize@@@@'+contactList.size());
                if(results.keyset().contains('@odata.nextLink')){            
                    nextLink=string.valueOf(results.get('@odata.nextLink'));
                    if(nextLink != null){
                          //  nextLink=nextLink.replace('https://graph.microsoft.com/v1.0/users?$top=5','');
                    nextLink=nextLink.replace('https://graph.microsoft.com/v1.0/users','');      
                    }
                              
                    LoadAllContacts.getContacts(nextLink);  
                }
                system.debug('contactListSize$$$'+contactList.size());
                
                ///*if(contactList !=null && !contactList.isEmpty()){*/
                   /*Database.UpsertResult[] resultUpsert=Database.upsert(contactList, Contact.AD_User_Id__c , false);*/
                ///*}*/
            
            }            
            }
           }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','LoadAllContacts','getContacts');
            result.getReturnValue(); // Log Exception
        }
    }
    
}