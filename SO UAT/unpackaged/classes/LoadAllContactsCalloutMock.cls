/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Send Mock Response.
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
global class LoadAllContactsCalloutMock  implements HttpCalloutMock {
    // Implement this interface method       
    global static HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users","value":[{"businessPhones":[],"displayName":"test","givenName":"test","jobTitle":null,"mail":"test@test.com","mobilePhone":null,"officeLocation":null,"preferredLanguage":"en-IN","surname":"testSurname","userPrincipalName":"accounts@test.com","id":"99000de1-d879-4ac5-a336-24fb99be2181"}]}');
        response.setStatusCode(200);
        return response; 
    }


}