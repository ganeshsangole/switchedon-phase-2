/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "LoadAllContacts" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class LoadAllContactsTest {
    @isTest
    public static  void getContacts_Test(){       
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new LoadAllContactsCalloutMock());  
        LoadAllContacts.getContacts('');
        Test.stopTest(); 
        system.assert(true);
    }
    
}