/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Helper Class For "QuotesApiScheduler" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class QuoteHelperClass {
    @future(callout=true)
    public static void getQuote() {
        string quote; 
        string author; 
        string quoteLabel=system.label.Quote_Label;
        string quoteFieldAPIName=system.label.Quote_Field_API_Name;
        string authorFieldAPIName=system.label.Author_Field_API_Name;
        string IntranetQuoteOfTheDayAPIName=system.label.Intranet_Quote_Of_The_Day_API_Name;
        string quoteCalloutEndpoint=system.label.Quote_Callout_Endpoint;      
        list<Intranet_Quote_Of_The_Day__mdt> ListquoteOfDayRecords=new list<Intranet_Quote_Of_The_Day__mdt>();
        Map<String, Object> metadataFieldValueMap = new Map<String, Object>();
        
        ListquoteOfDayRecords=[select id,Label,Quote__c,Author__c from Intranet_Quote_Of_The_Day__mdt where Label=:quoteLabel limit 1];
        
        //Callout To External Quote API
        Http http = new Http();
        HttpRequest request = new HttpRequest();           
        request.setEndpoint(quoteCalloutEndpoint);            
        request.setMethod('GET');            
        HttpResponse response = http.send(request);             
        
        try{   
            if (response.getStatusCode() == 200){
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                Map<String, Object> results2  = (Map<String, Object>)results.get('contents');
                list<object> objList=(list<object>) results2.get('quotes');
                quote= string.valueOf(((Map<String, Object>)objList[0]).get('quote'));
                author=string.valueOf(((Map<String, Object>)objList[0]).get('author'));             
                
                metadataFieldValueMap.put(quoteFieldAPIName,quote);
                metadataFieldValueMap.put(authorFieldAPIName,author);              
               
               // If there is already a Custom Metadata record then just update it. 
                if(ListquoteOfDayRecords!=null && !ListquoteOfDayRecords.isempty()){ 
                    if(ListquoteOfDayRecords[0].Label != null){
                      CustomMetadataUtils.updateCustomMetadata(IntranetQuoteOfTheDayAPIName,(ListquoteOfDayRecords[0].Label).replaceAll(' ', '_'), ListquoteOfDayRecords[0].Label,metadataFieldValueMap);  
                    }
                    
                }
                //Else create a new Custom Metadata record
                else{
                    CustomMetadataUtils.createCustomMetadata(IntranetQuoteOfTheDayAPIName,quoteLabel,metadataFieldValueMap); 
                }
            }
        }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','QuoteHelperClass','getQuote');
            result.getReturnValue(); // Log Exception
        }
        
    }
    
}