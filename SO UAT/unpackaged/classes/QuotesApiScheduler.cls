/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Scheduled Apex Class To Get Daily Quote 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/

global class QuotesApiScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
       QuoteHelperClass.getQuote();
    }

}