/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "QuotesApiScheduler" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class QuotesApiSchedulerTest {
    @isTest
    public static  void QuotesApiScheduler_Test(){
        QuotesApiScheduler ApiScheduler = new QuotesApiScheduler();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '00 50 14 4 6 ?';
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new quoteOfTheDayCalloutMockSuccess());  
        String jobID = System.schedule('Fetch Quote', sch, ApiScheduler);
        Test.stopTest(); 
        system.assert(true);
    }
    
}