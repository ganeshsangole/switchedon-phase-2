public without sharing class SaveResult {

    //Error messge which get displayed to user
    public static final String ERROR_MESSAGE = 'Something went wrong please contact your administrator. Error Code: @ErrorCode';
    public static final List<String> EXCEPTIONS = new List<String>{
        'Attempt to de-reference a null object',
        'CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY',
        'ENTITY_IS_DELETED',
        'UNKNOWN_EXCEPTION',
        'List index out of bounds',
        'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST',
        'Failed to provide'
    };
	
	SaveResultWrap resultWrap;
    //errors to log
    List<String> errorsToLog;
    //Source 
    String source;
    String sourceFunction;
    //exception
    Exception ex;
    //reference id
    String referenceId;
        
    public SaveResult(){
    //init save result wrapper
        init();
    }
    
    private void init() {
        resultWrap = new SaveResultWrap();
        this.errorsToLog = new List<String>();
    }
    
    public SaveResult(String addErrorMessage) {
     	init();
        addError(addErrorMessage);
    }
    
   
    
    public SaveResult  addError(String message){
    //add error message in to wrapper class
        resultWrap.addError(message);
        return this;
    }

    public SaveResult addErrorToLog(Exception ex, String error, String referenceId, String source, String sourceFunction){
        if(String.isNotBlank(error)){
            this.errorsToLog.add(error); 
        }
        if(ex != null){
            this.ex = ex;
        }
        this.referenceId = referenceId;
        this.source = source;
        this.sourceFunction = sourceFunction;
        return this;
    }
    
    public void addResult(Object result){
    //add success result in to wrapper class
        resultWrap.addResult(result);
    }

    public String getMessage(){
        if(ex != null){
            return (resultWrap != null ? JSON.serialize(resultWrap) : '') +'=>'+ ex.getStackTraceString();
        }else if(errorsToLog != null && !errorsToLog.isEmpty()){
            return (resultWrap != null ? JSON.serialize(resultWrap) : '') +'=>'+ JSON.serialize(errorsToLog);
        }
        return null; 
    }
    
    public String getReturnValue(){
    //provide added errors/success result into serialize format 
        //log exception 
        Application_Log__c logException = new Application_Log__c();
        logException.Source__c = this.source != null ? this.source : 'UI Screen';
        logException.Reference_Id__c = this.referenceId;
        logException.Source_Function__c = this.sourceFunction != null ? this.sourceFunction : 'FETCH DETAILS/SAVE';
        logException.Debug_Level__c = 'ERROR';
        logException.Log_Message__c = getMessage() != null ? getMessage() : JSON.serialize(resultWrap);
        system.debug('in save result : before inserting logexception');
        insert logException;
        //if errors to display
        if(resultWrap.errors != null && !resultWrap.errors.isEmpty()){
            for(String exceptionString : EXCEPTIONS){
                if(resultWrap.errors[0].contains(exceptionString)){
                    //provide error messge for USER
                    resultWrap.clearErrors();
                    if( ERROR_MESSAGE != null){
                       resultWrap.addError(ERROR_MESSAGE.replace('@ErrorCode',[SELECT Name FROM Application_Log__c WHERE Id =:logException.Id].Name));  
                    }
                   
                }
            }
        }else{
            if(ERROR_MESSAGE != null){
                 resultWrap.addError(ERROR_MESSAGE.replace('@ErrorCode',[SELECT Name FROM Application_Log__c WHERE Id =:logException.Id].Name));  
            }
         
        }
        return JSON.serialize(resultWrap);
    }
   
    
    public Application_Log__c getLogValue(String recordId){
    //provide added errors/success result into serialize format 
        //log exception 
        Application_Log__c logException = new Application_Log__c();
        logException.Source__c = this.source != null ? this.source : 'UI Screen';
        logException.Reference_Id__c = this.referenceId;
        logException.Source_Function__c = this.sourceFunction != null ? this.sourceFunction : 'FETCH DETAILS/SAVE';
        logException.Debug_Level__c = 'ERROR';
        
        if(resultWrap.errors != null && !resultWrap.errors.isEmpty()){
           resultWrap.addError('Failed Record Id : '+recordId); 
        }
        logException.Log_Message__c = getMessage() != null ? getMessage() : JSON.serialize(resultWrap);
       
       
        //if errors to display
      /*  if(resultWrap.errors != null && !resultWrap.errors.isEmpty()){
            for(String exceptionString : EXCEPTIONS){
                if(resultWrap.errors[0].contains(exceptionString)){
                    //provide error messge for USER
                    resultWrap.clearErrors();
                    if( ERROR_MESSAGE != null){
                       resultWrap.addError(ERROR_MESSAGE.replace('@ErrorCode',[SELECT Name FROM Application_Log__c WHERE Id =:logException.Id].Name));  
                    }
                   
                }
            }
        }else{
            if(ERROR_MESSAGE != null){
                 resultWrap.addError(ERROR_MESSAGE.replace('@ErrorCode',[SELECT Name FROM Application_Log__c WHERE Id =:logException.Id].Name));  
            }         
        }*/
        return logException;
    }
    

    public class SaveResultWrap{
    //save result wrapper 
        //list of error messages
        List<String> errors;
        //list of successes results
        List<Object> successes;
        
        public void addError(String message){
        //add error
            //debug error
            if(errors == null){
                errors = new List<String>();
            }
            errors.add(message);
            successes = null;
        }
        
        public void clearErrors(){
        //clear errors list
            errors.clear();
        }

        public void addResult(Object result){
        //add success result
            if(successes == null){
                successes = new List<Object>();
            }
            successes.add(result);
            errors = null;
        }
    }
}