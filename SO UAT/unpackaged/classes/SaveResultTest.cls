@isTest
private class SaveResultTest {
	
	@isTest static void test_SaveResult() {
		Test.startTest();
           SaveResult result = new SaveResult('Attempt to de-reference a null object');
            result.getReturnValue();  
            System.assert(true);
		Test.stopTest();
	}
	
	@isTest static void test_AddError() {
		Test.startTest();
		try{
			throw new InvalidDataException('Exception Occured');
			//System.assert(false);
		}catch(Exception ex){
			 SaveResult result = new SaveResult().addError('test error')
            .addErrorToLog(ex,null,null,'test','testmethod');
            result.getReturnValue();  
            System.assert(true);
		}
		Test.stopTest();
	}

	@isTest static void test_AddSuccessResult() {
		Test.startTest();
		new SaveResult().addResult(null);
		System.assert(true);
		Test.stopTest();
	}
	
}