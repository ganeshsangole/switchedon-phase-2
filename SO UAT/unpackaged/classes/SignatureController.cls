/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Controller used to create Signature Pdf
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/

public class SignatureController {
    
    //Method to get Trade Qualification Registration Details
    @AuraEnabled
    public static List<String> getQualificationDetails(String recordId){
        system.debug('Inside get function*****');
    	List<String> qualificaitonExist=new List<String>();
        List<String> tradeQualificationDetails=new List<String>();
        List<Case> caseDetails=[Select Id,Trade_Qualifications_Registrations__c,Trade_Qualifications_Registration_No__c,
                                Licence_Type__c,Expiry_date_of_registration_license__c,Trade_Qualifications_Registrations2__c,
                                Trade_Qualifications_Registration_No2__c,Licence_Type_2__c,Expiry_date_of_registration_license_2__c, 
                                Trade_Qualifications_Registrations_3__c,Trade_Qualifications_Registration_No3__c,Licence_Type_3__c,Expiry_date_of_registration_license_3__c from Case where Id=: recordId];
          for(Case c:caseDetails)
          {
              if(c.Trade_Qualifications_Registrations__c !=null){
              qualificaitonExist.add('TradeQualification1');

              }
              if(c.Trade_Qualifications_Registrations2__c !=null){
           qualificaitonExist.add('TradeQualification2');
              }
              if(c.Trade_Qualifications_Registrations_3__c !=null){
           qualificaitonExist.add('TradeQualification3');
              }
                 
          }
        
      
       system.debug('before return'+ qualificaitonExist);
        return qualificaitonExist;
    }
    // Method to create signature file and signature pdf on click of Save
    @AuraEnabled
    public static void saveSignature(String signatureBody,String recordId){
        
        try{
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
            conVer.PathOnClient = 'Signature Capture.jpg'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Signature Capture'; // Display name of the files
            conVer.VersionData = EncodingUtil.base64Decode(signatureBody);//EncodingUtil.base64Decode(yourFilesContent); // converting your binary string to Blog
            insert conVer;    //Insert ContentVersion
            
            
            // First get the Content Document Id from ContentVersion Object
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            Id conId = [Select Id from ContentVersion where ContentDocumentId=:conDoc].Id;
            //create ContentDocumentLink  record 
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = recordId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
            conDocLink.shareType = 'V';
            insert conDocLink;
            
            List<Case> caseList= [Select Id,VersionId__c from Case where Id=: recordId];
            String versionurl=System.Label.VersionIdURL;
            caseList[0].VersionId__c= versionurl + conId;
            update caseList;
            
            saveSignedPDF(caseList[0].Id);
        }
        catch(Exception e){
            
             SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','SignatureController','saveSignature');
            result.getReturnValue();
        }
    }
    
    // Method to create signature pdf on click of Download
    @auraEnabled
    public static void savePDFCase(String recordId){
        system.debug('Inside download pdf funciton aura'+recordId);
        try{
            PageReference pdfPage = new PageReference('/apex/AttachPDF');
            pdfPage.getParameters().put('Id', recordId);
            
            Blob pdfContent;
            if(Test.isRunningTest()) { 
                pdfContent = blob.valueOf('Unit.Test');
            } else {
                pdfContent = pdfPage.getContent();         
            } 
            
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
            conVer.PathOnClient = 'Trade worker Information.pdf'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'Trade worker Information'; // Display name of the files
            conVer.VersionData = pdfContent;//EncodingUtil.base64Decode(signatureBody);//EncodingUtil.base64Decode(yourFilesContent); // converting your binary string to Blog
            insert conVer;    //Insert ContentVersion
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = recordId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
            conDocLink.shareType = 'V';
            insert conDocLink;
        }
        catch(Exception e){
           
            SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','SignatureController','savePDFCase');
            result.getReturnValue();
        }
        
    }
    
    // Method to create signature pdf called from saveSignature method
    @future(callout=true)
    public static void saveSignedPDF(String recordId){
        
        system.debug('Inside download pdf funciton aura save function'+recordId);
        try{
            PageReference pdfPage = new PageReference('/apex/AttachPDF');
            pdfPage.getParameters().put('Id', recordId);
            Blob pdfContent;
            if(Test.isRunningTest()) { 
                pdfContent = blob.valueOf('Unit.Test');
            } else {
                pdfContent = pdfPage.getContent();         
            }         
            
            ContentVersion conVer1 = new ContentVersion();
            conVer1.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
            conVer1.PathOnClient = 'Trade worker Information.pdf'; // The files name, extension is very important here which will help the file in preview.
            conVer1.Title = 'Trade worker Information'; // Display name of the files
            conVer1.VersionData = pdfContent;//EncodingUtil.base64Decode(signatureBody);//EncodingUtil.base64Decode(yourFilesContent); // converting your binary string to Blog
            insert conVer1;    //Insert ContentVersion
            Id conDoc1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer1.Id].ContentDocumentId;
            
            ContentDocumentLink conDocLink1 = New ContentDocumentLink();
            conDocLink1.LinkedEntityId = recordId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink1.ContentDocumentId = conDoc1;  //ContentDocumentId Id from ContentVersion
            conDocLink1.shareType = 'V';
            insert conDocLink1;
        }
        catch(Exception e){
            
            SaveResult result = new SaveResult().addError(e.getMessage()).addErrorToLog(e,null,'','SignatureController','saveSignedPDF');
            result.getReturnValue();
        }
        
    }
}