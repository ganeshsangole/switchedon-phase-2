/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Data Factory for Master Data
History
17-04-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class TestDataFactory{
    //Account Test Data 
    public class AccountParams{
        public String Name;
    }
    
    public static List<Account> createTestAccounts(Integer numOfRecords,AccountParams accParams){
        List<Account> accountRecords = new List<Account>();
        for(Integer count=0;count<numOfRecords;count++){
            Account accObj = new Account();
            accObj.Name = accParams.Name;
            accountRecords.add(accObj);
        }
        return accountRecords;
    }
    
    //Contact Test Data 
    public class ContactParams{
        
        public String accountId;
        public String firstName;
        public String email;
        public String lastName;
        public DateTime emailbounceddate;
        public String emailBouncedReason;
        public String recordType;
    }
    
    public static List<Contact> createTestContacts(Integer numOfRecords,ContactParams conParams){
        
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(conParams.recordType).getRecordTypeId();
        
        List<Contact> contactRecords = new List<Contact>();
        for(Integer count=0;count<numOfRecords;count++){
            Contact conObj = new Contact();
            conObj.AccountId = conParams.AccountId;
            conObj.FirstName = conParams.firstName;
            conObj.LastName = conParams.LastName;
            conObj.Email = conParams.email;
            conobj.EmailBouncedDate=conParams.emailbounceddate;
            conobj.EmailBouncedReason=conParams.emailBouncedReason;
            conobj.RecordTypeId = conRecordTypeId;
            contactRecords.add(conObj);
        }
        return contactRecords;
    }
    
    //Lead Test Data 
     public class LeadParams{
        public String FirstName;
        public String LastName;
        public String Company;
        public String Email;
        public String Status;
    }
    
    public static List<Lead> createTestLeads(Integer numOfRecords,LeadParams ledParams){
        List<Lead> leadRecords = new List<Lead>();
        for(Integer count=0;count<numOfRecords;count++){
			Lead ledObj = new Lead();
			ledObj.FirstName = ledParams.FirstName; 
            ledObj.LastName = ledParams.LastName;
            ledObj.Company = ledParams.Company;
            ledObj.Status = ledParams.Status;
            String orgId = UserInfo.getOrganizationId();
            String dateString = 
            String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;
            ledObj.Email = uniqueName + '@test' + orgId + '.org';
            
			leadRecords.add(ledObj);
        }
		return leadRecords;
    }

    
    //Campaign Test Data 
    
    public class CampaignParams{
        public String campaignName;
    }
    
    public static List<Campaign> createTestCampaigns(Integer numOfRecords,CampaignParams campParams){
        
        List<Campaign> campaignRecords = new List<Campaign>();
        for(Integer count=0;count<numOfRecords;count++){
            Campaign campObj = new Campaign();
            campObj.Name= campParams.campaignName;
            campaignRecords.add(campObj);
        }
        return campaignRecords;
    }
    
    //Case Test Data 
     public class CaseParams{
        public String Origin;
        public String Status;
    }
    
    public static List<Case> createTestCases(Integer numOfRecords,CaseParams casParams){
        List<Case> caseRecords = new List<Case>();
        for(Integer count=0;count<numOfRecords;count++){
			Case casObj = new Case();
			casObj.Origin = casParams.Origin;
            casObj.Status = casParams.Status;            
			caseRecords.add(casObj);
        }
		return caseRecords;
    }
    
    //CampaignMember Test Data 
    
    public class CampaignMembersParams{
        
        public String contactId;
        public String campaignId;
    }
    
    public static List<CampaignMember> createTestCampaignMembers(Integer numOfRecords,CampaignMembersParams campmemParams){
        
        List<CampaignMember> campaignmemberRecords = new List<CampaignMember>();
        for(Integer count=0;count<numOfRecords;count++){
            CampaignMember campmemberObj = new CampaignMember();
            campmemberObj.CampaignId= campmemParams.campaignId;
            campmemberObj.ContactId=campmemParams.ContactId;
            campaignmemberRecords.add(campmemberObj);
        }
        return campaignmemberRecords;
    }
    //Task Test Data 
    
    public class TaskParams{
        
        
        public String subject;
        public String callType;
        public Date activityDate;
        public String status;
        public string priority;
        public string whoId;
    }
    
    public static List<Task> createTestTask(Integer numOfRecords,TaskParams taskParams){
        
        List<Task> taskRecords = new List<Task>();
        for(Integer count=0;count<numOfRecords;count++){
            Task taskObj = new Task();
            taskObj.subject=taskParams.subject;
            taskObj.Status= taskParams.status;
            taskObj.ActivityDate= taskParams.activityDate;
            taskObj.CallType=taskParams.callType;
            taskObj.Priority= taskParams.priority;
            taskObj.WhoId = taskParams.whoId;
            
            taskRecords.add(taskObj);
        }
        return taskRecords;
    }
    
    
    
    
    //Create Test User Data
    public static void createUserData(){
        list<User>userList=new list<User>();
        Profile p= [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User user1=new User(Date_of_Hire__c=Date.today(), Birthday__c=Date.today(),                 
                            alias = 'utest', email='Unit.Test@unittest.com',emailencodingkey='UTF-8', firstName='First', lastname='Last', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p.id,timezonesidkey='Europe/London', username='abc.xyz@unittest.com',Microsoft_User_Id__c='MS12345'                    
                           );
        User user2=new User(Date_of_Hire__c=Date.today().adddays(1),   Birthday__c= Date.today().adddays(1),
                            alias = 'utest', email='Unit.Test@unittest.com',emailencodingkey='UTF-8', firstName='First', lastname='Last', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p.id,timezonesidkey='Europe/London', username='def.ghi@unittest.com',Microsoft_User_Id__c='MS123'
                           );        
        
        userList.add(user1);
        userList.add(user2);
        insert userList;       
    }
    
    
    // Create Test Contact Record   
    public static void createContactData(){
    
        String employeeRecordTypeName=system.label.Contact_Record_Type_Name;
        Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(employeeRecordTypeName).getRecordTypeId();
        List<Contact> contactList=new list<Contact>();
        
        Contact contactReportee1 = new Contact(Date_Of_Joining__c=Date.today(), Birthdate=Date.today(), firstname='Test',lastname='xyz',Phone='123456',MobilePhone='1234567890',Department='CRM',
                                               RecordTypeId=employeeRecordTypeId,                                              
                                               AD_User_Id__c='455dbb27-0fa6-4839-afb3-00e06f0c9ce5',
                                               AD_Manager_Id__c='5db28930-fc55-48d0-8c58-7604d37bd7bb',
                                               Skip_Employee_Validation__c=true,
                                               AD_Company_Name__c= 'test',
                                               Is_Active__c=true 
                                              );
        
        Contact contactReportee2 = new Contact(Date_Of_Joining__c=Date.today(), Birthdate=Date.today(), firstname='Test',lastname='xyz',Phone='123456',MobilePhone='1234567890',Department='CRM',
                                               RecordTypeId=employeeRecordTypeId,
                                               AD_User_Id__c='455dbb27-0fa6-4839-afb3-00e06f0c9ce6',
                                               AD_Manager_Id__c='5db28930-fc55-48d0-8c58-7604d37bd7bb',
                                               Skip_Employee_Validation__c=true,
                                               AD_Company_Name__c= 'test',
                                               Is_Active__c=true 
                                              );
        
        Contact contactManager = new Contact(Date_Of_Joining__c=Date.today().adddays(1),   Birthdate= Date.today().adddays(1),firstname='Test',lastname='abc',Phone='123456',MobilePhone='1234567890',Department='CRM',
                                             RecordTypeId=employeeRecordTypeId,
                                             AD_User_Id__c='5db28930-fc55-48d0-8c58-7604d37bd7bb',
                                             Skip_Employee_Validation__c=true,
                                             AD_Company_Name__c= 'test',
                                             Is_Active__c=true                                           
                                            );
        
        contactList.add(contactReportee1);
        contactList.add(contactReportee2);
        contactList.add(contactManager);
        insert contactList;
    }
    
    
    // Create Test Audit Log Data
    public static void createAuditLogData(){
        id userId = UserInfo.getUserId();
        list<Audit_Log__c> AuditLogsList=new list<Audit_Log__c>();
        
        AuditLogsList.add(new Audit_Log__c(User__c=userId,Knowledge_Article_Id__c='12345',Knowledge_Article_Name__c='Finance Content',Knowledge_Article_Type__c='Finance'));
        AuditLogsList.add(new Audit_Log__c(User__c=userId,Knowledge_Article_Id__c='54321',Knowledge_Article_Name__c='Finance Content',Knowledge_Article_Type__c='Finance'));
        insert AuditLogsList;
    }
    
    // Create Test Knowledge Article
    public static list<Knowledge__kav> createKnowledgeArticle(){       
        list<Knowledge__kav> knowledgeList = new list<Knowledge__kav >();        
        
        knowledgeList.add(new Knowledge__kav(Title ='Test',UrlName='Test'));        
        insert knowledgeList;        
        return knowledgeList;
    }
    
    // Create ContentVersion Data
    public static List<ContentVersion> createContentVersionData(){       
        List<ContentVersion> contentVersionsList = new List<ContentVersion>();
        ContentVersion cv = new ContentVersion();
        Blob contentBlob = Blob.valueOf('Unit Test Attachment Body');
        cv.title = 'test content';  
        cv.VersionData = contentBlob;  
        cv.PathOnClient = 'test.jpg';
        
        contentVersionsList.add(cv);        
        insert contentVersionsList;
        return contentVersionsList;        
    }
    
    // Create Custom Setting Data
    public static void createCustomSettingData(){ 
        PreventRunningLogic__c setting = new PreventRunningLogic__c();
        setting.Name = 'Test Setting';
        setting.AccountDontRunLogic__c = false;
        setting.ContactDontRunLogic__c = false;
        setting.UserDontRunLogic__c = false;
        insert setting;
    }
    
    
    
}