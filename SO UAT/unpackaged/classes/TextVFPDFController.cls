/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: TextVFPDFController used to create signature pdf
History
10-07-2021 Tranzevo Initial Release for Trade Worker Induction Process 
----------------------------------------------------------------------------------------------*/
public class TextVFPDFController {
 public Case cas{get;set;}
    public TextVFPDFController(){
        Id casId = apexpages.currentpage().getparameters().get('id');
        cas = [select id,First_Name__c,Signature_Of_Inductee__c,VersionId__c,Last_Name__c,AccountId,Gender__c,Visa_Expiry_Date__c,Apprenticeship_Certification_Types__c,Year_Level__c,Is_KO_tenant_family_members_in_KO_prop__c,Ethnicity__c,Date_of_Birth__c,Job_Title__c,Work_Immigration_Status__c,Trade_Qualifications_Registration_No__c,Trade_Qualifications_Registrations__c,
               Licence_Type__c,Licence_Type_2__c,Licence_Type_3__c,Trade_Qualifications_Registrations2__c,Trade_Qualifications_Registrations_3__c,
               Trade_Qualifications_Registration_No2__c,Trade_Qualifications_Registration_No3__c,
               Expiry_date_of_registration_license__c,Expiry_date_of_registration_license_2__c,Close_relationship_with_Kainga_Ora_SOH__c,Expiry_date_of_registration_license_3__c,Working_under_supervision_of__c,Working_Under_Supvson_Of_Registration_No__c,Site_Safe_Qualifications_or_Equivalent__c,Site_Safe_Qualifications_Registration_No__c,Expiry_Date__c,Is_a_Direct_Employee__c,Obligation_under_HSAW_Act_2015__c,Inducted_through_internal_systems_fami__c,Is_Construction_ready__c from Case where id=: casId];
        system.debug('cas'+cas);
    }
}