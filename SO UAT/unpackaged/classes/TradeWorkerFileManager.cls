public without sharing class TradeWorkerFileManager {
    @AuraEnabled(cacheable=true)
public static string userId(){
    return UserInfo.getUserId();
}

      //fetch picklist values from custom object in lwc
      @AuraEnabled(cacheable=true)
      public static List < customValueWrapper > pickListValueDynamically(sObject customObjInfo, string selectPicklistApi) {
         Schema.DescribeSObjectResult objDescribe = customObjInfo.getSObjectType().getDescribe();     
         map < String, Schema.SObjectField > customFieldMap = objDescribe.fields.getMap();      
         list < Schema.PicklistEntry > custPickValues = customFieldMap.get(selectPicklistApi).getDescribe().getPickListValues();
         list < customValueWrapper > customObjWrapper = new list < customValueWrapper > ();
         for (Schema.PicklistEntry myCustPick: custPickValues) {
           customValueWrapper selectOptionValueWrapper = new customValueWrapper();
              selectOptionValueWrapper.custFldlabel = myCustPick.getLabel();
              selectOptionValueWrapper.custFldvalue = myCustPick.getValue();
             customObjWrapper.add(selectOptionValueWrapper);
         }
         
         return customObjWrapper;
   
       }
       // fetch multiselect picklist
       @AuraEnabled(cacheable=true)
  public static List<TradeWorkerFileManager.PickListEntrty> getPickListEntries(String fieldApiName, String objectName){
      List<TradeWorkerFileManager.PickListEntrty> picklistEntries = new List<TradeWorkerFileManager.PickListEntrty>();
      Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
      Schema.DescribeSObjectResult r = s.getDescribe() ;
      Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
      Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for( Schema.PicklistEntry entry : ple){
          String pickListVal = entry.getValue();
          String pickListLabel = entry.getLabel();
          TradeWorkerFileManager.PickListEntrty pentry = new TradeWorkerFileManager.PickListEntrty(pickListLabel, pickListVal);
          picklistEntries.add(pentry);
      }    
      return picklistEntries;
  }

  public class PickListEntrty {
      @AuraEnabled
      public String elementLabel;
      @AuraEnabled
      public String elementValue;

      public PickListEntrty(String label, String value) {
          elementLabel = label;
          elementValue = value;
      }
  }
       // wrapper class 
         public with sharing class CustomValueWrapper {
           @auraEnabled public string custFldlabel {get;set;}
           @auraEnabled public string custFldvalue {get;set;}
         }

     
  

  @AuraEnabled
    public static Map<String,String> getAccountDetails(String accountId){
        String accountIdValue=accountId;
        List<String> AccountDetails= new List<String>();
        Map<String,String> stringOFAccountDetails=new Map<String,String>(); 
      try{
          for(Account acc : [Select Id,Name,Administration_Contact_Email__c,Administration_Contact_Number__c from Account where Id =: accountIdValue]){
                stringOFAccountDetails.put('CompanyName',acc.Name);
                stringOFAccountDetails.put('AdminEmail',acc.Administration_Contact_Email__c);
                stringOFAccountDetails.put('AdminPhone',acc.Administration_Contact_Number__c);
        }
      }
      catch(Exception e){
        throw e;
      }
      return stringOFAccountDetails;
    }

  @AuraEnabled
  public static String createCaseForExxo(String AccountId,Case caserecord,String fileDetails,Boolean showConflictOfInterest){
    try{
      Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Trade Worker Onboarding').getRecordTypeId();
      List<Object> fieldList = (List<Object>)JSON.deserializeUntyped(fileDetails);
      String contactEmail=caserecord.Admin_Email__c;
      system.debug('Account Id'+AccountId);
      system.debug('Contact Email'+contactEmail);
     
       for(Account acc: [Select Id,Administration_Contact__c from Account where Id=:AccountId]){
           caserecord.ContactId= acc.Administration_Contact__c;
       }
       caserecord.First_Value_Of_Base_Location__c= caserecord.Base_Locations__c.substringBefore(';');
       system.debug('caserecord.First_Value_Of_Base_Location__c'+caserecord.First_Value_Of_Base_Location__c);
      caserecord.Subject = 'Trade Worker Onboarding' + '-' + caserecord.SuppliedCompany + '-' + caserecord.First_Name__c + ' ' + caserecord.Last_Name__c;
      caserecord.SuppliedName = caserecord.First_Name__c + ' ' + caserecord.Last_Name__c;
      caserecord.RecordTypeId=recordTypeId; 

         Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true; 
            dmo.EmailHeader.triggerUserEmail = true;
            caserecord.setOptions(dmo);
            
      insert caserecord;

      for(Object fld : fieldList){    
        Map<String,Object> data = (Map<String,Object>)fld;
      
        system.debug('8735673248jhbdcjeg'+data.values());
        system.debug('Data values keyset value'+data.keySet());

        linkFile(caserecord.Id, data.values());

    }

     
    }
    catch(Exception e){
     throw e;
    }
    return caserecord.Id;

    
  }   

  private static void linkFile(String recordId, List<Object> fileDetails){
      try {
       
        List<MOJ_Result__c> mojDetails= new List<MOJ_Result__c>();
        system.debug('Inside linkFile'+fileDetails);
            List<string> lsstr= new List<string> ();

            for(Object a: fileDetails){
                lsstr.add(String.valueOf(a));
            }
            system.debug('List of string'+lsstr);
            system.debug('List oth value'+lsstr.get(0));
            system.debug('List of 1st value'+lsstr.get(1));
            if(String.isBlank(lsstr.get(1)) || String.isBlank(recordId)) {
              return;
          }
          List<MOJ_Result__c> mojRecordExists =[Select Id,Case__c from MOJ_Result__c where Case__c =: recordId];
          system.debug('mojRecordExists'+mojRecordExists);
          system.debug('mojRecordExists Size'+mojRecordExists.size());
          
          if(mojRecordExists.IsEmpty()){
          if(lsstr.get(0).contains('MOJ Result')){
            system.debug('Inside MOJ REsult if');
            MOJ_Result__c mojRecord= new MOJ_Result__c();
            mojRecord.Case__c= recordId;
            mojRecord.Submitted_Date__c = System.today();
            mojRecord.MOJ_Status__c ='In Progress';
            mojDetails.add(mojRecord);
          }
        }
          List<Case> caseDetails=[Select Photo_Version_Id__c from Case where Id=: recordId];
          system.debug('Case details list'+caseDetails);
          List<Case> caseUpdateList = new List<Case>();
          if(lsstr.get(0).contains('Photograph')){
            system.debug('Inside Photograph if');
            Id conId = [Select Id from ContentVersion where ContentDocumentId=:lsstr.get(1)].Id;
            system.debug('Version id'+conId);
            for(Case c: caseDetails)
            {
              system.debug('Inside for loop for photo');
              String photourl=System.Label.VersionIdURL;
              c.Photo_Version_Id__c = photourl + conId;
              caseUpdateList.add(c);
            }
          }
          system.debug('Updated caselist'+caseUpdateList);
          if(caseUpdateList.size() > 0)
          {
            update caseUpdateList;
          }
          List<ContentDocument> cdList= new List<ContentDocument>();
          cdList= [Select Id,Title from ContentDocument where Id=: lsstr.get(1)];
          system.debug('cdList++++++++ before update'+cdList);
         cdList[0].Title = lsstr.get(0);          
          update cdList;
          
          if(mojDetails.size()>0){
            insert mojDetails;
            }
            system.debug('MOJ Details list'+mojDetails);
            system.debug('documentID_____'+lsstr.get(1));
            system.debug('recordId'+recordId);
          system.debug('cdList++++++++'+cdList);
          List<ContentDocumentLink> contentdocumentList= new List<ContentDocumentLink>();
         
          if(mojDetails.size() > 0)
          {
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.contentDocumentId = lsstr.get(1);
            cdl.linkedEntityId = mojDetails[0].Id;
            cdl.shareType = 'V';
            contentdocumentList.add(cdl);
            system.debug('cdl.linkedEntityId for moj result'+cdl.linkedEntityId);
          }
          else if(lsstr.get(0) != 'MOJ Result'){
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.contentDocumentId = lsstr.get(1);
          cdl.linkedEntityId = recordId;
          cdl.shareType = 'V';
          contentdocumentList.add(cdl);
          system.debug('cdl.linkedEntityId for case'+cdl.linkedEntityId);
          }
        
         if(contentdocumentList.size() > 0){
          insert contentdocumentList; 
         }
        
      }catch(Exception e) {
          
          throw e;
      }
  }


}