/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Scheduled Apex Class To Assign Manager To Contact
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
global class UpdateManager implements Schedulable {
    global void execute(SchedulableContext ctx) {
        UpdateManagerBatchApex updateManagerBatchApexObject = new UpdateManagerBatchApex();
        Id batchId = Database.executeBatch(updateManagerBatchApexObject);
    }
}