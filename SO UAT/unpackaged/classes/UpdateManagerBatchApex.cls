/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Batch Apex Class To Assign Manager To Contact
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class UpdateManagerBatchApex implements Database.Batchable<sObject>, Database.Stateful {
    // instance member to retain state across transactions   
    List<Contact> listReporteesToUpsert = new list<Contact>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String contactRecordTypeName=system.label.Contact_Record_Type_Name;
        
        return Database.getQueryLocator(
            'SELECT ID,Name,Email,ReportsToId, AD_Manager_Id__c,AD_User_Id__c FROM Contact Where AD_User_Id__c !=null AND  AD_Manager_Id__c != null ' +
            ' AND RecordType.Name =:contactRecordTypeName'
        );
        
    }
    public void execute(Database.BatchableContext bc, List<Contact> contactList){
        // process each batch of records
        Set <String> managerIds=new Set<String>();
        map<String, List<Contact>> mapManagerAndReporteesList = new map<String, List<Contact>>();
        
        if(contactList !=null && !contactList.isEmpty()){
            for(Contact thisContact :contactList){            
                if(thisContact.AD_Manager_Id__c != null){                
                    managerIds.add(thisContact.AD_Manager_Id__c); 
                    
                    if(mapManagerAndReporteesList.containsKey(thisContact.AD_Manager_Id__c)){                    
                        list<Contact> tempContactList = mapManagerAndReporteesList.get(thisContact.AD_Manager_Id__c);                    
                        tempContactList.add(thisContact);                    
                        mapManagerAndReporteesList.put(thisContact.AD_Manager_Id__c,tempContactList);                    
                    }else{                    
                        mapManagerAndReporteesList.put(thisContact.AD_Manager_Id__c,new list<contact>{thisContact});                                            
                    }                 
                }
            }
        }
        
        
        if(managerIds !=null && !managerIds.isEmpty()){            
            List<Contact> managerList=new List<Contact>();
            
            managerList = [SELECT ID,Name,Email, ReportsToId, AD_Manager_Id__c,AD_User_Id__c FROM Contact Where AD_User_Id__c IN :managerIds];
            
            if(managerList != null && !managerList.isEmpty()){
                for(Contact thisManager : managerList){
                    if(mapManagerAndReporteesList.containsKey(thisManager.AD_User_Id__c)){
                        for(Contact thisReportee: mapManagerAndReporteesList.get(thisManager.AD_User_Id__c)){
                            thisReportee.ReportsToId = thisManager.Id; 
                            thisReportee.AD_Manager_Id__c=null;
                            listReporteesToUpsert.add(thisReportee);
                        }
                    }          
                }
            }
        }
    }
    
    public void finish(Database.BatchableContext bc){
        System.debug('listReporteesToUpsert****'+listReporteesToUpsert);
        // execute any post-processing operations
        if(listReporteesToUpsert != null && !listReporteesToUpsert.isEmpty()){
            list<Database.UpsertResult> resultUpsert=Database.upsert(listReporteesToUpsert,Contact.AD_User_Id__c,false); 
            system.debug('resultUpsert@@@'+resultUpsert);
            ApexUtilityClass.handleDMLResults(resultUpsert,listReporteesToUpsert,'Contact',true,'Contact Manager Batch Status');            
        }        
    }
}