/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "UpdateManager" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class UpdateManagerTest {
    @isTest
    public static  void UpdatedContactDetailsApiScheduler_Test(){
        TestDataFactory.createContactData();
        UpdateManager apiScheduler = new UpdateManager();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '00 50 14 4 6 ?';
        Test.startTest();         
        String jobID = System.schedule('Process Contacts In Batch', sch, apiScheduler);
        Test.stopTest();      
        System.assert(true);
    }
}