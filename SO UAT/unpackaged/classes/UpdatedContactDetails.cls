/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Scheduled Apex Class To Fetch Updated Contact Details From Microsoft
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
global class UpdatedContactDetails  implements  Schedulable {
     global void execute(SchedulableContext sc) 
    {   
        UpdatedContactDetailsHelper.getUpdatedContacts();
    }    
 }