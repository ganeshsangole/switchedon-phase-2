/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description:  Apex Class To Send Mock Response
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@istest
global class UpdatedContactDetailsCalloutMock implements HttpCalloutMock{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
   response.setBody('{"@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users","@odata.deltaLink":"https://graph.microsoft.com/v1.0/users/delta?$deltatoken=MF1LuFYbK6Lw4DtZ4o9PDrcGekRP65WEJfDmM0H26l4v9zILCPFiPwSAAeRBghxgiwsXEfywcVQ9R8VEWuYAB50Yw3KvJ-8Z1zamVotGX2b_AHVS_Z-3b0NAtmGpod","value":[{"displayName":"Testuser7","accountEnabled":"true","companyName":null,"givenName":"Joe","userType":"Member","jobTitle":"test","surname":null,"extensionAttribute1":"27/02/2020","id":"25dcffff-959e-4ece-9973-e5d9b800e8cc","manager@delta":[{"id":"123"}],"onPremisesExtensionAttributes":{"extensionAttribute1":"24/06/2020","extensionAttribute2":"8/03/2021"}},{"displayName":"Testuser7","accountEnabled":null,"companyName":"abc","givenName":"Joe","surname":"Doe","userType":"Member","jobTitle":"test","extensionAttribute1":null,"id":"25dcffff-959e-4ece-9973-e5d9b800e8cc","manager@delta":[{"id":"123"}],"testbooleanfield":"true","onPremisesExtensionAttributes":{"extensionAttribute1":null,"extensionAttribute2":null}},{"id":"8ffff70c-1c63-4860-b963-e34ec660931d","@removed":{"reason":"changed"}}]}');
        response.setStatusCode(200);
        return response; 
    }
    
}