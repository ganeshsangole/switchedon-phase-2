/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Helper Class For "UpdatedContactDetails" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class UpdatedContactDetailsHelper  {   
    
    public static String selectClause = system.label.Delta_Query_Select_Clause;
    public static String employeeRecordTypeName = system.label.Contact_Record_Type_Name;
    public static list<Contact> updatedContactList = new list<Contact>();    
    public static map<string,string> mapFieldMapping = new map<string,string>();
    public static list<Delta_Link_Detail__c> objDeltaLinkList = new list<Delta_Link_Detail__c>();
    public static String deltaLinkRecordName = System.Label.Delta_Link_Record_Name;  
    public static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(employeeRecordTypeName).getRecordTypeId();  
    public static  String accessToken = '';  
    public static String companyAPIName = system.label.Account_Name_Field_API_Name;
    public static String microsoftBirthdayFieldAPIName = system.label.Microsoft_Birthday_Field_API_Name;
    public static String microsoftUserDateOfJoiningFieldAPIName = system.label.Microsoft_User_Date_Of_Joining_Field_API_Name;    
    public static map<string,string> mapOfContactFieldAndItsDataType = new map<string,string>();  
    public static list<Application_Log__c> applicationLogList = new list<Application_Log__c>();
    public static map<string,Contact> mapOfIdAndContact = new map<string,Contact>(); 
    public static String phoneFieldAPIName = system.label.Contact_Phone_Field_API_Name;   
    
    
    /*TO GET UPDATED CONTACT DETAILS*/ 
    @future(callout=true) 
    public static void getUpdatedContacts(){ 
        string nextPageLink = '';
        
        do{                
            String endPoint; 
            
            if(nextPageLink.length() ==0){
                
                accessToken = ApexUtilityClass.requestAccessToken();                
                mapFieldMapping=ApexUtilityClass.createFieldMappingMap(); 
                mapOfContactFieldAndItsDataType = ApexUtilityClass.createMapOfContactFieldAndItsDataType(); 
                objDeltaLinkList=[select id,Delta_Link_URL__c from Delta_Link_Detail__c limit 1];
                if(objDeltaLinkList.isempty()){
                    endPoint = 'callout:Graph_Explorer'+'/delta?$'+selectClause;  
                }
                else{
                    if(objDeltaLinkList[0].Delta_Link_URL__c != null){
                        endPoint = 'callout:Graph_Explorer'+'/delta'+objDeltaLinkList[0].Delta_Link_URL__c.replace('https://graph.microsoft.com/v1.0/users/delta','');   
                    }                        
                }
            }
            else{
                endPoint = 'callout:Graph_Explorer'+'/delta'+nextPageLink; 
            }             
            system.Debug('endPoint'+endPoint);
            map<string,string> mapOfHeaderDetails=new map<string,string>();
            
            mapOfHeaderDetails.put('endPoint',endPoint);
            mapOfHeaderDetails.put('methodType','GET');
            //mapOfHeaderDetails.put('Prefer','return=minimal');
            mapOfHeaderDetails.put('Authorization', 'Bearer ' + accessToken);
            mapOfHeaderDetails.put('applicationCd','SALESFORCE');
            mapOfHeaderDetails.put('Content-Type','application/json');
            mapOfHeaderDetails.put('accept','application/json');            
            
            HttpResponse response = ApexUtilityClass.getAPIResponse(mapOfHeaderDetails);        
            
            if(response !=null){
                System.debug('response.getBody()****** : '+response.getBody());
                
                if(response.getStatusCode() == 200){            
                    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());                    
                    list<Object> objectList = new list<Object>();
                    objectList = (list<Object>)results.get('value');
                    
                    if(objectList!=null && !objectList.isempty()){ 
                        if(mapFieldMapping.keySet() !=null && !mapFieldMapping.keySet().isempty()){
                            
                            for(Object objrecord:objectList){                                
                                Contact contactObject=new Contact(); 
                                Map<String, Object> recordDetails=(Map<String, Object>)objrecord;
                                
                                if(recordDetails.containskey('userType') && recordDetails.containskey('jobTitle')){
                                    system.debug('userType***'+(String)recordDetails.get('userType'));                                                                                              
                                    
                                    if((String)recordDetails.get('userType') == 'Member' && (String)recordDetails.get('jobTitle') != null){ 
                                        try{ 
                                            // Deactivate contact if user removed in AD
                                            if(recordDetails.keyset().contains('@removed')){                   
                                                contactObject.put('Is_Active__c',false);
                                                contactObject.put('AD_Account_Enabled__c',false);
                                                contactObject.put('AD_User_Id__c',string.valueOf(recordDetails.get('id')));
                                            }
                                            else{                                                
                                                for(string outlookContactField:mapFieldMapping.keySet()){                                                    
                                                    if(mapFieldMapping.get(outlookContactField) == phoneFieldAPIName){
                                                        if(recordDetails.containskey(outlookContactField)){                                                   
                                                            if(recordDetails.get(outlookContactField) != null){ 
                                                                list<Object> phoneList = new list<Object>();
                                                                phoneList = (list<Object>)recordDetails.get(outlookContactField);
                                                                if(phoneList != null && !phoneList.isEmpty()){
                                                                    String phone = (String)phoneList[0];
                                                                    contactObject.put(phoneFieldAPIName,phone);      
                                                                }                                                                
                                                            }                                                           
                                                        }
                                                    }
                                                    else{
                                                        if(mapOfContactFieldAndItsDataType.get(mapFieldMapping.get(outlookContactField)) == 'String'){
                                                            System.debug('inside string###'+outlookContactField);
                                                            if(recordDetails.containskey(outlookContactField)){
                                                                if(recordDetails.get(outlookContactField) != null){                                                                  
                                                                    system.debug('string analysis officeLocation***'+String.valueOf(recordDetails.get(outlookContactField)));     
                                                                    contactObject.put(mapFieldMapping.get(outlookContactField),String.valueOf(recordDetails.get(outlookContactField)));             
                                                                }
                                                                else{
                                                                    contactObject.put(mapFieldMapping.get(outlookContactField),null);             
                                                                }
                                                                
                                                            }                                                   
                                                        }
                                                        /*else if(mapOfContactFieldAndItsDataType.get(mapFieldMapping.get(outlookContactField))=='Integer'){
System.debug('inside integer###'+outlookContactField);
if(recordDetails.containskey(outlookContactField)){
if(recordDetails.get(outlookContactField) != null){
contactObject.put(mapFieldMapping.get(outlookContactField),Integer.valueOf(recordDetails.get(outlookContactField)));         
}
else{
contactObject.put(mapFieldMapping.get(outlookContactField),null);          
}

}                                                   
}*/
                                                        
                                                        else if(mapOfContactFieldAndItsDataType.get(mapFieldMapping.get(outlookContactField)) == 'Date'){
                                                            System.debug('Inside date###'+outlookContactField);
                                                            if(recordDetails.containskey(outlookContactField)){
                                                                if(recordDetails.get(outlookContactField) != null){
                                                                    contactObject.put(mapFieldMapping.get(outlookContactField),date.parse((String)recordDetails.get(outlookContactField)));         
                                                                }
                                                                else{
                                                                    contactObject.put(mapFieldMapping.get(outlookContactField),null);         
                                                                }
                                                                
                                                            }                                                     
                                                        }
                                                        else if(mapOfContactFieldAndItsDataType.get(mapFieldMapping.get(outlookContactField)) == 'Boolean'){
                                                            System.debug('Inside boolean###'+outlookContactField);
                                                            if(recordDetails.containskey(outlookContactField)){
                                                                if(outlookContactField == 'accountEnabled'){
                                                                    if(recordDetails.get(outlookContactField) != null){
                                                                        contactObject.put(mapFieldMapping.get(outlookContactField),Boolean.valueOf(recordDetails.get(outlookContactField)));
                                                                        contactObject.put('Is_Active__c',Boolean.valueOf(recordDetails.get(outlookContactField))); 	     
                                                                    }
                                                                    else{
                                                                        contactObject.put(mapFieldMapping.get(outlookContactField),false);
                                                                        contactObject.put('Is_Active__c',false); 	
                                                                    }
                                                                    
                                                                }else{
                                                                    if(recordDetails.get(outlookContactField) != null){
                                                                        contactObject.put(mapFieldMapping.get(outlookContactField),Boolean.valueOf(recordDetails.get(outlookContactField)));     
                                                                    }
                                                                    else{
                                                                        contactObject.put(mapFieldMapping.get(outlookContactField),false); 
                                                                    }
                                                                    
                                                                }
                                                                
                                                            }                                                    
                                                        }
                                                    }
                                                }
                                                
                                                //Handle Birthdate and Date_Of_Joining__c field values
                                                if(recordDetails.containskey('onPremisesExtensionAttributes')){
                                                    Map<String, Object> dateFieldDetails=(Map<String, Object>)recordDetails.get('onPremisesExtensionAttributes'); 
                                                    
                                                    if(dateFieldDetails != null ){
                                                        if(dateFieldDetails.containskey(microsoftUserDateOfJoiningFieldAPIName)){
                                                            if(dateFieldDetails.get(microsoftUserDateOfJoiningFieldAPIName) != null){
                                                                system.debug('date value check *****'+(String)dateFieldDetails.get(microsoftUserDateOfJoiningFieldAPIName));
                                                                contactObject.put(mapFieldMapping.get(microsoftUserDateOfJoiningFieldAPIName), date.parse((String)dateFieldDetails.get(microsoftUserDateOfJoiningFieldAPIName)));     
                                                            }
                                                            else{
                                                                contactObject.put(mapFieldMapping.get(microsoftUserDateOfJoiningFieldAPIName), null);     
                                                            }
                                                        }
                                                        if(dateFieldDetails.containskey(microsoftBirthdayFieldAPIName)){ 
                                                            if(dateFieldDetails.get(microsoftBirthdayFieldAPIName) != null){
                                                                contactObject.put(mapFieldMapping.get(microsoftBirthdayFieldAPIName), date.parse((String)dateFieldDetails.get(microsoftBirthdayFieldAPIName)));     
                                                            }
                                                            else{
                                                                contactObject.put(mapFieldMapping.get(microsoftBirthdayFieldAPIName), null);     
                                                            }
                                                        }  
                                                    }
                                                    
                                                }
                                                
                                                // If Contact's Manager Is Changed
                                                if(recordDetails.keyset().contains('manager@delta')){
                                                    list<Object> listManagerDetails=new list<Object>();
                                                    listManagerDetails=(list<Object>)recordDetails.get('manager@delta');
                                                    Map<String, Object> managerDetails=(Map<String, Object>)listManagerDetails[0];
                                                    String managerId;
                                                    if(managerDetails.keyset().contains('id')){
                                                        managerId=string.valueOf(managerDetails.get('id'));  
                                                    }
                                                    contactObject.AD_Manager_Id__c = managerId;                                       
                                                }                            
                                                
                                            }                                               
                                            contactObject.RecordTypeId = employeeRecordTypeId; 
                                            contactObject.Skip_Employee_Validation__c = true;
                                            mapOfIdAndContact.put(contactObject.AD_User_Id__c,contactObject);                                           
                                            
                                        }
                                        catch(Exception error){                                    
                                            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','UpdatedContactDetailsHelper','getUpdatedContacts');
                                            String recordId = '';
                                            if(recordDetails.containskey('id')){
                                                recordId = (String)recordDetails.get('id');    
                                            }                                            
                                            applicationLogList.add(result.getLogValue(recordId)); // Log Exception
                                        }                                        
                                    }
                                }                                
                            }
                        }
                    }     
                    
                    if(results.keyset().contains('@odata.nextLink')){            
                        nextPageLink=string.valueOf(results.get('@odata.nextLink'));
                        if(nextPageLink != null){
                            nextPageLink=nextPageLink.replace('https://graph.microsoft.com/v1.0/users/delta','');  
                        }                           
                        //continue;                            
                    }
                    else if(results.keyset().contains('@odata.deltaLink')){
                        nextPageLink=null;
                        
                        //Update Delta link In Current Record
                        if(objDeltaLinkList !=null && !objDeltaLinkList.isempty()){                              
                            objDeltaLinkList[0].Delta_Link_URL__c = string.valueof(results.get('@odata.deltaLink')); 
                        }
                        //Else Create New Record To Store Delta Link
                        else{                               
                            Delta_Link_Detail__c deltaLinkRecord=new Delta_Link_Detail__c();
                            deltaLinkRecord.Delta_Link_URL__c=string.valueof(results.get('@odata.deltaLink')); 
                            deltaLinkRecord.Name = deltaLinkRecordName;
                            objDeltaLinkList.add(deltaLinkRecord);
                        }
                        
                    }
                    
                }
                else{
                    // Capture log if Status Code other than 200
                    Application_Log__c applicationLog = new Application_Log__c();               
                    applicationLog.Log_Message__c = 'Response Status Code : '+response.getStatusCode()+'\n'+'Response Status : '+response.getStatus()+'\n'+'Response Body : '+response.getBody();
                    applicationLog.Log_Type__c='Process Log';
                    applicationLogList.add(applicationLog);                   
                }
            }
            
        }
        while(nextPageLink !=null);
        
        if(mapOfIdAndContact.values() != null && !mapOfIdAndContact.values().isEmpty()){
            updatedContactList.addAll(mapOfIdAndContact.values());   
        }
        
        system.Debug('updatedContactListSize'+updatedContactList.size());        
        
        
        //Update Delta Link Record
        if(objDeltaLinkList !=null && !objDeltaLinkList.isempty()){
            system.debug('objDeltaLinkList@@@@'+objDeltaLinkList);  
            upsert objDeltaLinkList;  
        }
        
        
        //Update AccountId On Contact.This logic has been moved to Contact trigger   
        /* mapOfAccountNameAndId = UserTriggerHelper.getMapOfAccountNameAndId(setOfCompanyNames); 

if(updatedContactList !=null && !updatedContactList.isEmpty()){
for(Contact contact:updatedContactList){
if(contact.get(companyAPIName) != null){
contact.AccountId = mapOfAccountNameAndId.get((String)contact.get(companyAPIName));  
}            
} 
}*/
        
        
        //Update Contacts In Salesforce
        if(updatedContactList !=null && !updatedContactList.isEmpty()){
            system.debug('JSONFormatupdatedContactList***'+json.serializePretty(updatedContactList));            
            list<Database.UpsertResult> resultUpsert=Database.upsert(updatedContactList,Contact.AD_User_Id__c,false);                                 
            ApexUtilityClass.handleDMLResults(resultUpsert,updatedContactList,'Contact',true,'M365 & Salesforce Contact Sync Status'); 
        }
        
        // Insert Application Logs   
        if(applicationLogList != null && !applicationLogList.isEmpty()){
            insert applicationLogList;            
        }
    }
    
    
    
}