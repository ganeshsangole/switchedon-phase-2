/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "UpdatedContactDetails" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class UpdatedContactDetailsTest {
     @isTest
    public static  void UpdatedContactDetailsApiScheduler_Test(){
       UpdatedContactDetails apiScheduler = new UpdatedContactDetails();
       // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
       String sch = '00 50 14 4 6 ?';
       Test.startTest();  
       Test.setMock(HttpCalloutMock.class, new UpdatedContactDetailsCalloutMock());  
       String jobID = System.schedule('Fetch Updated Contacts Details', sch, apiScheduler);
       Test.stopTest();      
        System.assert(true);
       }
}