/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "UserTriggerHandler" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class UserTriggerHandlerTest {
    @testSetup 
    static void createCustomSettingData() {
        TestDataFactory.createCustomSettingData();   
    }
    
    @istest
    public static void HandleTriggerLogic_Test(){        
        TestDataFactory.createUserData();
    }  
}