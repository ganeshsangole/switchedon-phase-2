/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Helper Apex Class To Handle "UserTrigger" Logic
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class UserTriggerHelper {
    
    public static void getfilteredUserList(List<User> newUserList){
        Set<String> setOfCompanyNames=new Set<String>();
        Set<Id> filteredUserIds=new Set<Id>();
        List<User> filteredUserList=new List<User>();
        
        // Filter Those Users Where "Microsoft_User_Id__c" Is Present 
        if(newUserList !=null && !newUserList.isEmpty()){
            for(User user:newUserList){
                if(user.Microsoft_User_Id__c !=null){
                    filteredUserList.add(user);
                    filteredUserIds.add(user.id);
                    if(user.CompanyName !=null){
                        setOfCompanyNames.add(user.CompanyName);
                    }                    
                }
            }            
            
            if(filteredUserIds !=null && !filteredUserIds.isEmpty()){
                assignPermissionSet(filteredUserList);  
                createContacts(filteredUserIds,setOfCompanyNames);                       
            }
        }
    }
    
    
    /*Create Contacts Asynchronously */    
    @Future
    public static void createContacts(Set<Id> filteredUserIds,Set<String> setOfCompanyNames){
        try{            
            List<User> filteredUserList = new List<User>();  
            filteredUserList = [select id,Department,Microsoft_User_Id__c,CompanyName,firstName,lastname,MobilePhone,Phone,Title,Office_Location__c,Birthday__c,Date_of_Hire__c,Email from User where id in:filteredUserIds];   
            
            if(filteredUserList !=null && !filteredUserList.isEmpty()){
                
                map<string,string> mapFieldMapping = new map<string,string>();
                list<UserAndContactFieldMapping__mdt> listFieldMapping = new list<UserAndContactFieldMapping__mdt>();
                String employeeRecordTypeName = system.label.Contact_Record_Type_Name;
                Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(employeeRecordTypeName).getRecordTypeId();             
                list<Contact> contactList = new list<Contact>(); 
                map<String,String> mapOfContactFieldAndItsDataType = new map<String,String>();
                System.debug('filteredUserList###'+filteredUserList);
                
                listFieldMapping = [select id,User_Field__c,Contact_Field__c,Data_Type__c from UserAndContactFieldMapping__mdt];                
                
                //Create "Map Of Contact Field And User Field" And "Map Of Contact Field And Its Data Type"
                if(listFieldMapping !=null && !listFieldMapping.isempty()){
                    for(UserAndContactFieldMapping__mdt fieldMappingRecord:listFieldMapping){
                        if(fieldMappingRecord.User_Field__c !=null && fieldMappingRecord.Contact_Field__c !=null && fieldMappingRecord.Data_Type__c !=null){
                            mapFieldMapping.put(fieldMappingRecord.Contact_Field__c,fieldMappingRecord.User_Field__c); 
                            mapOfContactFieldAndItsDataType.put(fieldMappingRecord.Contact_Field__c,fieldMappingRecord.Data_Type__c);                     
                        }
                    }
                } 
                
                if(mapFieldMapping.keyset() != null && !mapFieldMapping.keyset().isEmpty() && mapOfContactFieldAndItsDataType.keyset() !=null && !mapOfContactFieldAndItsDataType.keyset().isEmpty()){
                    for(User user:filteredUserList){
                        Contact contact = new Contact();
                        for(String contactField: mapFieldMapping.keyset()){
                            if(mapOfContactFieldAndItsDataType.get(contactField)=='String'){
                                System.debug('inside string###'+contactField);
                                if(user.get(mapFieldMapping.get(contactField)) != null){
                                    contact.put(contactField,String.valueOf(user.get(mapFieldMapping.get(contactField))));           
                                }
                                else{
                                    contact.put(contactField,null);  
                                }
                                
                            }
                                                            /*else if(mapOfContactFieldAndItsDataType.get(contactField)=='Integer'){
                                System.debug('inside integer###'+contactField);
                                
                                if(user.get(mapFieldMapping.get(contactField)) != null){
                                contact.put(contactField,Integer.valueOf(user.get(mapFieldMapping.get(contactField))));   
                                }
                                else{
                                contact.put(contactField,null);  
                                }
                                }*/
                            
                            else if(mapOfContactFieldAndItsDataType.get(contactField)=='Date'){
                                System.debug('Inside date###'+contactField);
                                
                                if(user.get(mapFieldMapping.get(contactField)) != null){
                                    contact.put(contactField,Date.valueOf(user.get(mapFieldMapping.get(contactField)))); 
                                }
                                else{
                                    contact.put(contactField,null);  
                                }
                            }
                                                    /*  else if(mapOfContactFieldAndItsDataType.get(contactField)=='Boolean'){
                        System.debug('Inside boolean###'+contactField);                                     
                        if(user.get(mapFieldMapping.get(contactField)) != null){
                        contact.put(contactField,Boolean.valueOf(user.get(mapFieldMapping.get(contactField))));
                        }
                        else{
                        contact.put(contactField,null);  
                        }
                        }*/
                                                }
                        contact.RecordTypeId=recordTypeId;
                        contact.Is_Active__c=true;
                        contact.Skip_Employee_Validation__c=true;
                        contactList.add(contact);                    
                    }
                    
                    System.debug('contactList###'+contactList);
                    if(contactList !=null && !contactList.isEmpty()){
                        Database.upsert(contactList,Contact.AD_User_Id__c,false);                 
                    }
                }            
                
            }
        }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','UserTriggerHelper','createContacts');
            result.getReturnValue(); // Log Exception
        }
    }
    
    
    
    
    /*Assign Permission Set To Users*/ 
    public static void assignPermissionSet(List<User> filteredUserList){        
        if(filteredUserList !=null && !filteredUserList.isEmpty()){
            String EmployeeUserPermissionSetId = system.label.Employee_User_Permission_Set_Id;
            List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
            
            for(User user:filteredUserList){
                PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = EmployeeUserPermissionSetId, AssigneeId = user.Id);
                permissionSetList.add(psa);
            }
            
            if(permissionSetList !=null && !permissionSetList.isEmpty()){
                upsert permissionSetList;           
            }            
        }
    }
    
    
    
    /*Create Map Of Account Name And Id */    
    public static map<String,Id> getMapOfAccountNameAndId(Set<String>setOfCompanyNames){ 
        List<Account> accountList = new list<Account>();
        map<String,Id> accountNameAndIdMap = new map<String,Id>();
        
        if(setOfCompanyNames != null && !setOfCompanyNames.isEmpty()){
            accountList = [Select Id,Name from Account where Name in:setOfCompanyNames];
            
            if(accountList !=null && !accountList.isEmpty()){
                for(Account accRecord : accountList){
                    accountNameAndIdMap.put(accRecord.Name, accRecord.Id);
                }
            }   
        }        
        return accountNameAndIdMap; 
    }
}