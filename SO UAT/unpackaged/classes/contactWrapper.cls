/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Wrapper Apex Class To Store Contacts Details 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class contactWrapper {
    @auraEnabled public string name;
    @auraEnabled public string Department;
    @auraEnabled public string role;
    @auraEnabled public string Email;
    @auraEnabled public string phone;
    @auraEnabled public string MobilePhone;
    @auraEnabled public string profilePhoto;
    @auraEnabled public string ExternalId; 
    @auraEnabled public string MailToLink; 
    @auraEnabled public string officeLocation;
    @auraEnabled public string reportsToName;
    @auraEnabled public string reportsToRole;
    @auraEnabled public string employeeTeam;
    
    
    public contactWrapper(Contact contactRecord){
        
        name=contactRecord.Name;        
        role=contactRecord.AD_Title__c; 
        Email=contactRecord.Email;
        phone=contactRecord.Phone; 
        MobilePhone=contactRecord.MobilePhone;          
        ExternalId=contactRecord.AD_User_Id__c;
        reportsToName=contactRecord.ReportsTo.Name;
        reportsToRole=contactRecord.ReportsTo.AD_Title__c; 
        employeeTeam=contactRecord.AD_Department__c;
        officeLocation=contactRecord.AD_Location__c;  
        
        /* if(contactRecord.Base_Location__c != null){
officeLocation=contactRecord.Base_Location__c.replaceAll(';',',');  
}*/
        
    }
    
}