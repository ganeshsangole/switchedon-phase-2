/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch Urgent Notifications From Custom Metadata
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class floatingText {        
    @AuraEnabled   
    public static string getAllNotifications(){         
        List<notificationsWrapper> notificationWrapperList = new list<notificationsWrapper>();
        list<Intranet_Urgent_Notifications__mdt> notificationsData=new list<Intranet_Urgent_Notifications__mdt>();
        notificationsData=[select id,Notification__c,Sort_Order__c from Intranet_Urgent_Notifications__mdt order by Sort_Order__c asc];
        
        if(notificationsData !=null && !notificationsData.isempty()){
            for(Intranet_Urgent_Notifications__mdt notification:notificationsData){
                if(notification.Notification__c !=null && notification.Sort_Order__c != null){
                    notificationWrapperList.add(new notificationsWrapper(notification));  
                }
            }
        }
        
        return JSON.serialize(notificationWrapperList); 
      
    }
}