/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Controller apex class to fetch Birthday/Work Anniversary Details used in birthdayWorkAnniversaryLWC component.
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public without sharing class getBirthdayAnniversaryDetails {
    //Get birthday or Work Anniversary details as per the values passed from design attributes 
    @AuraEnabled(cacheable=true) 
    public static string getDetails(string IsbDayOrWorkSection,string timeDuration){
        
        try{
            integer currentDayInMonth = date.today().day(); 
            integer currentMonth = date.today().month();                
            integer currentDayInYear = date.today().dayOfYear();        
            string datefieldApiName;
            String birthdayFieldApiName = system.label.Birthday_Field_API_Name;
            string dateOfJoiningFieldApiName = system.label.Date_Of_Joining_Field_API_Name;
            list<userWrapper> listUserWrapper = new list<userWrapper>();
            list<Contact> contactList = new list<Contact>();            
            
            if(IsbDayOrWorkSection == 'Birthday'){
                datefieldApiName = birthdayFieldApiName;
            }
            else{
                datefieldApiName = dateOfJoiningFieldApiName;  
            }
            
            if(timeDuration == 'today'){            
                if(IsbDayOrWorkSection == 'Birthday'){
                    contactList = [select id,name,Birthdate,AD_User_Id__c from Contact where CALENDAR_MONTH(Birthdate)=:currentMonth and DAY_IN_MONTH(Birthdate)=:currentDayInMonth order by name asc];                     
                }
                else if(IsbDayOrWorkSection == 'Work Anniversary'){
                    contactList = [select id,name,Date_Of_Joining__c,AD_User_Id__c from Contact where CALENDAR_MONTH(Date_Of_Joining__c)=:currentMonth and DAY_IN_MONTH(Date_Of_Joining__c)=:currentDayInMonth order by name asc];                    
                }
            }
            
            else if(timeDuration == 'week'){ 
                if(IsbDayOrWorkSection == 'Birthday'){
                    contactList = [select id,name,Birthdate,AD_User_Id__c from Contact where DAY_IN_YEAR(Birthdate)>=:currentDayInYear-6 and DAY_IN_YEAR(Birthdate) <=: currentDayInYear+6 order by name asc];                     
                }
                else if(IsbDayOrWorkSection == 'Work Anniversary'){
                    contactList = [select id,name,Date_Of_Joining__c,AD_User_Id__c from Contact where  DAY_IN_YEAR(Date_Of_Joining__c)>=:currentDayInYear-6 and DAY_IN_YEAR(Date_Of_Joining__c) <=: currentDayInYear+6 order by name asc];                    
                }            
            }
            
            contactList = getBirthdayAnniversaryDetails.sortContacts(contactList,datefieldApiName,timeDuration,IsbDayOrWorkSection);                   
            
            if(contactList != null && !contactList.isEmpty()){
                for(Contact contact:contactList){
                    String contactImage;
                    if(contact.AD_User_Id__c != null){
                        contactImage = FetchOutlookContact.getImageBlocks(contact.AD_User_Id__c); 
                    }                    
                    listUserWrapper.add(new userWrapper(contact,datefieldApiName,contactImage));                    
                }
            }
            system.debug('listUserWrapper$$$'+listUserWrapper);
            return JSON.serialize(listUserWrapper);       
        }
        catch(Exception error){
            SaveResult result = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','getBirthdayAnniversaryDetails','getDetails');
            result.getReturnValue(); // Log Exception
        }
        return JSON.serialize(null);
    }
    
    
    //Sort contacts data on the basis of "Date Of Joining"/"Birthdate"  
    @AuraEnabled
    public static list<Contact> sortContacts(list<Contact>listContacts,string datefieldApiName,string timeDuration,String IsbDayOrWorkSection){
        map<date,list<Contact>> dateAndContactsListMap = new map<date,list<Contact>>();
        list<Contact> sortedContactsDetails = new list<Contact>();
        integer maxIteration = 0;
        
        if(timeDuration == 'today'){
            maxIteration = 1;
            for(integer i=0;i<maxIteration;i++){
                dateAndContactsListMap.put(date.today().adddays(i),new list<Contact>()); 
            }
        }        
        else if(timeDuration == 'week'){
            maxIteration = 7;
            for(integer i=0;i<maxIteration;i++){
                dateAndContactsListMap.put(date.today().toStartofWeek().adddays(i+1),new list<Contact>()); 
            }
        }        
        
        if(listContacts != null && !listContacts.isempty()){
            for(Contact contactRecord:listContacts){            
                if(contactRecord.get(datefieldApiName) != null){
                    if(((date)contactRecord.get(datefieldApiName)).month() == 2 && ((date)contactRecord.get(datefieldApiName)).day() == 29 ){
                        if(!date.isleapyear(date.today().year())){
                            continue;
                        }
                        else{
                            dateAndContactsListMap = getBirthdayAnniversaryDetails.getDateAndContactsListMap(dateAndContactsListMap,contactRecord,datefieldApiName,IsbDayOrWorkSection);
                        }
                    }                
                    else{                    
                        dateAndContactsListMap = getBirthdayAnniversaryDetails.getDateAndContactsListMap(dateAndContactsListMap,contactRecord,datefieldApiName,IsbDayOrWorkSection);                
                    }
                }            
                //record has blank date 
                else{
                    continue;
                }
            }
            if(dateAndContactsListMap.values() != null && !dateAndContactsListMap.values().isEmpty()){
                for(list<Contact> listContactRecords : dateAndContactsListMap.values()){
                    if(listContactRecords != null && !listContactRecords.isempty()){
                        for(Contact contact:listContactRecords){
                            sortedContactsDetails.add(contact);
                        }
                    }
                    else{
                        continue;
                    }
                }
            }            
        }        
        return sortedContactsDetails;
    }
    
    
    //This method is used to create map of date and corresponding list of contacts.
    @AuraEnabled
    public static map<date,list<Contact>> getDateAndContactsListMap(map<date,list<Contact>> dateAndContactsListMap,Contact contactRecord,string datefieldApiName,String IsbDayOrWorkSection){
        date tempDate = (date)contactRecord.get(datefieldApiName);        
        integer noOfYrs = date.today().toStartofWeek().year() - tempDate.year();
        
        //If any contact has Birthdate/Work Anniversary in January but 
        //that date is a part of December's last week then format it accordingly  
        if(date.today().toStartofWeek().year() != date.today().toStartofWeek().adddays(6).year()){
            if(tempDate.month() == 1){
                noOfYrs = noOfYrs+1;
            }
        } 
        
        date formattedDate = tempDate.addYears(noOfYrs);
        
        //If its Birthday then display formatted date otherwise display original date 
        if(IsbDayOrWorkSection == 'Birthday'){
            contactRecord.put(datefieldApiName,formattedDate);      
        }         
        
        if(dateAndContactsListMap.containskey(formattedDate)){
            dateAndContactsListMap.get(formattedDate).add(contactRecord);  
        }        
        return  dateAndContactsListMap;        
    }
    
}