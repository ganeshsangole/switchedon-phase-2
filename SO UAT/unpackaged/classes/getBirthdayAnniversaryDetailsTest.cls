/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test class for "getBirthdayAnniversaryDetails" apex class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class getBirthdayAnniversaryDetailsTest {    
    
    @isTest
    public static void getDetails_todaysBdayTest(){       
        TestDataFactory.createContactData();
        String result;
        Test.startTest();   
        result=getBirthdayAnniversaryDetails.getDetails('Birthday','today');
        Test.stopTest();      
        system.assert(true);
    }
    
    @isTest    
    public static void getDetails_todaysAnniversaryTest(){        
        String result;
        Test.startTest();   
        result=getBirthdayAnniversaryDetails.getDetails('Work Anniversary','today');
        Test.stopTest();        
        system.assert(true);
    }    

    @isTest
    public static void getDetails_thisWeeksBdayTest(){        
        String result;
        Test.startTest();   
        result=getBirthdayAnniversaryDetails.getDetails('Birthday','week');
        Test.stopTest();       
        system.assert(true);
    }
    
    @isTest
    public static void getDetails_thisWeeksAnniversaryTest(){       
        String result;
        Test.startTest();   
        result=getBirthdayAnniversaryDetails.getDetails('Work Anniversary','week');
        Test.stopTest();
        system.assert(true);
    }    
    
}