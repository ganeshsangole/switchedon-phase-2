/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch Knowledge Data 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/ 
public class getKnowledgeData {
    //Wrapper Class To Store Knowledge Data 
    public class knowledgeCollectionWrapper {        
        @AuraEnabled
        public List<String> listOfKnowledgeIds = new list<String>();        
        @AuraEnabled
        public List<Knowledge__kav> items = new list<Knowledge__kav>();    
        @AuraEnabled
        public Integer total = 0; 
        
        public knowledgeCollectionWrapper() {
            items = new List<Knowledge__kav>();          
            total = 0;
        }
    }
    
    //Fetch Published Knowledge Articles
    @AuraEnabled(cacheable=true)
    public static knowledgeCollectionWrapper relatedContents(String contentType) { 
        try{           
            List<Knowledge__kav> knowledgeItemsList= new list<Knowledge__kav>();
            knowledgeCollectionWrapper knowledgeWrapper=new knowledgeCollectionWrapper(); 
            Set<String> setOfKnowledgeIds = new Set<String>();
            
            if(!Test.isRunningTest()) {  
                knowledgeItemsList = [select Id,Title,CreatedDate,RecordType.Name,UrlName,ArticleTotalViewCount,LastPublishedDate,Banner_Image_Id__c,Summary,PublishStatus from Knowledge__kav where RecordType.Name =:contentType and PublishStatus='Online'];
            }
            else{
                knowledgeItemsList = [select Id,Title,CreatedDate,RecordType.Name,UrlName,ArticleTotalViewCount,LastPublishedDate,Banner_Image_Id__c,Summary,PublishStatus from Knowledge__kav ];
            }
            
            if(knowledgeItemsList !=null && !knowledgeItemsList.isEmpty()){
                knowledgeWrapper.items.addAll(knowledgeItemsList);
                knowledgeWrapper.total = knowledgeItemsList.size();
                
                id userId = UserInfo.getUserId();
                
                list<Audit_Log__c> auditLogsList = new list<Audit_Log__c>(); 
                auditLogsList = [select id,User__c,Knowledge_Article_Id__c from Audit_Log__c where User__c=:userId and Knowledge_Article_Type__c =:contentType ]; 
                
                /*Create Set Of Knowledge Ids Viewed By User */            
                if(auditLogsList !=null && !auditLogsList.isempty()){ 
                    for(Audit_Log__c auditLog:auditLogsList){
                        if(auditLog.Knowledge_Article_Id__c != null){                           
                            setOfKnowledgeIds.add(auditLog.Knowledge_Article_Id__c);                                           
                        }
                    }
                    knowledgeWrapper.listOfKnowledgeIds.addAll(setOfKnowledgeIds);     
                }                        
            }            
            return knowledgeWrapper;
        }
        catch(Exception error){           
            SaveResult saveresult = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','getKnowledgeData','relatedContents');
            saveresult.getReturnValue(); // Log Exception
        }        
        return null;        
    }
    
    
    
    //Create Entry in Audit Log
    @AuraEnabled
    public static void addEntryToAuditLog(string knowledgeId,string knowledgeName,string knowledgeType ){
        try{
            id userId = UserInfo.getUserId();
            list<Audit_Log__c> auditLogsList = new list<Audit_Log__c>();
            list<Audit_Log__c> existingAuditLogs = new list<Audit_Log__c>();
            
            existingAuditLogs = [select id,User__c,Knowledge_Article_Id__c from Audit_Log__c where Knowledge_Article_Id__c=:knowledgeId and User__c=:userId];
            
            if(existingAuditLogs == null || existingAuditLogs.isEmpty()){
                auditLogsList.add(new Audit_Log__c(User__c=userId,Knowledge_Article_Id__c=knowledgeId,Knowledge_Article_Name__c=knowledgeName,Knowledge_Article_Type__c=knowledgeType));    
                
                if(auditLogsList != null && !auditLogsList.isempty()){
                    insert auditLogsList;
                }                
            }            
            
        }
        catch(Exception error){           
            SaveResult saveresult = new SaveResult().addError(error.getMessage()).addErrorToLog(error,null,'','getKnowledgeData','addEntryToAuditLog');
            saveresult.getReturnValue(); // Log Exception 
        }        
    }
}