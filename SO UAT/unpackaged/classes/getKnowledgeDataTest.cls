/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Test Class For "getKnowledgeData" Apex Class
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
public class getKnowledgeDataTest {    
    
    @isTest
    public static void addEntryToAuditLog_Test(){           
        Test.startTest();   
        getKnowledgeData.addEntryToAuditLog('98760','Test Content','News');
        Test.stopTest(); 
        system.assert(true);
    }
    
    @isTest
    public static void relatedContents_Test(){
        list<Knowledge__kav> knowledgeList = new list<Knowledge__kav >();
        
        knowledgeList = TestDataFactory.createKnowledgeArticle();
        TestDataFactory.createAuditLogData();
        
        Test.startTest();   
        getKnowledgeData.relatedContents('Finance');
        Test.stopTest(); 
        system.assert(true);
    }
}