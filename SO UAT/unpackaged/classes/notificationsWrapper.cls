/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Wrapper Class To Store Urgent Notifications Details 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class notificationsWrapper {
    @auraEnabled public string notification;
    @auraEnabled public decimal sortOrder;
    
    public notificationsWrapper(Intranet_Urgent_Notifications__mdt notificationRecord){
        notification = notificationRecord.Notification__c;
        sortOrder = notificationRecord.Sort_Order__c;
    }

}