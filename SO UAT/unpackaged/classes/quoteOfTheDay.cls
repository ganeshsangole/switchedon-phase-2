/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Fetch Daily Quote From Custom Metadata
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class quoteOfTheDay {
    @AuraEnabled 
    public static quoteWrapper getQuote(){    
        string quote; 
        string author; 
        string quoteLabel=system.label.Quote_Label;
        list<Intranet_Quote_Of_The_Day__mdt> ListquoteOfDayRecords=new list<Intranet_Quote_Of_The_Day__mdt>();
        
        ListquoteOfDayRecords=[select id,Label,Quote__c,Author__c from Intranet_Quote_Of_The_Day__mdt where Label=:quoteLabel limit 1];        
   
        if(ListquoteOfDayRecords !=null && !ListquoteOfDayRecords.isEmpty()){            
         quote=ListquoteOfDayRecords[0].quote__c;
         author=ListquoteOfDayRecords[0].author__c;     
        }                     
       return new quoteWrapper(quote,author);          
    }    
}