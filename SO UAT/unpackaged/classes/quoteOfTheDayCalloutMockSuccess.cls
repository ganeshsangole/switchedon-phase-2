/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Send Success Mock Response.
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
@isTest
global class quoteOfTheDayCalloutMockSuccess implements HttpCalloutMock{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"success":{"total":1},"contents":{"quotes":[{"quote":"We are each gifted in a unique and important way. It is our privilege and our adventure to discover our own special light","length":"121","author":"Mary Dunbar","tags":["discover","inspire","self"],"category":"inspire","language":"en","date":"2021-05-31","permalink":"https://theysaidso.com/quote/mary-dunbar-we-are-each-gifted-in-a-unique-and-important-way-it-is-our-privilege","id":"NMrdeKJZAouC1i06aoMm3weF","background":"https://theysaidso.com/img/qod/qod-inspire.jpg","title":"Inspiring Quote of the day"}]},"baseurl":"https://theysaidso.com","copyright":{"year":2023,"url":"https://theysaidso.com"}}');
        response.setStatusCode(200);
        return response; 
    }

}