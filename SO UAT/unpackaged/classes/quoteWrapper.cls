/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Wrapper Apex Class To Store Quote Details 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class quoteWrapper {   
     @auraEnabled  public string quote;
     @auraEnabled public string author;
    
    public quoteWrapper(string quoteDetails, string authorDetails){
         quote=quoteDetails;
         author=authorDetails;
    }

}