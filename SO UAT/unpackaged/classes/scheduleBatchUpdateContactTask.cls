global class scheduleBatchUpdateContactTask implements Schedulable {
   global void execute(SchedulableContext sc) {
      BatchUpdateContactTask batchupdatetask = new BatchUpdateContactTask(); 
      database.executebatch(batchupdatetask);
   }
}