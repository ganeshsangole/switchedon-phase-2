/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Class To Get Contacts Details 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public without sharing class search {
    
    @AuraEnabled
    public static string getContacts(String contactName){        
        list<contactWrapper>listcontactWrapper=new list<contactWrapper>();
        list<Contact> contactList=new list<Contact>();
        String ContactRecordTypeName=system.label.Contact_Record_Type_Name;
        
        contactList=[select id,name,AD_Company_Name__c,AD_Department__c,Email,AD_Title__c,Phone,MobilePhone,AD_User_Id__c,ReportsTo.Name,ReportsTo.AD_Title__c,Base_Location__c,AD_Location__c from Contact where Name like:'%'+contactName+'%' and RecordType.Name =: ContactRecordTypeName and Is_Active__c = true];
        
        if(contactList !=null && !contactList.isEmpty()){
            for(Contact contactRec:contactList){
                listcontactWrapper.add(new contactWrapper(contactRec));  
            }
        }        
        return json.serialize(listcontactWrapper); 
    }
}