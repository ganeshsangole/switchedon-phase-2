public class sendCampaignEmail {
    //7015D000000SacVQAS sendCampaignEmail.sendEmail('7015D000000SacVQAS');
    public static void sendEmail(Id campaignId){
         list<CampaignMember> listCampaignMembers = new list<CampaignMember>();
         listCampaignMembers = [Select Id,ContactId,Email from CampaignMember where CampaignId = :campaignId];
         set<Id> setContactIds = new set<Id>();
        
        for(CampaignMember thisMember : listCampaignMembers){
            setContactIds.add(thisMember.ContactId);
        }
        
        list<Id> listContacts = new list<Id>(setContactIds);
        
        EmailTemplate et = [Select Id, Name from EmailTemplate where name = 'INFORM_ITC_096_Stock_Shortage_Centreflush template builder' limit 1];
        
        Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
        mail.setTargetObjectIds(listContacts);
        mail.setSenderDisplayName('Info');
        mail.setTemplateId(et.id);
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
        
    }
}