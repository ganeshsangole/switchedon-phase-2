/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Wrapper apex class to store contacts details 
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/
public class userWrapper {
    
    @auraEnabled public string dateValue;
    @auraEnabled public string name;
    @auraEnabled public string profilePhoto;
    
    public userWrapper(Contact userRecord,string DatefieldApiName,String contactImg){  
        dateValue = string.valueOf(date.valueof(userrecord.get(DatefieldApiName)).day())+' '+userWrapper.getMapMonthInStringFormat().get(date.valueof(userrecord.get(DatefieldApiName)).month())+' '+string.valueOf(date.valueof(userrecord.get(DatefieldApiName)).year());
        name = string.valueof(userRecord.name);     
        profilePhoto = contactImg;          
    }
    
    public static  map<integer,string> getMapMonthInStringFormat(){
        map<integer,string> mapOfIntegerStringMonthFormat = new map<integer,string>();
        
        mapOfIntegerStringMonthFormat.put(1,'January');
        mapOfIntegerStringMonthFormat.put(2,'February');
        mapOfIntegerStringMonthFormat.put(3,'March');
        mapOfIntegerStringMonthFormat.put(4,'April');
        mapOfIntegerStringMonthFormat.put(5,'May');
        mapOfIntegerStringMonthFormat.put(6,'June');
        mapOfIntegerStringMonthFormat.put(7,'July');
        mapOfIntegerStringMonthFormat.put(8,'August');
        mapOfIntegerStringMonthFormat.put(9,'September');
        mapOfIntegerStringMonthFormat.put(10,'October');
        mapOfIntegerStringMonthFormat.put(11,'November');
        mapOfIntegerStringMonthFormat.put(12,'December');        
        
        return mapOfIntegerStringMonthFormat;
    }
    
}