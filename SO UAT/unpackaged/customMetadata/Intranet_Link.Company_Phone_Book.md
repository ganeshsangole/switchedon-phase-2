<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Company Phone Book</label>
    <protected>false</protected>
    <values>
        <field>Link_Type__c</field>
        <value xsi:type="xsd:string">Key Links</value>
    </values>
    <values>
        <field>Link_URL__c</field>
        <value xsi:type="xsd:string">https://teams.microsoft.com/dl/launcher/launcher.html?url=%2F_%23%2Fl%2Ffile%2F5333E1E5-113D-4427-8280-41CB49EC69AF%3FtenantId%3D598a9421-6974-4650-b8b5-1e5bc91fa345%26fileType%3Dxlsx%26objectUrl%3Dhttps%253A%252F%252Fswitchedongroupltd.sharepoint.com%252</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
