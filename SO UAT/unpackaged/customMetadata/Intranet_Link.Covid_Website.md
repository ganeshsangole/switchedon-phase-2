<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Covid Website</label>
    <protected>false</protected>
    <values>
        <field>Link_Type__c</field>
        <value xsi:type="xsd:string">Key Links</value>
    </values>
    <values>
        <field>Link_URL__c</field>
        <value xsi:type="xsd:string">https://www.switchedongroup.co.nz/covid-19-updates/</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
