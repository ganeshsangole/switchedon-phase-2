<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Web Word</label>
    <protected>false</protected>
    <values>
        <field>Link_Type__c</field>
        <value xsi:type="xsd:string">Quick Links</value>
    </values>
    <values>
        <field>Link_URL__c</field>
        <value xsi:type="xsd:string">https://www.office.com/launch/word?flight=unauthrefresh&amp;auth=2</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
