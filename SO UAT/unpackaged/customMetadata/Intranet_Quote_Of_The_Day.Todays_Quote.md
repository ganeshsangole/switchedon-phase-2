<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Todays Quote</label>
    <protected>false</protected>
    <values>
        <field>Author__c</field>
        <value xsi:type="xsd:string">Paul Brown</value>
    </values>
    <values>
        <field>Quote__c</field>
        <value xsi:type="xsd:string">When you win, say nothing. When you lose, say less.</value>
    </values>
</CustomMetadata>
