<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Note3</label>
    <protected>false</protected>
    <values>
        <field>Notification__c</field>
        <value xsi:type="xsd:string">LATEST COVID ALERT UPDATE: New Zealand is at Alert Level 1 &lt;a href=&quot;https://www.google.com&quot; target=&quot;_blank&quot;&gt;Click here for more details!&lt;/a&gt;</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
</CustomMetadata>
