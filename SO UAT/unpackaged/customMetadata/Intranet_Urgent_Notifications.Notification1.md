<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Notification1</label>
    <protected>false</protected>
    <values>
        <field>Notification__c</field>
        <value xsi:type="xsd:string">Please note that all company offices will be closed December 24 to 27. We will reopen Monday, December 29, and close again for December 31 and January 1.We wish you all the warmest of holiday cheer</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
