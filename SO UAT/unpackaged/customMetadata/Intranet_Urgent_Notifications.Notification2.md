<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Notification2</label>
    <protected>false</protected>
    <values>
        <field>Notification__c</field>
        <value xsi:type="xsd:string">We are pleased to announce the completion of our new office building at 1600 Main Street in Springfield. We look forward to serving you at our new location. Our telephone number will remain the same: 555-5555.</value>
    </values>
    <values>
        <field>Sort_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
