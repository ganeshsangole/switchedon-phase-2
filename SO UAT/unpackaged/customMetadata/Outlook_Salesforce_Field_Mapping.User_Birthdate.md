<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>User Birthdate</label>
    <protected>false</protected>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">Date</value>
    </values>
    <values>
        <field>Outlook_Contact_Field_Name__c</field>
        <value xsi:type="xsd:string">extensionAttribute2</value>
    </values>
    <values>
        <field>Salesforce_Contact_Field_API_Name__c</field>
        <value xsi:type="xsd:string">Birthdate</value>
    </values>
</CustomMetadata>
