<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>User Office Location</label>
    <protected>false</protected>
    <values>
        <field>Contact_Field__c</field>
        <value xsi:type="xsd:string">AD_Location__c</value>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">String</value>
    </values>
    <values>
        <field>User_Field__c</field>
        <value xsi:type="xsd:string">Office_Location__c</value>
    </values>
</CustomMetadata>
