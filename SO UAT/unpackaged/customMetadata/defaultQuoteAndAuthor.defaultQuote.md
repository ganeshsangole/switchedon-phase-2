<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>defaultQuote</label>
    <protected>false</protected>
    <values>
        <field>author__c</field>
        <value xsi:type="xsd:string">Zaha Hadid</value>
    </values>
    <values>
        <field>quote__c</field>
        <value xsi:type="xsd:string">Your success will not be determined by your gender or your ethnicity, but only on the scope of your dreams and your hard work to achieve them.</value>
    </values>
</CustomMetadata>
