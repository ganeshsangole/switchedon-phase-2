import { LightningElement, wire, track, api} from 'lwc';
import getReports from '@salesforce/apex/AddMembersController.availableReports';
import getReportsAndFolder from '@salesforce/apex/AddMembersController.reportsAndFolder';
import createContact from '@salesforce/apex/AddMembersController.createMembers';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class AddMembersComponent extends LightningElement {
    reportEntries = [];
    folderEntries = [];
    reportId;
    @api recordId;
    serVerData = {};
    isLoading = true;

    @wire(getReportsAndFolder)
    reportsWithFolderName({error, data}) {
        if(data) {
            if(Object.keys(data).length == 0) {
                this.closeQuickAction();
                this.notifyUser('No reports are available', 'error');
            }else {
                this.serVerData = data;
                Object.keys(data).forEach(key => {
                    let folderEntry = {
                        "label" : key,
                        "value" : key
                    };
                    this.folderEntries.push(folderEntry);
                    console.log(this.folderEntries);
                });
                this.reportEntries = this.serVerData[this.folderEntries[0].label];
                this.reportId = this.reportEntries.length > 0 ? this.reportEntries[0].elementValue : '';
                this.isLoading = false;
            }
        }else if(error) {
            console.log(err);   
        }
    }

    folderChange(event) {
        this.reportEntries = this.serVerData[event.target.value];
        this.reportId = this.reportEntries.length > 0 ? this.reportEntries[0].elementValue : '';
    }

    handleChange(event) {
        this.reportId = event.target.value;
    }

    handleClick(event) {
        this.isLoading = true;
        createContact({'reportId':this.reportId, 'campaignId':this.recordId})
        .then(result => {
            console.log('campaign members created....');
            this.notifyUser(result, 'success');
            this.closeQuickAction();
        }).catch(err => {
            console.error('Error while creating campaign members... : ');
            if(err != null && err != undefined && err.hasOwnProperty('body') && err.body.hasOwnProperty('message')) {
                this.notifyUser(err.body.message, 'error');
                this.closeQuickAction();
            }
        })
    }

    notifyUser(message, variant) {
        const evt = new ShowToastEvent({
            title: '',
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    closeQuickAction() {
        const closeQA = new CustomEvent('close');
        this.dispatchEvent(closeQA);
    }

    isValid(value) {
        if(value != null && value != '' && typeof value != 'undefined') {
            return true;
        }

        return false;
    }
}