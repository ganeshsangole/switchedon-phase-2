import { LightningElement, api } from 'lwc';
import FilesUploadLabel from "@salesforce/label/c.Files_Upload_Label";
import AcceptedFileFormats from "@salesforce/label/c.Accepted_File_Formats";
import updateContentVersionIdOnKnowledge from '@salesforce/apex/FileUpload.updateContentVersionIdOnKnowledge';
import fileUploadSuccessMessage from "@salesforce/label/c.File_Upload_Success_Message";

export default class FileUpload extends LightningElement {
    @api recordId;
    @api FilesUploadLabel = FilesUploadLabel;
    @api ContentDocumentIds = [];
    @api acceptedFileFormatsLabel = AcceptedFileFormats;
    @api acceptedFileFormatsList = [];
    @api fileUploadCount = 0;
    @api fileUploadSuccessMessage = fileUploadSuccessMessage;

    connectedCallback() {
        if (this.acceptedFileFormatsLabel != null) {
            this.acceptedFileFormatsList = this.acceptedFileFormatsLabel.split(',');
        }
    }

    handleUploadFinished(event) {
        this.ContentDocumentIds.push((event.detail.files)[0].documentId);
        this.fileUploadCount = event.detail.files.length;
        updateContentVersionIdOnKnowledge({ listContentDocumentIds: this.ContentDocumentIds, recordId: this.recordId })
            .then(result => {})
            .catch(error => {
                this.error = error;
            });



    }
}