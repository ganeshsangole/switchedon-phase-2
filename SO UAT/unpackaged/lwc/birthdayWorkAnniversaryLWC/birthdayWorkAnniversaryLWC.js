import { LightningElement, api, track, wire } from 'lwc';
import getDetails from "@salesforce/apex/getBirthdayAnniversaryDetails.getDetails";
import NoAnniversariesThisWeek from "@salesforce/label/c.No_Anniversaries_This_Week";
import NoAnniversariesToday from "@salesforce/label/c.No_Anniversaries_Today";
import NoBirthdaysThisWeek from "@salesforce/label/c.No_Birthdays_This_Week";
import NoBirthdaysTodayLabel from "@salesforce/label/c.No_Birthdays_Today_Label";
import DefaultContactImage from '@salesforce/resourceUrl/DefaultContactImage';

export default class BirthdayWorkAnniversaryLWC extends LightningElement {
    @api details;
    @api isbDayOrWorkSection;
    @api timeDuration;
    @api noRecordsDisplayMsg;
    @api loaded = false;

    connectedCallback() {
        getDetails({ IsbDayOrWorkSection: this.isbDayOrWorkSection, timeDuration: this.timeDuration })
            .then(result => {
                result = JSON.parse(result);
                //  this.template.querySelector('.celebrationContainer').style.display = 'block';
                if (result == null || result.length == 0) {
                    this.details = null;

                } else {
                    this.details = result;

                    if (result.length > 2) {
                        this.template.querySelector('.celebrationwrapper').style.overflowY = 'scroll';
                    }

                    for (var i = 0; i < result.length; i++) {
                        if (result[i].profilePhoto == null) {
                            result[i].profilePhoto = DefaultContactImage;
                        } else {
                            result[i].profilePhoto = 'data:image/jpeg;base64,' + result[i].profilePhoto;
                        }
                    }
                }
                this.loaded = true;
            })
            .catch(error => {
                this.template.querySelector('.celebrationContainer').style.display = 'block';
                this.details = null;
                this.loaded = true;
            });

        if (this.timeDuration == 'today' && this.isbDayOrWorkSection == 'Birthday') {
            this.noRecordsDisplayMsg = NoBirthdaysTodayLabel;
        } else if (this.timeDuration == 'week' && this.isbDayOrWorkSection == 'Birthday') {
            this.noRecordsDisplayMsg = NoBirthdaysThisWeek;
        } else if (this.timeDuration == 'today' && this.isbDayOrWorkSection == 'Work Anniversary') {
            this.noRecordsDisplayMsg = NoAnniversariesToday;
        } else if (this.timeDuration == 'week' && this.isbDayOrWorkSection == 'Work Anniversary') {
            this.noRecordsDisplayMsg = NoAnniversariesThisWeek;
        }
    }
}