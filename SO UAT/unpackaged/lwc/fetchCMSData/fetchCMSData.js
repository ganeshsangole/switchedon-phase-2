/* 
    Author :      Tranzevo
    Description : Contoller for News List component.
    
*/

import { LightningElement, wire, track, api } from 'lwc';
import Contents from '@salesforce/apex/FetchCMSdata.relatedContents';

import addEntryToAuditLog from '@salesforce/apex/FetchCMSdata.addEntryToAuditLog';
import SalesforceBaseURL from '@salesforce/label/c.Salesforce_Base_URL';
import VIEWCOUNT_LOGO from '@salesforce/resourceUrl/ViewCount';


export default class InvencoNewsList extends LightningElement {
    @api contentType = 'Communications';
    @api recordsPerPage;
    @track contentList = [];
    contentCollection = [];
    monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    pagenumber = 0;
    localPageNumber = 1;
    //localPageSize = 3;
    @track paginationConfs = [];
    totalPageSize = 0;
    lowerIndex;
    upperIndex;
    visiblePageNumUpto = 5;
    isNextPageAvailable = false;
    refetched = false;
    isLoaded = false;
    showNoPostBlock = false;
    @api AuditLogsData = new Set();
    ViewCountLogo = VIEWCOUNT_LOGO;




    formatDate(dateVal) {
        var newDate = new Date(dateVal);

        var sMonth = this.padValue(newDate.getMonth() + 1);
        var sDay = this.padValue(newDate.getDate());
        var sYear = newDate.getFullYear();
        var sHour = newDate.getHours();
        var sMinute = this.padValue(newDate.getMinutes());
        var sAMPM = "am";



        var iHourCheck = parseInt(sHour);



        if (iHourCheck > 12) {
            sAMPM = "pm";
            sHour = iHourCheck - 12;
        } else if (iHourCheck === 0) {
            sHour = "12";
        }



        sHour = this.padValue(sHour);



        return sHour + ":" + sMinute + sAMPM + ", " + sDay + "." + sMonth + "." + sYear;
    }

    padValue(value) {
        return (value < 10) ? "0" + value : value;
    }


    handleClick(event) {
        console.log('event.target.className' + event.target.getAttribute('data-id'));
        console.log('event.target.className' + event.target.value);
        console.log('event.target.className' + event.target.id.split('-').shift());
        addEntryToAuditLog({ CMSContentId: event.target.getAttribute('data-id'), ContentType: event.target.id.split('-').shift(), ContentName: event.target.value })
            .then(result => {

            })
            .catch(error => {
                this.error = error;
            });
    }


    @api
    get localPageSize() {
        if (this.recordsPerPage > 0) {
            return this.recordsPerPage;
        }

        return 5;
    }

    @api
    get showPrevious() {
        if (this.localPageNumber > 1) {
            return true;
        }

        return false;
    }

    @api
    get showNext() {
        if (this.localPageNumber < this.paginationConfs.length || this.isNextPageAvailable) {
            return true;
        }

        return false;
    }

    @api
    get showPagination() {
        if (this.paginationConfs.length > 1) {
            return true;
        }

        return false;
    }

    constructor() {
        super();
        /* if(this.recordsPerPage > 0) {
            this.localPageSize = this.recordsPerPage;
        } */
    }

    @wire(Contents, { 'contentType': '$contentType' })
    fetchRelatedContents({ error, data }) {
        console.log('inside wire function*****');
        if (data) {

            console.log('content data fetched success....');
            this.initVariables(data);
        } else if (error) {
            console.log('content data fetched failure');
            console.error(error);
        }
    }

    htmlDecode(input) {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
    }

    initVariables(data) {
        if (data != undefined && data != '' && data != null) {
            if (data.hasOwnProperty('nextPageUrl')) {
                this.isNextPageAvailable = true;
            } else {
                this.isNextPageAvailable = false;
            }

            this.totalPageSize += data.total;

            for (let index = 0; index < data.items.length; index++) {
                let content = data.items[index];
                let contentObj = {
                    'publishedDate': content.publishedDate,
                    'managedContentId': content.managedContentId,
                    'title': '',
                    'subtitle': '',
                    'contentUrlName': '',
                    'type': '',
                    'isViewed': (new Set(data.listOfContentIds)).has(content.managedContentId),
                    'ViewCount': data.contentIdsAndViewCount[content.managedContentId] == null ? 0 : data.contentIdsAndViewCount[content.managedContentId],
                    'ContentImage': null,
                    'ContentType': content.type
                }

                if (this.isValid(content)) {
                    console.log('content.contentNodes' + content.contentNodes);
                    if (content.contentNodes.hasOwnProperty('published_Date') && this.isValid(content.contentNodes.published_Date)) {

                        // this.formatDate();
                        console.log('content.contentNodes.published_Date.value');
                        contentObj.publishedDate = content.contentNodes.published_Date.value;
                    }

                    if (content.hasOwnProperty('contentNodes') && content.contentNodes.hasOwnProperty('Excerpt')) {
                        console.log('excerpt');
                        contentObj.subtitle = this.htmlDecode(content.contentNodes.Excerpt.value);
                    }
                    if (content.hasOwnProperty('contentNodes') && content.contentNodes.hasOwnProperty('Content_image')) {

                        contentObj.ContentImage = SalesforceBaseURL + content.contentNodes.Content_image.url;
                        //contentObj.ContentImage = content.contentNodes.Content_image.url;
                        console.log('contentObj.ContentImage' + contentObj.ContentImage);
                    }

                    if (content.hasOwnProperty('title')) {
                        contentObj.title = this.htmlDecode(content.title);
                    }

                    if (content.hasOwnProperty('contentUrlName')) {
                        contentObj.contentUrlName = content.contentUrlName;
                    }

                    if (content.hasOwnProperty('type')) {
                        contentObj.type = content.type;
                    }

                    this.contentCollection.push(contentObj);
                }

            }

            this.sortContentCollection();

            if (!this.refetched) {
                this.initPaginationObj();
            } else {
                this.updatePaginationArray();
            }

            this.prepareContentList();

        }
    }

    sortContentCollection() {
        for (let i = 1; i < this.contentCollection.length; i++) {
            for (let j = 0; j < this.contentCollection.length - 1; j++)
                if (new Date(this.contentCollection[j].publishedDate) < new Date(this.contentCollection[j + 1].publishedDate)) {
                    let tempObj = this.contentCollection[j];
                    this.contentCollection[j] = this.contentCollection[j + 1];
                    this.contentCollection[j + 1] = tempObj;
                }
        }
    }

    isValid(value) {
        if (value != null && value != '' && value != undefined) {
            return true;
        }

        return false;
    }

    updatePaginationArray() {
        let numberOfPages = this.totalPageSize / this.localPageSize;
        let index = this.paginationConfs.length;
        for (let i = index; i < numberOfPages; i++) {
            let shouldShow = false;
            let pageConfObj = {
                'Id': i,
                'pagenumber': i + 1,
                'shouldShow': shouldShow,
                'cssclass': ""
            }

            this.paginationConfs.push(pageConfObj);
        }
    }

    initContentList() {
        return new Promise((resolve, reject) => {
            this.contentList = [];
            let contents = this.contentCollection;
            let startIndex = (this.localPageNumber - 1) * this.localPageSize;
            console.log('inside init contentList');
            if (startIndex >= contents.length) {
                console.log('Data needs to be refetched.....');
                this.isLoaded = false;
                this.refetched = true;
                this.pagenumber += 1;
                resolve();
            } else {
                let urlParts = window.location.href.split('/s/');
                let baseURl = '';
                if (urlParts.length > 0) {
                    baseURl = urlParts[0] + '/s/';
                }

                for (let i = startIndex; i < contents.length; i++) {
                    let data = contents[i];
                    let contentObj = {
                        'Id': data.managedContentId,
                        'publishedDate': this.formatDate(data.publishedDate),
                        'title': data.title,
                        'subtitle': data.subtitle,
                        //  'url': baseURl + 'communications' + '/' + data.contentUrlName + '-' + data.managedContentId,
                        'url': baseURl + this.contentType.toLowerCase().replaceAll(' ', '-') + '/' + data.contentUrlName + '-' + data.managedContentId,

                        'isViewed': data.isViewed,
                        'managedContentId': data.managedContentId,
                        'ViewCount': data.ViewCount,
                        'ContentImage': data.ContentImage,
                        'ContentType': data.ContentType,

                    }

                    this.contentList.push(contentObj);

                    if (this.contentList.length == this.localPageSize) {
                        break;
                    }
                }

                resolve();
            }
        })

    }

    initPaginationObj() {
        console.log('this.localPageSize' + this.localPageSize);
        let numberOfPages = this.totalPageSize / this.localPageSize;
        let activeUpperIndex = -1;

        for (let i = 0; i < numberOfPages; i++) {
            let shouldShow = false;
            if (i < this.visiblePageNumUpto) {
                shouldShow = true;
                activeUpperIndex = i;
            }
            let pageConfObj = {
                'Id': i,
                'pagenumber': i + 1,
                'shouldShow': shouldShow,
                'cssclass': ""
            }

            this.paginationConfs.push(pageConfObj);
        }

        if (this.paginationConfs.length > 0) {
            this.paginationConfs[this.localPageNumber - 1].cssclass = "selected";
            this.lowerIndex = this.localPageNumber - 1;
            this.upperIndex = activeUpperIndex;
        }
    }

    navigateToPage(event) {
        let datasetid = event.target.dataset.id;
        let pageNumber;
        console.log('navigate to page clicked....');
        console.log(datasetid);
        if (datasetid === "next") {
            pageNumber = this.localPageNumber + 1;
        } else if (datasetid === "prev") {
            pageNumber = this.localPageNumber - 1;
        } else {
            pageNumber = parseInt(datasetid);
        }

        this.handlePageNavigation(pageNumber);
    }

    handlePageNavigation(pageNumber) {
        if (pageNumber < 1 || this.localPageNumber == pageNumber) {
            return;
        }

        this.paginationConfs[this.localPageNumber - 1].cssclass = "";
        this.localPageNumber = pageNumber;
        this.prepareContentList();
    }

    prepareContentList() {
        this.initContentList()
            .then(result => {
                if (this.contentList.length == 0) {
                    this.isLoaded = true;
                    this.showNoPostBlock = true;
                } else if (this.paginationConfs.length >= this.localPageNumber) {
                    this.isLoaded = true;
                    this.paginationConfs[this.localPageNumber - 1].cssclass = "selected";
                    this.updatePaginationEntry(this.localPageNumber);
                }
            })
            .catch(error => {
                console.log('Error Occured...');
                console.log(error);
            });
    }

    updatePaginationEntry(pageNumber) {
        let lowerLimit = pageNumber - 2;
        let upperLimit = pageNumber + 2;
        for (let i = 0; i < this.paginationConfs.length; i++) {
            if (pageNumber <= 3) {
                if (i < 5) {
                    this.paginationConfs[i].shouldShow = true;
                } else {
                    this.paginationConfs[i].shouldShow = false;
                }
            } else if (pageNumber > 3 && upperLimit <= this.paginationConfs.length) {
                if (i < lowerLimit - 1) {
                    this.paginationConfs[i].shouldShow = false;
                } else if (i >= lowerLimit - 1) {
                    if (i < upperLimit) {
                        this.paginationConfs[i].shouldShow = true;
                    } else {
                        this.paginationConfs[i].shouldShow = false;
                    }
                }
            } else if (pageNumber > 3 && upperLimit > this.paginationConfs.length) {
                upperLimit = this.paginationConfs.length;
                lowerLimit = upperLimit - 5;
                if (i < lowerLimit - 1) {
                    this.paginationConfs[i].shouldShow = false;
                } else if (i >= lowerLimit - 1) {
                    if (i < upperLimit) {
                        this.paginationConfs[i].shouldShow = true;
                    } else {
                        this.paginationConfs[i].shouldShow = false;
                    }
                }
            }
        }
    }

    /*  formatDate(datetime) {
          let dateTimeObj = new Date(datetime);
          let month = dateTimeObj.getMonth();
          let day = dateTimeObj.getDate();
          let year = dateTimeObj.getFullYear();
          let monthName = this.monthArray[month];
  
          return monthName + ' ' + day + ', ' + year;
      }*/
}