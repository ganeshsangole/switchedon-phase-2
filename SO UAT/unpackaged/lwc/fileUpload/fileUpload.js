import { LightningElement, api, wire} from 'lwc';
import userId from '@salesforce/apex/FileManager.userId';

export default class FileUpload extends LightningElement {
    
    @wire (userId) recordId;
    filename;

    @api
    get acceptedformat() {
        return this._acceptedformat;
    }

    set acceptedformat(value) {
        this._acceptedformat = value;
    }
        
    @api
    get componentlabel() {
        return this._componentlabel;
    }

    set componentlabel(label) {
        this._componentlabel = label;
    }

    constructor() {
        super();
    }

    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        console.log('uploadedFiles in child component****',uploadedFiles);
        let uploadCompleteEvt = new CustomEvent('uploadcomplete', {detail: uploadedFiles});
        this.dispatchEvent(uploadCompleteEvt);
    }
}