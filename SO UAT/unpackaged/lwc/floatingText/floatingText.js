import { LightningElement, api } from 'lwc';
import getAllNotifications from '@salesforce/apex/floatingText.getAllNotifications';

export default class FloatingText extends LightningElement {
    @api intervalId = window.innerWidth;
    @api floatElement;
    @api textShiftInterval = 20;
    @api font = '14px verdana';
    @api textColor = '#ff0000';
    @api hyperLinkColor = "rgb(20, 69, 101)";

    @api backgroundColor = '#ffffff';
    @api notification;
    @api notificationsList = [];
    @api index = 0;
    @api paragraphlength;

    connectedCallback() {
        getAllNotifications()
            .then(result => {
                result = JSON.parse(result);
                if (result == null || result.length == 0) {
                    var informparent = new CustomEvent("nodatapresent", {});
                    this.dispatchEvent(informparent);

                } else {
                    this.notificationsList = result;
                    this.notification = this.notificationsList[0].notification;
                    this.template.querySelector('.tofloat').innerHTML = this.notification;
                    var linkTagList = this.template.querySelectorAll('a');
                    if (linkTagList != null && linkTagList.length > 0) {
                        for (var i = 0; i < linkTagList.length; i++) {
                            linkTagList[i].style.color = this.hyperLinkColor;
                        }
                    }
                    this.floatingTextFunction();
                }

            })
            .catch(error => {
                this.error = error;
            });

    }


    floatingTextFunction() {
        this.floatElement = this.template.querySelector('.tofloat');
        this.template.querySelector('.classDiv').style.backgroundColor = this.backgroundColor;
        this.floatElement.style.color = this.textColor;
        this.floatElement.style.font = this.font;

        this.getParagraphLength(this.notification);

        this.floatElement.style.left = this.intervalId + 'px';

        if (this.notificationsList != null && this.notificationsList.length > 0) {

            window.setInterval(() => {

                if (this.intervalId > (-this.paragraphlength)) {

                    this.intervalId = this.intervalId - 1;
                    this.floatElement.style.left = this.intervalId + 'px';

                } else {

                    if (this.index + 1 > (this.notificationsList.length - 1)) {
                        this.index = 0;
                    } else {
                        this.index++;
                    }
                    this.notification = this.notificationsList[this.index].notification;
                    this.template.querySelector('.tofloat').innerHTML = this.notification;
                    var linkTagList = this.template.querySelectorAll('a');

                    if (linkTagList != null && linkTagList.length > 0) {
                        for (var i = 0; i < linkTagList.length; i++) {
                            linkTagList[i].style.color = this.hyperLinkColor;
                        }
                    }
                    this.getParagraphLength(this.notification);
                    this.intervalId = window.innerWidth;
                    this.floatElement.style.left = this.intervalId + "px";
                }
            }, this.textShiftInterval);

        }

    }

    getParagraphLength(notification) {
        var newString = notification.replace(/(<([^>]+)>)/ig, '');
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");
        ctx.font = this.font;
        this.paragraphlength = ctx.measureText(newString).width;
    }
}