import { LightningElement, track, api } from 'lwc';
import getLinks from '@salesforce/apex/Links.getLinks';
import quickLinksIcon from '@salesforce/resourceUrl/quickLinksIcon';
import keyLinksIcon from '@salesforce/resourceUrl/keyLinksIcon';

export default class KeyLinks extends LightningElement {
    @api LinksData;
    @api error;
    @api LinksHeading;
    @api linkType;
    @api LinksIcon;
    connectedCallback() {
        getLinks({ linkType: this.linkType })
            .then(result => {
                this.LinksHeading = this.linkType;
                this.LinksData = JSON.parse(result);
            })
            .catch(error => {
                this.error = error;
            });

        if (this.linkType == "Key Links") {
            this.LinksIcon = keyLinksIcon;
        } else if (this.linkType == "Quick Links") {
            this.LinksIcon = quickLinksIcon;
        }
    }


}