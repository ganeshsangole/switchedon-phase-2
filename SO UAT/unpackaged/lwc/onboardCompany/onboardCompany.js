import { LightningElement, api, wire,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import SO_logo from '@salesforce/resourceUrl/SwitchedOnLogo';
import createRecord from '@salesforce/apex/CompanyOnboardingRecordCreation.createRecord';
import getLeadDetails from '@salesforce/apex/CompanyOnboardingRecordCreation.getLeadDetails';
import AllFilesRequiredErrorMessageLabel from '@salesforce/label/c.AllFilesRequiredErrorMessage';
import CompanyOnboardingErrorMessageLabel from '@salesforce/label/c.CompanyOnboardingErrorMessage';
import KeyContactErrorMessageLabel from '@salesforce/label/c.KeyContactErrorMessage';
import pickListValueDynamically  from '@salesforce/apex/CompanyOnboardingRecordCreation.pickListValueDynamically';

export default class SOCompanyInductionForm extends LightningElement {
    
    @wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case_Contact__c'},
    selectPicklistApi: 'Contact_Type__c'}) selectContactTypeTargetValues;

    @wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case_Contact__c'},
    selectPicklistApi: 'Salutation__c'}) selectSalutationTargetValues;
    
    SO_logourl = SO_logo;

    @api returnURL;
    @track statuspicklistVal;
    @track originpicklistVal;
    @track contacttypepicklistVal;
    @track salutationpicklistVal;
    @track parameter ;
     leadName;
     leadCompany;
     leadRecordId;
     docId;
     subcontractorinformationfilename;
     socontractoragreementfilename;
     additionaltermsfilename;
     PublicLiabilityCertificatefilename;
     VehicleInsurancefilename;
     HandSfilename;
     atleastonevalue = -1;
     Infoandapprovalfilename;
     KOpledgefilename;
     FinancialReviewfilename;
     CodeOfConductfilename;
     SOContractoagreement =false;
     AdditionalTerms =false;
     HSQualityterms =false;
     Infoandapprovalform =false;
     KOPledge =false;
     emailErrorMessage;
     setdisable;
     contentId;
     firstfileuploadClass= ".file-container1";
     firstfileuploadId;
     secondfileuploadClass;
     secondfileuploadId;
     listFileDetails = [];
     
     listOfFiles={
        "FileNames" : "",
        "Fileids" : "",
    }
     formSubmitted = false;
    @api supportedFiles;
    supportedFiles = ['.doc', '.docx' ,'.pdf'];
    sameAddress;
    showNextFields = false;
    dontshowNextFields =true;

getQueryParameters() {

    var params = {};
    var search = location.search.substring(1);

    if (search) {
        params = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value)
        });
    }

    return params;
}
@api isLoaded = false;
listOFIndexes =[];
filteredArraydata = [];
casecontactArray=[];
 leadDetails =new Map()
casecontact={
    "sObjectType" : "Case_Contact__c",
    "Contact_Type__c" : "",
    "Salutation__c" : "",
    "First_Name__c" : "",
    "Last_Name__c"  : "",
    "Phone__c" : "",
    "Email__c" : "",
}

connectedCallback() {
    this.parameter = this.getQueryParameters().id;
    
    for(var i=0; i<5; i++) {
        let tempObj = Object.assign({}, this.caseContact);
        this.casecontactArray[i] = tempObj;
    }
    console.log('casecontactarray oth element data',this.casecontactArray[0]);
   

    this.getLeadDetails(this.parameter);
}


getLeadDetails(val)
{
    console.log('This Parameter',val);
    console.log('this.leadetails',this.leadDetails);
    getLeadDetails({'leadId': val}).then(result => {
      
            console.log('result',result);
            
                console.log('result[[[',result['Company']);
           
           
        this.recordObj['SuppliedCompany'] = result['Company'];
        this.recordObj['Postal_Address__c'] = result['PostalAddress'];
        this.recordObj['Postal_Address_City__c'] = result['City'];
        this.recordObj['Postal_Address_Postal_Code__c'] = result['PostalCode'];
        this.casecontactArray[0]['Contact_Type__c'] = 'Primary';
        this.casecontactArray[0]['Salutation__c'] = result['Salutation'];
        this.casecontactArray[0]['First_Name__c'] = result['FirstName'];
        this.casecontactArray[0]['Last_Name__c'] = result['LastName'];
        this.casecontactArray[0]['Phone__c'] = result['Phone'];
        this.casecontactArray[0]['Email__c'] = result['Email']; 
        
        }).catch(error=> {
            console.log('error occured');
        })
        console.log('this.leadetails',this.leadDetails);

}
recordObj = {
    "sObjectType" : "Case",
    "SuppliedCompany" : "",
    "NZBN_Number__c" : "",
    "GST_Number__c":"",
    "Postal_Address__c" : "",
    "Postal_Address_Suburb__c" : "",
    "Postal_Address_City__c" : "",
    "Physical_Address_Suburb__c" : "",
    "Physical_Address_City__c" : "",
    "Physical_Address__c":"",
    "Postal_Address_Postal_Code__c" : "",
    "Physical_Address_Postal__c" : "",
    "Bank_Account_Name__c" : "",
    "Bank_Account_Number__c":"",
    "Public_Liability_Insurer__c" : "",
    "Public_Liability_Policy_No__c":"",
    "Public_Liability_Expiry__c" : null,
    "Public_Liability_Value_Insured__c":"",
    "Vehicle_Insurance_Insurer__c" : "",
    "Vehicle_Insurance_Policy_No__c":"",
    "Vehicle_Insurance_Expiry__c":null,
    "Lead__c" : this.parameter,
};
beforeUnloadHandler() {
    if(this.isValidValue(this.docId) && !this.formSubmitted) {
        this.markDocumentToDelete(this.docId);
        this.docId = '';
    }
}

disconnectedCallback() {
    if(this.isValidValue(this.docId) && !this.formSubmitted) {
        this.markDocumentToDelete(this.docId);
    }
} 

validateFile(valId,valClass) {
    let filePresent = true;
    console.log('listFileDetails',JSON.stringify(listFileDetails));
    if(!this.isValidValue(valId)) {
        let uploadFileDiv = this.template.querySelector(valClass);
        console.log('uploadFileDiv++++++++++',uploadFileDiv);
        uploadFileDiv.classList.add("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("hide-error");
        errorDiv.classList.add("show-error");
        filePresent = false;
    }

    return filePresent;
}

renderedCallback() {

    this.template.querySelectorAll('select').forEach(element => {
        let fieldName = element.getAttribute("data-id");
        let indexval= element.getAttribute("data-id1");
        let tempObj = Object.assign({}, this.caseContact);
        element.value = this.casecontactArray[indexval][fieldName];
    }); 
   
}
    

 handleCaseChange(event){
    let fieldName = event.target.getAttribute("data-id");
   
    let fieldValue = event.target.value;

    this.recordObj[fieldName] = fieldValue;
   
    let checkboxFieldValue = event.target.checked;
        if(fieldName==='Sameaddress' && checkboxFieldValue===true)
    {
        this.getLeadDetails(this.parameter);
        console.log('checkbox__',checkboxFieldValue);
        this.recordObj['Physical_Address__c'] = this.recordObj['Postal_Address__c'];
        this.recordObj['Physical_Address_Suburb__c'] =this.recordObj['Postal_Address_Suburb__c'];
        this.recordObj['Physical_Address_City__c'] = this.recordObj['Postal_Address_City__c'];
        this.recordObj['Physical_Address_Postal__c']= this.recordObj['Postal_Address_Postal_Code__c'];
       
       this.template.querySelectorAll('.data-id2').forEach(element => {
        let fieldName = element.getAttribute("data-id2");
        element.querySelector
        console.log('fieldName',fieldName);
        if(fieldName!='First_Name__c' || fieldName!= 'Last_Name__c' || fieldName!='Phone__c' || fieldName!= 'Email__c'){
            element.value = this.recordObj[fieldName];  
        }
        console.log('record obj in first loop',JSON.stringify(this.recordObj));
    }); 
    this.setdisable=true;
    }
    else if(fieldName==='Sameaddress' && checkboxFieldValue===false)
    {
        this.getLeadDetails(this.parameter);
        console.log('checkbox__',checkboxFieldValue);
        this.recordObj['Physical_Address__c'] = "";//this.recordObj['Postal_Address__c'];
        this.recordObj['Physical_Address_Suburb__c'] ="";//this.recordObj['Postal_Address_Suburb__c'];
        this.recordObj['Physical_Address_City__c'] = "";//this.recordObj['Postal_Address_City__c'];
        this.recordObj['Physical_Address_Postal__c']= "";//this.recordObj['Postal_Address_Postal_Code__c'];
       
       this.template.querySelectorAll('.data-id2').forEach(element => {
        let fieldName = element.getAttribute("data-id2");
        element.querySelector
        console.log('fieldName',fieldName);
        if(fieldName!='First_Name__c' || fieldName!= 'Last_Name__c' || fieldName!='Phone__c' || fieldName!= 'Email__c'){
            element.value = this.recordObj[fieldName];  
        }
        console.log('record obj in second loop',JSON.stringify(this.recordObj));

    }); 
    this.setdisable=true;
    }
    this.setdisable=false;

} 

handleChange(event) {
    var value;

    console.log('Hadle change method called...');
    let fieldName = event.target.getAttribute("data-id");
    let indexval= event.target.getAttribute("data-id1");
    let fieldValue = event.target.value;
    let valueExist;
    this.casecontactArray[indexval][fieldName] = fieldValue;
    if(this.listOFIndexes.indexOf(indexval) !== -1){
        valueExist= 1;
    }
    if(valueExist !== 1)
    {
        this.listOFIndexes.push(indexval);
    }
}



isValidValue(value) {
    if(value != null && value != '' && typeof value != 'undefined') {
        return true;
    }

    return false;
}

handlePrevious(event) {
    

    this.showNextFields = false;
    this.dontshowNextFields = true;
}

handleNext(event) {
    if((!this.isValidValue(this.recordObj['SuppliedCompany'])) || 
    (!this.isValidValue(this.recordObj['NZBN_Number__c'])) || 
    (!this.isValidValue(this.recordObj['GST_Number__c']))) {
        this.showCaseErrorToast();
    }else {
        for(let i=0;i< this.casecontactArray.length;i++) {
            console.log('First inside for loop');
            let filteredArrayObj = Object.assign({}, this.casecontact);
            console.log('atleastonevalue for casecontact array',this.atleastonevalue);
            filteredArrayObj = this.casecontactArray[i];
            console.log('filteredArrayObj',filteredArrayObj)
            if(this.atleastOneInputThere(filteredArrayObj)){
                this.atleastonevalue = i;
                console.log('atleastonevalue',this.atleastonevalue);
            }
        }

        if(this.atleastonevalue === -1) {
            console.log('Inside if of atleast');
            this.showErrorToast();   
        }else {
            this.showNextFields = true;
            this.dontshowNextFields = false;
        }
    }
}       
    showCaseErrorToast() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: CompanyOnboardingErrorMessageLabel,//'Please enter Company Name, NZBN Number, GST Number ',
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
        this.atleastonevalue = -1;
        console.log('Inside error of case',this.atleastonevalue)
    }
    
      showErrorToast() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: KeyContactErrorMessageLabel,//'Please enter details of atleast one Key Contact ',
            variant: 'error',
            mode: 'dismissable'
        });
        this.atleastonevalue = -1;
        console.log('Inside error of key contacts',this.atleastonevalue)
        this.dispatchEvent(evt);
    }

    showFileErrorToast(message) {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: message,
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Status'}) selectStatusTargetValues; 

@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Origin'}) selectOriginTargetValues;

    uploadCompleteHandlerSOAgreement(event) {
                
        this.docId = event.detail[0].documentId;
        this.socontractoragreementfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Switched On Contractor Agreement(IMS 1010)';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.SOContractoagreement=true;
        console.log('FileIds object array',JSON.stringify(this.listFileDetails));
        if(event.detail[0].documentId === ''){
            this.firstfileuploadId = event.detail[0].documentId;
        }
    }

    uploadCompleteHandlerAdditionalTerms(event) {

        this.docId = event.detail[0].documentId;
        this.additionaltermsfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Contract for Sub Contractors Additional Terms(IMS 1011)';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.AdditionalTerms= true;
        console.log('FileIds object array',JSON.stringify(this.listFileDetails));
        if(event.detail[0].documentId === ''){
            this.secondfileuploadClass= ".file-container2";
            this.secondfileuploadId = event.detail[0].documentId;
        }
    }
    
    uploadCompleteHandlerHandS(event) {
        this.docId = event.detail[0].documentId;
        this.HandSfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='H and S and Quality Pre-Qual List(IMS 1021)';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.HSQualityterms =true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }      

    
    uploadCompleteHandlerInfoandApproval(event) {
        this.docId = event.detail[0].documentId;
        this.Infoandapprovalfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Prequal H&S Questionnaire(IMS 1039)';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.Infoandapprovalform=true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }
    
    uploadCompleteHandlerKOPledge(event) {
        this.docId = event.detail[0].documentId;
        this.KOpledgefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Kainga Ora Pledge(IMS 1044)';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.KOPledge = true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    uploadCompleteHandlerPublicLiabilityCertificate(event) {
        this.docId = event.detail[0].documentId;
        this.PublicLiabilityCertificatefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Public Liability Certificate';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        //this.listFileIds.push(event.detail[0].documentId);
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
        
    }

    uploadCompleteHandlerVehicleInsurance(event) {
       
        this.docId = event.detail[0].documentId;
        this.VehicleInsurancefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Vehicle Insurance';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        //this.listFileIds.push(event.detail[0].documentId);
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    atleastOneInputThere(filteredarrayobj){
       const keys = Object.keys(filteredarrayobj);
       let setcheckedValue;
       keys.forEach((key, index) => {
           if(this.isValidValue(filteredarrayobj[key]))
           {
               setcheckedValue = 1;
           }            
       });
       if(setcheckedValue === 1)
       {
           return true;
       }
       
       return false;
    }
    saveRecord() {
        console.log('record needs to be saved...');

        this.recordObj["Lead__c"] = this.parameter;
        console.log('FileDetails',this.listFileDetails.length);
        console.log('JSON.stringify(this.listFileDetails)**********',JSON.stringify(this.listFileDetails));
        console.log('this.recordObjVehicle_Insurance_Expiry__c*****',JSON.stringify(this.recordObj['Vehicle_Insurance_Expiry__c']));
        console.log('This is console in saverecord funciton');
        console.log('casecontact data ********'+JSON.stringify(this.recordObj));
        console.log('casecontact data ********'+JSON.stringify(this.casecontact));
        console.log('this.casecontactArray',JSON.stringify(this.casecontactArray));
     
       let arrayToSubmit = [];
       for(let i=0;i< this.casecontactArray.length;i++)
       {
           let filteredArrayObj = Object.assign({}, this.casecontact);
           filteredArrayObj = this.casecontactArray[i];
           console.log('Filterearray object&&&&&&',filteredArrayObj);
           if(this.atleastOneInputThere(filteredArrayObj)){
               arrayToSubmit.push(filteredArrayObj);
               console.log('arraytosubmit',JSON.stringify(arrayToSubmit));
           }
           else{
               break;
           }
       }
        if(this.listFileDetails.length === 0)
        {
            console.log('inside first if');
            let message=AllFilesRequiredErrorMessageLabel;//'Please upload all the files';
            this.showFileErrorToast(message);
        }
        else if(this.listFileDetails.length !=0)
        {
            if(this.SOContractoagreement === false || this.AdditionalTerms === false 
                || this.HSQualityterms === false || this.Infoandapprovalform === false ||
                this.KOPledge === false )
            {
                let message=AllFilesRequiredErrorMessageLabel;//'Please upload all the files';
                this.showFileErrorToast(message);
            }
            else{
                    this.isLoaded=true;    
                    console.log('Now ready to call saveRecord');
                    createRecord({'leadId': this.parameter,'caseContent' : this.recordObj,'caseContactData' :  JSON.stringify(arrayToSubmit),'fileDetails' : JSON.stringify(this.listFileDetails)}).then(result => {
                    console.log ('Case and case contact record created successfully');
                    window.open(this.returnURL, '_self');
                    }).catch(error=> {
                    console.log('error occured');
                    })
                }
    }
    }

    handleSubmit(event) {
      
        console.log('List fo file details',JSON.stringify(this.listFileDetails));
        this.saveRecord(); 
}

    arrayRemove(arr, value) { 
    
        return arr.filter(function(ele){ 
            return ele === value; 
        });
    }   
}