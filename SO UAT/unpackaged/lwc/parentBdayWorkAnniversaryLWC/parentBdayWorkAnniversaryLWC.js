/** */
import { LightningElement, api, wire, track } from 'lwc';
import BirthdayIcon from '@salesforce/resourceUrl/BirthdayIcon';
import WorkAnniversaryIcon from '@salesforce/resourceUrl/WorkAnniversaryIcon';

export default class ParentBdayWorkAnniversaryLWC extends LightningElement {
    @api IsbDayOrWorkSection;
    @api celebration;
    @api celebrationIcon;


    connectedCallback() {
        console.log('BDAYin parent connectedcallback');
        console.log('BDAYthis.isbDayOrWorkSection' + this.IsbDayOrWorkSection);
        if (this.IsbDayOrWorkSection == "Birthday") {
            this.celebrationIcon = BirthdayIcon;
        } else if (this.IsbDayOrWorkSection == "Work Anniversary") {
            this.celebrationIcon = WorkAnniversaryIcon;
        }

    }

}