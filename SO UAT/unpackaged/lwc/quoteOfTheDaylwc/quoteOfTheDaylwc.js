import { LightningElement, track, api } from 'lwc';
import getQuote from '@salesforce/apex/quoteOfTheDay.getQuote';
import quoteIcon from '@salesforce/resourceUrl/quoteIcon';


export default class QuoteOfTheDaylwc extends LightningElement {
    @api error;
    @api quoteIcon = quoteIcon;
    connectedCallback() {
        getQuote()
            .then(result => {
                this.template.querySelector(".quotecontent").innerHTML = result.quote;
                this.template.querySelector(".quoteauthor").innerHTML = '-' + result.author;

            })
            .catch(error => {
                this.error = error;
            });
    }
}