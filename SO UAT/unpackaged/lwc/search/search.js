import { LightningElement, api } from 'lwc';
import contactSearchIcon from '@salesforce/resourceUrl/contactSearchIcon';
import SearchIcon from '@salesforce/resourceUrl/search';

export default class search extends LightningElement {
    @api isModalOpen = false;
    @api searchInput;
    @api contactListNotEmpty = false;
    @api loaded = false;
    @api contactSearchIcon = contactSearchIcon;
    @api SearchIcon = SearchIcon;

    handleChange(event) {
        this.searchInput = event.target.value;
        if (this.searchInput != null && this.searchInput.length > 0) {
            this.template.querySelector('.search__input').style.border = 'solid 1px';
            this.template.querySelector('.inputRequiredText').style.display = 'none';
            this.template.querySelector('.search__button').style.borderTop = 'solid 1px';
            this.template.querySelector('.search__button').style.borderRight = 'solid 1px';
            this.template.querySelector('.search__button').style.borderBottom = 'solid 1px';
        }
    }

    HandleSearch() {
        if (this.searchInput == null || this.searchInput.length < 3) {
            this.template.querySelector('.search__input').style.border = 'solid 1px red';
            this.template.querySelector('.search__button').style.borderTop = 'solid 1px red';
            this.template.querySelector('.search__button').style.borderRight = 'solid 1px red';
            this.template.querySelector('.search__button').style.borderBottom = 'solid 1px red';
            this.template.querySelector('.inputRequiredText').style.display = 'block';

        } else {
            this.isModalOpen = true;
        }
    }

    closeModal() {
        this.isModalOpen = false;
    }

}