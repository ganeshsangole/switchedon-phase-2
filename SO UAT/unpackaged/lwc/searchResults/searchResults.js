import { LightningElement, api } from 'lwc';
import getContacts from '@salesforce/apex/search.getContacts';
import getImageBlocks from '@salesforce/apex/FetchOutlookContact.getImageBlocks';
import DefaultContactImage from '@salesforce/resourceUrl/DefaultContactImage';
import contactDetailsPhone from '@salesforce/resourceUrl/contactDetailsPhone';
import contactDetailsMail from '@salesforce/resourceUrl/contactDetailsMail';
import contactDetailsLocation from '@salesforce/resourceUrl/contactDetailsLocation';
import NoRecordsToDisplayMsg from '@salesforce/label/c.No_Contacts_Found_Label';


export default class SearchResults extends LightningElement {
    @api contactList;
    @api contactListNotEmpty = false;
    @api loaded = false;
    @api searchInput;
    @api contactDetailsLocation = contactDetailsLocation;
    @api contactDetailsMail = contactDetailsMail;
    @api contactDetailsPhone = contactDetailsPhone;
    @api NoRecordsToDisplayMsg = NoRecordsToDisplayMsg;

    connectedCallback() {
        this.getContacts(this.searchInput);
    }

    getContacts = async(searchInput) => {
        var result = await getContacts({
            contactName: searchInput

        });
        result = JSON.parse(result);
        this.contactList = result;
        if (result == null || result.length == 0) {
            this.contactListNotEmpty = false;
        } else {
            this.contactListNotEmpty = true;
            for (var i = 0; i < (this.contactList.length); i++) {
                this.contactList[i].MailToLink = 'mailto:' + this.contactList[i].Email;
                this.contactList[i].profilePhoto = DefaultContactImage;
                if (this.contactList[i].ExternalId != null) {
                    var photo = await getImageBlocks({ userId: this.contactList[i].ExternalId });
                    if (photo != null) {
                        this.contactList[i].profilePhoto = 'data:image/jpeg;base64,' + photo;
                    }
                    photo = null;
                }



            }
        }

        this.loaded = true;
    };
}