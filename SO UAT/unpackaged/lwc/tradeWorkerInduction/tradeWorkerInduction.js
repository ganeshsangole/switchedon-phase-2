import { LightningElement,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class TradeWorkerInduction extends NavigationMixin(LightningElement) {
    //@api recordId;
    @api redirectURL; 
     parameter ;
     disabledCondition = true;

    getQueryParameters() {

        var params = {};
        var search = location.search.substring(1);
    
        if (search) {
            params = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
                return key === "" ? value : decodeURIComponent(value)
            });
        }
    
        return params;
    }

    connectedCallback() {
        this.parameter = this.getQueryParameters().id;
        //window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
        console.log('Parameters**********************',this.parameter);
    }

    checkboxChecked(event){
        if (event.target.checked)
        {
          console.log('Check box is checked');
          this.disabledCondition = false;
        }
        else
        {
          console.log('check box is unchecked');
          this.disabledCondition = true;
        }
    }

    //Navigate to community page
    handleNavigate() {
        
        //disabledCondition
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                //url: 'https://tnzdev-switchedon.cs76.force.com/trades/s/company-induction?id=' + this.parameter
                url: this.redirectURL + '?id=' + this.parameter
            }
        }).then(generatedUrl => {
            window.open(generatedUrl,"_self");
        });
    }
}