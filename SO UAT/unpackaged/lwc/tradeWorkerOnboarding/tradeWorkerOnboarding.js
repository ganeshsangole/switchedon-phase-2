import { LightningElement, api, wire,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import SO_logo from '@salesforce/resourceUrl/SwitchedOnLogo';
import declarationLabel from '@salesforce/label/c.Declaration';
import PrivacyDisclaimerLabel from '@salesforce/label/c.PrivacyDisclaimer';
import WorkImmigrationStatusLabel from '@salesforce/label/c.WorkImmigrationStatus';
import TradeQualificationExpiryDateLabel from '@salesforce/label/c.TradeQualificationExpiryDate';
import WorkImmigrationExpiryDateLabel from '@salesforce/label/c.WorkImmigrationExpiryDate';
import TradeQualificationRegistrationLabel from '@salesforce/label/c.TradeQualificationRegistration';
import tradeworkererrormessageLabel from '@salesforce/label/c.TradeWorkerErrorMessage';
import FileErrorMessageLabel from '@salesforce/label/c.FileErrorMessage';
import tradeWorkerInducteeDetailsErrorMessageLabel from '@salesforce/label/c.tradeWorkerInducteeDetailsErrorMessage';
import MOJResultErrorMessageLabel from '@salesforce/label/c.MOJResultErrorMessage';
import PhotographEvidenceErrorMessageLabel from '@salesforce/label/c.PhotographEvidenceErrorMessage';
import AllFilesRequiredErrorMessageLabel from '@salesforce/label/c.AllFilesRequiredErrorMessage';
import WorkVisaErrorMessageLabel from '@salesforce/label/c.WorkVisaErrorMessage';
import createRecord from '@salesforce/apex/TradeWorkerRecordCreation.createCaseForTradeWorker';
import getAccountDetails from '@salesforce/apex/TradeWorkerRecordCreation.getAccountDetails';
import pickListValueDynamically  from '@salesforce/apex/TradeWorkerRecordCreation.pickListValueDynamically';
import POSVendorEntries from '@salesforce/apex/TradeWorkerRecordCreation.getPickListEntries';

export default class SOTradeWorkerInductionForm extends LightningElement {
    SO_logourl = SO_logo;
    label = {
        declarationLabel,
        PrivacyDisclaimerLabel
    };
    @api returnURL;
    @api privacypracticesURL; 
    @track statuspicklistVal;
    @track originpicklistVal;
    @track parameter ;
     leadName;
     leadCompany;
     leadRecordId;
     docId;
     filename;
     photographfilename;
     EvidenceOfIdfilename;
     RegisterationCertificatefilename;
     SiteSafeCertificatefilename;
     MOJResultfilename;
     WorkVisafilename;
     MOJReleasefilename;
     hideRegistrationNo =true;
     hideRegistrationNo1=true;
     hideRegistrationNo2=true;
     photographuploaded = false;
     mojreleaseuploaded = false;
     evidenceIduploaded = false;
     mojresultuploaded =false;
     workvisauploaded = false;
     disableSubmit= true;
     showConflictOfInterest =false;
     showQualificationFields1 = false;
     showQualificationFields2= false;
     showRelationshipToPerson =false;
     areDetailsVisible = false;
     declarationfieldValue = false;
     privacydisclaimerfieldValue = false;
     showVisaExpiryDate = false;
     requiredExpiryDate1 =false;
     requiredExpiryDate2 =false;
     requiredExpiryDate3 =false;
     requiredVisaExpiryDate =false;
     showTradeQualificationFields=false;
     showTradeQualificationFields1=false;
     showTradeQualificationFields2=false;
     showWorkVisaFileUpload = false;
     formSubmitted = false;
     workvisafileuploaded=false;
     showHideAdd1=true;
     showHideAdd2=false;
     showHideRemove1=false;
     listFileIds = [];
     listFileDetails = [];
     
     listOfFiles={
        "FileNames" : "",
        "Fileids" : "",
    }
    @api supportedFiles;
    @api isLoaded = false;

   supportedFiles = ['.doc', '.docx' ,'.pdf','.png','.jpeg','.jpg'];


    
    
    // toggle logic

   changeMOJToggle(event){
    let fieldapiname = event.target.getAttribute("data-id");
    let fieldvalue  = event.target.checked;
    console.log('event.target.value***');
    this.recordObj[fieldapiname]=fieldvalue;
    this.areDetailsVisible = event.target.checked;
   }

    changeToggle(event){
       let fieldapiname = event.target.getAttribute("data-id");
       let fieldvalue  = event.target.checked;
       console.log('event.target.value***');
       this.recordObj[fieldapiname]=fieldvalue;
    if(this.recordObj['Is_KO_tenant_family_members_in_KO_prop__c'] == true)
    {
        this.showConflictOfInterest =true;
    }
    else{
        this.showConflictOfInterest =false;
    }
    if(this.recordObj['Close_relationship_with_Kainga_Ora_SOH__c'] == true){
        this.showRelationshipToPerson= true;
    }
    else{
        this.showRelationshipToPerson=false;
    }
    
    }



    
    // multislect api
    @api 
    get baseLocation() {
        return this._baseLocation;
    }
  
    // multiselect picklist
    changepicklisthandler(event){
        console.log('element.insidevaluepicklist*****');
        let fieldName = event.target.getAttribute("data-id");
        let fieldValue = event.target.value;
       console.log('fieldValue**', fieldValue);
       let selectedValue = '';
       for(let i=0; i<event.target.options.length; i++) {
        if(event.target.options[i].selected) {
        selectedValue += event.target.options[i].value+';';
        }
}
       this.recordObj[fieldName] = selectedValue;
       console.log('selectedValue**');

     // logic to remove last character from string
         selectedValue = selectedValue.substr(0,selectedValue.length-1);
    
              this.recordObj[fieldName] = selectedValue;
    }

    



    set baseLocation(value) {
        if(this.isValidValue(value)) {
            this._baseLocation = value;
        }else {
            this._baseLocation = '';
        }
    }
    pumpTypesPickListEntries;


showNextFields = false;
dontshowNextFields =true;




getQueryParameters() {

    var params = {};
    var search = location.search.substring(1);

    if (search) {
        params = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value)
        });
    }
    return params;
}

connectedCallback() {
    this.parameter = this.getQueryParameters().id;
    
    console.log('Parameters**********************',this.parameter);
    this.getAccountDetails(this.parameter);
    
    
}

showErrorToast(message){
    const evt = new ShowToastEvent({
        title: 'Error',
        message: message,//tradeworkererrormessageLabel,//'There are no admin contact details. Please contact support.',
        variant: 'error',
        mode: 'dismissable'
    });
    this.dispatchEvent(evt);
}


getAccountDetails(val)
{
    console.log('This Parameter',val);
    getAccountDetails({'accountId': val}).then(result => {
        console.log('result******',result);
        this.recordObj['SuppliedCompany'] = result['CompanyName'];
        this.recordObj['Admin_Phone__c'] = result['AdminPhone'];
        this.recordObj['Admin_Email__c'] = result['AdminEmail'];
    
        }).catch(error=> {
            console.log('error occured');
        })
        console.log('Before if',JSON.stringify(this.recordObj));
   

}


recordObj = {
    "sObjectype":'Case',
    "SuppliedCompany" : "",
    "SuppliedPhone" :"",
    "SuppliedEmail" : "",
    "MOJ_Request_Status__c" : "",
    "First_Name__c" : "",
    "Last_Name__c" : "",
    "Admin_Email__c" : "",
    "Admin_Phone__c" : "",
    "Job_Title__c" : "",
    "Work_Immigration_Status__c" : "",
    "Visa_Expiry_Date__c" : "",
    "Working_under_supervision_of__c" : "",
    "Base_Locations__c" : "",
    "Date_of_Birth__c" : "",
    "Ethnicity__c" : "",
    "Expiry_Date__c" : "",
    "Gender__c" : "",
    "Do_you_have_a_Criminal_History_Record__c" : false,
    "Is_a_Direct_Employee__c" : false,
    "Obligation_under_HSAW_Act_2015__c": false,
    "Inducted_through_internal_systems_fami__c" : false,
    "Is_Construction_ready__c" : false,
    "Is_KO_tenant_family_members_in_KO_prop__c" : false,
    "Details_of_conflict_of_interest_with_KO__c" : "",
    "Relationship_to_Person_1__c" : "",
    "Relationship_to_Person_2__c" : "",
    "Relationship_to_Person_3__c" :"",
    "Address_1__c" : "",
    "Address_2__c" : "",
    "Address_3__c" : "",
    "Postal_Address__c" : "",
    "Site_Safe_Qualifications_or_Equivalent__c" : "",
    "Trade_Qualifications_Registration_No__c" : "",
    "Trade_Qualifications_Registration_No2__c":"",
    "Trade_Qualifications_Registration_No3__c":"",
    "Working_Under_Supvson_Of_Registration_No__c" : "",
    "Site_Safe_Qualifications_Registration_No__c" : "",
    "Apprenticeship_Certification_Types__c" : "",
    "Salutation__c" : "",
    "Trade_Qualifications_Registrations__c" : "",
    "Trade_Qualifications_Registrations2__c" : "",
    "Trade_Qualifications_Registrations_3__c" : "",
    "Expiry_date_of_registration_license__c":"",
    "Expiry_date_of_registration_license_2__c" :"",
    "Expiry_date_of_registration_license_3__c":"",
    "Licence_Type__c":"",
    "Licence_Type_2__c":"",
    "Licence_Type_3__c":"",
    "Postal_Address_City__c" : "",
    "Postal_Address_Postal_Code__c" : "",
    "Postal_Address_Suburb__c" : "",
    "Privacy_Disclaimer__c":"",
    "Close_relationship_with_Kainga_Ora_SOH__c" : false,
    "Conflict_Of_Interest_Details__c" :"",
    "AccountId" : this.parameter,
};
beforeUnloadHandler() {
   
}
//get sleected value
getSelectedValues(inputElement) {
    let selectedValue = '';
    if(this.isValidValue(inputElement) && inputElement.options.length > 0) {
        for(let i=0; i<inputElement.options.length; i++) {
            if(inputElement.options[i].selected) {
                selectedValue += inputElement.options[i].value+';';
            }
        }
    }

    if(selectedValue.length > 0) {
        selectedValue = selectedValue.substr(0, selectedValue.length-1);
    }

    return selectedValue;
}


disconnectedCallback() {
    console.log('inside disconnected callback...');
   
} 

validateFile() {
    let filePresent = true;
    if(!this.isValidValue(this.docId)) {
        console.log('file is not uploaded...');
        let uploadFileDiv = this.template.querySelector(".file-container");
        uploadFileDiv.classList.add("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("hide-error");
        errorDiv.classList.add("show-error");
        filePresent = false;
    }

    return filePresent;
}

renderedCallback() {
    console.log('rendered called*********');
    this.template.querySelectorAll('select').forEach(element => {
        
        let fieldName = element.getAttribute("data-id");
        let selectedValues = this.recordObj[fieldName];
        let selectedValueArray = selectedValues.split(';');
        console.log('selectedValueArray**');
        for(let i=0; i<element.options.length; i++) {
            if(selectedValueArray.indexOf(element.options[i].value) > -1) {
                element.options[i].selected = true;
            }
        }
       
        console.log('element.value*****',element.value);
    });
}

handleDeclarationChange(event){
    let fieldName = event.target.getAttribute("data-id");
    let fieldValue = event.target.value;

    if(fieldName==='declaration')
   {
       
       console.log('Inside if for checkbox');
       this.declarationfieldValue  = event.target.checked;
    console.log('declarationFieldName*****',fieldName);
    console.log('declarationFieldValue*****',this.declarationfieldValue);
   }
   if(fieldName==='privacydisclaimer')
   {
    console.log('Inside if for checkbox privacydisclaimer');
    this.privacydisclaimerfieldValue  = event.target.checked;
    this.recordObj["Privacy_Disclaimer__c"]=this.privacydisclaimerfieldValue;
    console.log('privacydisclaimerfieldValue*****',this.privacydisclaimerfieldValue);
    console.log('privacydisclaimerFieldValue*****',JSON.stringify(this.recordObj["Privacy_Disclaimer__c"]));
   
   }
   if(this.declarationfieldValue === true && this.privacydisclaimerfieldValue === true)
   {
       this.disableSubmit = false;
   }
   else if(this.declarationfieldValue === false && this.privacydisclaimerfieldValue === false)
   {
       this.disableSubmit =true;
   }
   else if((this.declarationfieldValue === true && this.privacydisclaimerfieldValue === false ) || (this.declarationfieldValue === false && this.privacydisclaimerfieldValue === true)){
      this.disableSubmit =true;
   }
}


handleChange(event) {
    console.log('Handle change method called...');
    let fieldName = event.target.getAttribute("data-id");
    let fieldValue = event.target.value;
    console.log('FieldValue in worker form',fieldValue);
    let immigrationstatus=WorkImmigrationStatusLabel;
    let tradequalificationregistration=TradeQualificationRegistrationLabel;

        this.recordObj[fieldName] = fieldValue;

    if(fieldName=== 'Work_Immigration_Status__c')
    {
        if(immigrationstatus.includes(fieldValue)){
            console.log('Inside work immigration related condition'); 
            this.showVisaExpiryDate = true;
             this.showWorkVisaFileUpload =true;
             this.requiredVisaExpiryDate =true;
         }
         
        else{
            this.showVisaExpiryDate =false;
            this.showWorkVisaFileUpload =false;
            this.requiredVisaExpiryDate =false;
            this.recordObj['Visa_Expiry_Date__c']="";
        }
    }
    if(fieldName=== 'Trade_Qualifications_Registrations__c')
    {
        if(tradequalificationregistration.includes(fieldValue)){
            console.log('Inside Trade_Qualifications_Registrations__c related condition'); 
            this.showTradeQualificationFields = true;
            this.hideRegistrationNo =false;
            this.requiredExpiryDate1=true;
         }
        else{
            this.showTradeQualificationFields =false;
            this.hideRegistrationNo =true;
            this.requiredExpiryDate1=false;
            this.recordObj['Expiry_date_of_registration_license__c']="";
        }
    }
    if(fieldName=== 'Trade_Qualifications_Registrations2__c'){
        if(tradequalificationregistration.includes(fieldValue)){
        this.showTradeQualificationFields1 = true;
        this.hideRegistrationNo1 =false;
        this.requiredExpiryDate2=true;
        }
        else{
            this.showTradeQualificationFields1 =false;
            this.hideRegistrationNo1 =true;
            this.requiredExpiryDate2=false;
        }
    }
    if(fieldName=== 'Trade_Qualifications_Registrations_3__c'){
        if(tradequalificationregistration.includes(fieldValue)){
            this.showTradeQualificationFields2 = true;
            this.hideRegistrationNo2 =false;
            this.requiredExpiryDate3=true;
            }
            else{
                this.showTradeQualificationFields2 =false;
                this.hideRegistrationNo2 =true;
                this.requiredExpiryDate3=false;
            }
    }

}

handleAdd(event){
    let fieldName = event.target.getAttribute("data-id");
    console.log('Inside handleAdd');
    if(fieldName==='Add1'){
        console.log('Inside handleAdd1');
    this.showQualificationFields1 =true;
    this.showHideAdd1 =false;
    this.showHideAdd2 =true;
    this.hideRegistrationNo1 =true;
    this.showHideRemove1 =true;
    }
    if(fieldName==='Add2'){
        console.log('Inside handleAdd2');
    this.showQualificationFields2 = true;
    this.showHideAdd2 =false;
    this.hideRegistrationNo2 =true;
    this.showHideRemove1 =false;
    }
}

handleRemove(event){

    let fieldName = event.target.getAttribute("data-id");
    console.log('Inside handleRemove',fieldName);
    if(fieldName==='Remove1'){
        console.log('Inside handleRemove1');
    this.showQualificationFields1 =false;
    this.showHideAdd1=true;
    this.recordObj['Trade_Qualifications_Registrations2__c']=''
    this.recordObj['Trade_Qualifications_Registration_No2__c']='';
    this.recordObj['Licence_Type_2__c']='';
    this.recordObj['Expiry_date_of_registration_license_2__c']='';
    this.showTradeQualificationFields1=false;
    this.requiredExpiryDate2 =false;
    console.log('Inside handleRemove1',this.showQualificationFields1);
    }
    if(fieldName==='Remove2'){
        console.log('Inside handleRemove2');
    this.showQualificationFields2 = false;
    this.showHideAdd2=true;
    this.showHideRemove1=true;
    this.recordObj['Trade_Qualifications_Registrations_3__c']=''
    this.recordObj['Trade_Qualifications_Registration_No3__c']='';
    this.recordObj['Licence_Type_3__c']='';
    this.recordObj['Expiry_date_of_registration_license_3__c']='';
    this.showTradeQualificationFields2=false;
    this.requiredExpiryDate3=false;
    console.log('Inside handleRemove2',this.showQualificationFields2);

    }
}

isValidValue(value) {
    if(value != null && value != '' && typeof value != 'undefined') {
        return true;
    }

    return false;
}

handlePrevious(event) {
    this.showNextFields = false;
    this.dontshowNextFields = true;
    this.disableSubmit= true;
    console.log('this.recordObj[fieldName]****',this.recordObj);
 
}

handleNext(event) {
    
    console.log('Insied next record obj******',this.recordObj);
    
    if((!this.isValidValue(this.recordObj['Admin_Phone__c'])) || 
        (!this.isValidValue(this.recordObj['Admin_Email__c']))){
            let message = tradeworkererrormessageLabel;
            this.showErrorToast(message);
    } 
    else if((!this.isValidValue(this.recordObj['First_Name__c'])) || 
    (!this.isValidValue(this.recordObj['Last_Name__c'])) || (!this.isValidValue(this.recordObj['Work_Immigration_Status__c'])) ||
    (!this.isValidValue(this.recordObj['Gender__c'])) || (!this.isValidValue(this.recordObj['Base_Locations__c'])) ||
    (!this.isValidValue(this.recordObj['Date_of_Birth__c'])) || (!this.isValidValue(this.recordObj['Ethnicity__c'])) ||
    (!this.isValidValue(this.recordObj['SuppliedPhone'])) || (!this.isValidValue(this.recordObj['SuppliedEmail'])) ||
    (!this.isValidValue(this.recordObj['Postal_Address__c'])) || (!this.isValidValue(this.recordObj['Postal_Address_City__c'])) ||
    (!this.isValidValue(this.recordObj['Postal_Address_Postal_Code__c'])) ||
    (!this.isValidValue(this.recordObj['Job_Title__c']))){
    let message=tradeWorkerInducteeDetailsErrorMessageLabel;
    this.showErrorToast(message);
    }
    else if((!this.isValidValue(this.recordObj['Expiry_date_of_registration_license__c'] )) && this.requiredExpiryDate1 === true)
    {
        let message=TradeQualificationExpiryDateLabel;
        this.showErrorToast(message);
    }   
    else if((!this.isValidValue(this.recordObj['Expiry_date_of_registration_license_2__c'] )) && this.requiredExpiryDate2 === true){
        let message=TradeQualificationExpiryDateLabel;
        this.showErrorToast(message);
    }
    else if((!this.isValidValue(this.recordObj['Expiry_date_of_registration_license_3__c'] )) && this.requiredExpiryDate3 === true){
        let message=TradeQualificationExpiryDateLabel;
        this.showErrorToast(message);
    }
    else if((!this.isValidValue(this.recordObj['Visa_Expiry_Date__c'] )) && this.requiredVisaExpiryDate === true){
        let message=WorkImmigrationExpiryDateLabel;
        this.showErrorToast(message);
    }        
    else{
    
        this.showNextFields = true;
        this.dontshowNextFields = false;
      }
    }
  
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Ethnicity__c'}) selectStatusTargetValues;
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Trade_Qualifications_Registrations__c'}) selectTradeQualificationRegistrationTargetValues;
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Work_Immigration_Status__c'}) selectWorkImmigrationStatusTargetValues;
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'MOJ_Request_Status__c'}) selectMOJRequestStatusTargetValues;  
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Job_Title__c'}) selectJobTitleTargetValues; 
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Gender__c'}) selectGenderTargetValues;   
@wire(pickListValueDynamically, {customObjInfo: {'sobjectType' : 'Case'},
selectPicklistApi: 'Salutation__c'}) selectSalutationTargetValues; 
 
// multiselect wire
@wire(POSVendorEntries, {'fieldApiName' : 'Base_Locations__c', 'objectName' : 'Case'})
getPumpTypesEntries({error, data}) {
    if(data) {
        this.pumpTypesPickListEntries = data;
        console.log('this.pumpTypesPickListEntries.......+data' );
    }else if(error) {
        console.log('Error Occured while fetching picklist........');
        console.log(error);
    }
}



   
    uploadCompleteHandler(event) {
      
        this.docId = event.detail[0].documentId;
        this.photographfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Photograph';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.photographuploaded =true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    uploadCompleteHandlerEvidenceOfId(event) {
       
        this.docId = event.detail[0].documentId;
        this.EvidenceOfIdfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Evidence of Id';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.evidenceIduploaded= true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }
    uploadCompleteHandlerRegisterationCertificate(event) {
      
        this.docId = event.detail[0].documentId;
        this.RegisterationCertificatefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Registration Certificate';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    uploadCompleteHandlerSiteSafeCertificate(event) {
      
        this.docId = event.detail[0].documentId;
        this.SiteSafeCertificatefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='Site Safe Document';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    uploadCompleteHandlerMOJResult(event) {
      
        this.docId = event.detail[0].documentId;
        this.MOJResultfilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='MOJ Result';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.mojresultuploaded =true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }

    uploadCompleteHandlerMOJRelease(event) {
     
        this.docId = event.detail[0].documentId;
        this.MOJReleasefilename = event.detail[0].name;
        this.contentId = event.detail[0].name;
        let tempObj = Object.assign({}, this.listOfFiles);
        tempObj["FileNames"]='MOJ Release';
        tempObj["Fileids"]=event.detail[0].documentId;
        this.listFileDetails.push(tempObj);
        this.mojreleaseuploaded =true;
        let uploadFileDiv = this.template.querySelector(".file-container1");
        uploadFileDiv.classList.remove("error-css");
        let errorDiv = this.template.querySelector(".error-msg");
        errorDiv.classList.remove("show-error");
        errorDiv.classList.add("hide-error");
    }
    
    uploadCompleteHandlerWorkVisa(event) {
   
         this.docId = event.detail[0].documentId;
         this.WorkVisafilename = event.detail[0].name;
         console.log('WorkVisafilename**********---------',this.WorkVisafilename);
         this.contentId = event.detail[0].name;
         let tempObj = Object.assign({}, this.listOfFiles);
         tempObj["FileNames"]='Work Visa';
         tempObj["Fileids"]=event.detail[0].documentId;
         this.listFileDetails.push(tempObj);
         this.workvisauploaded= true;
         console.log('File inside work visa',JSON.stringify(this.listFileDetails));
         
         let uploadFileDiv = this.template.querySelector(".file-container1");
         uploadFileDiv.classList.remove("error-css");
         let errorDiv = this.template.querySelector(".error-msg");
         errorDiv.classList.remove("show-error");
         errorDiv.classList.add("hide-error");
     }

    showFileErrorToast(message) {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: message,
            variant: 'error',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    saveRecord() {
        console.log('record needs to be saved...');

        this.recordObj["AccountId"] = this.parameter;
        console.log('This is console in saverecord funciton');
      
        console.log(JSON.stringify(this.recordObj));
        console.log('this.recordObj**********'+ this.recordObj);
       if(this.listFileDetails.length === 0)
        {
            console.log('inside first if');
            let message=FileErrorMessageLabel;//'Please upload files';
            this.showFileErrorToast(message);
        }
        else if(this.listFileDetails.length !=0)
        {
                if(this.areDetailsVisible === true && this.mojresultuploaded === false)
                {
                    console.log('inside first if of moj result',this.listFileDetails.length);
                    let message=MOJResultErrorMessageLabel;//'Please upload MOJ Result';
                    this.showFileErrorToast(message);
                }
                else if(this.showWorkVisaFileUpload === true && this.workvisauploaded ===false)
                {
                    console.log('inside first if of work visa',this.listFileDetails.length);
                    let message=WorkVisaErrorMessageLabel;//'Please upload Work Visa';
                    this.showFileErrorToast(message);
                    
                }
                else if(this.areDetailsVisible === true && this.showWorkVisaFileUpload === true && (this.mojresultuploaded === false || this.workvisauploaded === false))
                {
                    console.log('inside first if of work visa and moj result',this.listFileDetails.length);
                    let message=AllFilesRequiredErrorMessageLabel;//'Please upload all the files';
                    this.showFileErrorToast(message);
                    
                }
                else if(this.photographuploaded === false || this.evidenceIduploaded === false || this.mojreleaseuploaded === false)
                {
                    console.log('inside first if of normal uploads',this.listFileDetails.length);
                     let message=PhotographEvidenceErrorMessageLabel;//'Please upload Photograph and Evidence Of Id';
                    this.showFileErrorToast(message);
                }
                else{
                    console.log('inside first if of normal uploads',this.listFileDetails.length);
                    console.log('Photograph uploaded',this.photographuploaded);
                    console.log('Evidence ID uploaded',this.evidenceIduploaded);
                    console.log('Inside else to create record');
                    this.isLoaded=true; 
                    createRecord({'AccountId' : this.parameter,'caserecord' : this.recordObj,'fileDetails' : JSON.stringify(this.listFileDetails)}).then(result => {
                    window.open(this.returnURL, '_self');
                    }).catch(error=> {
                    console.log('error occured');
                    })
                }
               
        }
  }

    handleSubmit(event) {
    
    console.log('Inside handleSubmit')
    console.log('Declaration checkbox value',this.declarationfieldValue)
   
        this.saveRecord();
}
   
}