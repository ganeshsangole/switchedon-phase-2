/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Case Trigger used to create trade contacts,validate onboarding task completion, 
             validate KO approval form and create content document link
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
trigger CaseTrigger on Case (before update, after update) {
    if(!PreventRunningLogic__c.getInstance().CaseDontRunLogic__c){
        CaseTriggerHandler.handleMethod(Trigger.new, Trigger.oldMap);
    }
}