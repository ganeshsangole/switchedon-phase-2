/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Contact Trigger used to set trade worker Id,set Contact's on Trade account and
             set employee account
History
10-07-2021 Tranzevo Initial Release 
----------------------------------------------------------------------------------------------*/
trigger ContactTrigger on Contact (before insert, after insert) {
    ContactTriggerHandler.handleMethod(Trigger.new,Trigger.oldMap);
}