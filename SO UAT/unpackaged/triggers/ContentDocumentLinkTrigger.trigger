/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: ContentDocumentLinkTrigger used to update the file ids on the corresponding 
			 MOJ result record
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
   ContentDocumentLinkHandler.handleMethod(Trigger.new);
}