/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Lead Trigger for lead conversion
History
10-07-2021 Tranzevo Initial Release for Trade Worker Onboarding Process 
----------------------------------------------------------------------------------------------*/
trigger LeadTrigger on Lead (after update) {
    LeadTriggerHandler.handleMethod(Trigger.new,Trigger.oldMap);
}