/*----------------------------------------------------------------------------------------------
Author : Tranzevo
Company: Tranzevo
Description: Apex Trigger On User Object
History
01-06-2021 Tranzevo Initial Release
----------------------------------------------------------------------------------------------*/

trigger UserTrigger on User (before insert,after insert) { 
     if(!PreventRunningLogic__c.getInstance().UserDontRunLogic__c){
       UserTriggerHandler.handleTriggerLogic(trigger.new,trigger.oldmap,trigger.newmap);  
    }
          
}